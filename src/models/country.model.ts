export default interface CountryModel {
  id: number,
  name: string,
  code: string
}

export interface CountryCodeModel {
  name: string,
  code: string,
  dial_code: string,
}


export const countryDefaultState ={
  id: 0,
  name: "Germany (DE)",
  code: "79"
}