
export default interface AuthVerifySubmitDtoModel {
  id: string;
  code?: string;
}
