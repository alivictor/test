import UserAgreementChildModel from "./userAgreementChild.model";
import PhoneModel from "../phone.model";

export default interface AuthRegisterSubmitDtoModel {
  mail: string;
  password: string;
  firstName: string;
  lastName: string;
  agreement: UserAgreementChildModel;
  phone: PhoneModel;
  promotionalCode: string;
  country: any;
  gender: string;
  membership: string;
}
