import UserModel from "../user.model";

export default interface AuthVerifyResponseDtoModel {
  token: string,
  user: UserModel;
}
