import UserModel from "../user.model";

export interface TransactionDto {
    id: number,
    user: UserModel,
    operator: UserModel,
    serial: string,
    amount: number,
    description: string,
    isManually: boolean
    status: string,
    type: string,
    createdAt: string,
}
