import UserModel from "../user.model";

export default interface AuthLoginResponseDtoModel {
  token: string,
  user: UserModel;
}
