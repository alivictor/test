import UserModel from "../user.model";

export interface ProfitDto {
    id: number,
    user: UserModel,
    about: string,
    amount: number,
    createdAt: string,
    paidAt: string,
    updatedAt: string,
    isActive: boolean
}
