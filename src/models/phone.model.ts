export default interface PhoneModel {
  number: string,
  countryCode: string,
  additionalNumber?: string
}
