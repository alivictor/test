import CountryModel from "./country.model";
import PhoneModel from "./phone.model";
import UserPhoto from "./userPhoto.model";

export default interface UserModel {
  id: string;
  mail?: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  phone?: PhoneModel;
  createdAt?: string;
  country?: CountryModel;
  point: number;
  role?: string,
  registerMembership: string;
  photo: UserPhoto,
  balance: number,
  balanceBlocked: number,
  promotional?: {
    representativeCode?: string,
    referralCode?: string,
  };
  kyc: {verify: boolean}
  wallet?: {
    bitcoin?: string,
    ethereum?: string
  };
  eurNameOfTheClient?: string,
  eurAddressOfTheClient?: string,
  eurBankName?: string,
  eurBankAddress?: string,
  eurIBAN?: string,
  eurSwiftOrBIC?: string,
  eurCountryClient?: CountryModel,
  eurCountryBank?: CountryModel,
  eurComments?: string,
  firebaseNotificationId?: string,
  gender?: string,
  startedAt: string,
  startedAtToken: string,
  gender_label?: string,
  setting:{
    emailLanguage: string
  }
}

