export default interface ResourceModel {
  id: number
  title: string
  slug: string
  icon1: string
  icon2: string
  color: string
  amount: string
  percent: string
}
