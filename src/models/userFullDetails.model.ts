import UserModel from "./user.model";
import {LevelItemInterface} from "./levels.model";

export default interface UserFullDetailsModel {
  financial:{
    roiPercent: number
    totalBalance: number
    totalBonusDeposit: number
    totalDeposit: number
    totalProfit: number
  },
  user: UserModel,
  level:LevelItemInterface
}
