import UserPhoto from "./userPhoto.model";

export default interface InvestModel {
  id: number,
  isActive: boolean,
  attachment: UserPhoto,
  lastActivity:{
    amount: string,
    amountAccepted: string,
    createdAt: string,
    description: string,
    status: string,
  },
  saIBAN: string | number,
  saBIC: string | number,
  saAccountOwner: string,
  saBank: string,
  createdAt: string,
  trackingCode: string,
}
