
export default interface MarketModel {
  marketCap: number
  smbPrice: number
  totalInvestToken: number,
  monthlyExpectedRoi: number
  monthlyCurrentRoi: number
}
