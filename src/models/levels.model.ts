
export interface levelInterface {
  id: string;
  title: string;
  amount: string | number;
  body: string;
  profitAverage?: number;
  createdAt: Date;
  updatedAt: Date;
  isActive: boolean;
  groupMinRange: string;
  groupMaxRange: string;
  levels: LevelItemInterface[]
}


export interface LevelItemInterface {
  id: number;
  title: string;
  displayTitle: string;
  displayAmount: string;
  market: string;
  profitAverage: number;
  minScore: string;
  maxScore: string;
  groupMinRange: string;
  groupMaxRange: string;
  minMonthlyProfit: string;
  maxMonthlyProfit: string;
  minYearlyProfit: string;
  maxYearlyProfit: string;
  eurBankTransfer: boolean;
  withdrawals24Support: boolean;
  accessToOurInstitutionalAccounts: boolean;
  accessToTheOtcPreciousMetalsMarket: boolean;
  accessToTheOtcOilAndCryptoMarket: boolean;
  additionalOtcMarketsWithHigherMargins: boolean;
  additionalSMTTokenBonusPerTrade: boolean;
  conciergeServiceForManualHigherMarginTrades: boolean;
  accessToTheRealEstateMarkets: boolean;
  accessToTheOtcForexMarket: boolean;
  createdAt: Date;
  updatedAt: Date;
  isActive: boolean;
  groupId: number;
  color: {
    text: string,
    background: string
  };
  group? : {
    id: string;
    title: string;
    createdAt: Date;
    updatedAt: Date;
    isActive: boolean;
  },
  resources?: {
    color: string
    icon1: string
    icon2: string
    id: number
    slug: string
    title: string
    weight: number
  }[]
}


