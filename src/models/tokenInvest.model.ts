import UserModel from "./user.model";

export default interface TokenInvestModel {
  id: number,
  isActive: boolean,
  createdAt: string,
  updatedAt: string,
  paidAt: string,
  saTrackingCode: string,
  operator: UserModel,
  user: UserModel,
  userId: number,
  isManually: boolean,
  operatorId: number,

  price_amount:string
  smb_convert_type: string
  smb_price:string,
  status: string,
  token: {amount: string,}
  token_amount: string,
  type: string,
}
