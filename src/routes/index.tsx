import React, {Suspense, useState} from "react";
import {RouteConfig, renderRoutes, RouteConfigComponentProps} from "react-router-config";
import {BrowserRouter} from "react-router-dom";
import Loading from "../components/UiKits/Loading/Loading";
import URLs from "../constants/URLs";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";
import {useTranslation} from "react-i18next";
import MessageModal from "../components/UiKits/Modal/MessageModal";
import {getLocalStorage} from "../assets/helpers/storage";
import {LayoutSwitch, PathSwitch, RouterSwitch} from "./routes";
import MembershipModel from "../models/membership.model";
import Splash from "../containers/Splash";

//Views
const My404Component = React.lazy(() => import('../containers/404'));
const DeepLinks = React.lazy(() => import('../containers/DeepLinks/DeepLinks'));
//auth
const Logout = React.lazy(() => import('../containers/Auth/Logout'));
const AuthWrapper = React.lazy(() => import('../layouts/auth/AuthWrapper'));
const Login = React.lazy(() => import('../containers/Auth/Login'));
const SignUp = React.lazy(() => import('../containers/Auth/SignUp'));
const VerifyAccount = React.lazy(() => import('../containers/Auth/VerifyAccount'));
const ForgotPassword = React.lazy(() => import('../containers/Auth/ForgotPassword'));
const ResetPassword = React.lazy(() => import('../containers/Auth/ResetPassword'));

const Root = ({route}: RouteConfigComponentProps) => {
    const [forceModal, setForceModal] = useState<boolean>(false);
    const {
        t,
    } = useTranslation();
    return (
        <div>
            {
                forceModal
                &&
                <MessageModal
                  callBackClose={() => setForceModal(true)}
                  title={t('errors.maintenance_work')}
                  kind={"red"}
                  content={""}
                  button={true}
                  icon={""}/>
            }
            {renderRoutes(route && route.routes)}
            <ToastContainer
                position="bottom-right"
                autoClose={10000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
};

const memberships: MembershipModel[] = getLocalStorage('memberships') || [];

let dashboardRoutes: any[] = [];

if (memberships && memberships.length !== 0) {
    memberships.map(membership => {
        dashboardRoutes.push({
            path: PathSwitch(membership.slug),
            component: LayoutSwitch(membership.slug),
            routes: RouterSwitch(membership.slug),
            loadData: () => Promise.resolve({})
        })
    })
} else {
    dashboardRoutes.push({
        path: PathSwitch("arbitrage-investor"),
        component: LayoutSwitch("arbitrage-investor"),
        routes: RouterSwitch("arbitrage-investor"),
        loadData: () => Promise.resolve({})
    })
}

// route config
const routes: RouteConfig[] = [
    {
        component: Root,
        routes: [
            {
                path: "/",
                exact: true,
                component: Splash
            },
            {
                path: URLs.auth,
                component: AuthWrapper,
                routes: [
                    {
                        path: URLs.login,
                        component: Login
                    },
                    {
                        path: URLs.register,
                        component: SignUp
                    },
                    {
                        path: URLs.verify,
                        component: VerifyAccount
                    },
                    {
                        path: URLs.forgotPassword,
                        component: ForgotPassword
                    },
                    {
                        path: URLs.resetPassword,
                        component: ResetPassword
                    },
                    {
                        component: My404Component
                    },
                ],
                loadData: () => Promise.resolve({})
            },
            ...dashboardRoutes,
            {
                path: URLs.logout,
                component: Logout
            },
            {
                path: URLs.deepLinks,
                component: DeepLinks
            },
            {
                component: My404Component
            },
        ]
    }
];

// pass this into ReactDOM.render
const RouteAPI = () => (
    <BrowserRouter>
        <Suspense fallback={<Loading/>}>
            {renderRoutes(routes)}
        </Suspense>
    </BrowserRouter>
);
export default RouteAPI;
