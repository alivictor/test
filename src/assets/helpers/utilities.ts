import UserPhoto from "../../models/userPhoto.model";
import URLs from "../../constants/URLs";
import moment from "moment";

export function convertTZ(date: any, tzString: any) {
    if (date === null) {
        return null;
    }
    return date.toLocaleString("en-US", {timeZone: tzString});
}

export const encryptPhone = (phone: string) => {
    return phone.substring(0, 3) + "*****" + phone.substring(phone.length - 2, phone.length);
};

export const readableCountryCode = (code: string) => {
    let newCode = code.replace('+', '');
    newCode = newCode.replace(' ', '');

    if (newCode.length === 1) {
        newCode = '0' + newCode
    }

    return newCode;
};

export const handleAvatarError = (e: any) => {
    e.target.src = "/assets/img/png/sample-avatar.png";
};

export const checkAvatar = (photo?: UserPhoto) => {
    if (photo) {
        return photo.path ? URLs.base_URL + "/" + photo.path : "/assets/img/png/sample-avatar.png"
    } else {
        return "/assets/img/png/sample-avatar.png"
    }
};

export const stringDate = (str: string) => {
    return str;
};

export const stringDate2 = (str: string) => {
    return str.substr(0, 10);
};

export const stringDateToDMY = (str: string) => {
    return moment(str.substr(0, 10)).month(1).format("DD.MM.YYYY");
};

export const formatter = new Intl.NumberFormat('de-GE', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 0,
    // maximumFractionDigits: 3,
});

export const formatter2 = new Intl.NumberFormat('de-GE', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2,
    // maximumFractionDigits: 3,
});

export const formatter4 = new Intl.NumberFormat('de-GE', {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 2,
    maximumFractionDigits: 4,
});

export const currencyFormatter = (value: any, maxiFractionDigits?: number) => {
    if (value == 0) {
        return value
    } else {
        let minimumFractionDigits, maximumFractionDigits
        if (value !== 0) {
            minimumFractionDigits = value > 1 ? 2 : 4
            maximumFractionDigits = value > 1 ? 2 : 4
        } else {
            minimumFractionDigits = 0
            maximumFractionDigits = 0
        }


        return new Intl.NumberFormat('de-GE', {
            style: 'currency',
            currency: 'EUR',
            minimumFractionDigits: minimumFractionDigits,
            maximumFractionDigits: maxiFractionDigits ? maxiFractionDigits : maximumFractionDigits,
        }).format(value)
    }
}

export const readableNumber = (labelValue: number) => {
    let minimumFractionDigits
    if (labelValue !== 0) {
        minimumFractionDigits = labelValue > 1 ? 2 : 4
    } else {
        minimumFractionDigits = 0
    }
    let nf = new Intl.NumberFormat('de-GE', {
        minimumFractionDigits: minimumFractionDigits,
        maximumFractionDigits: minimumFractionDigits,
    });
    return nf.format(labelValue); // "1,234,567,890"
};

export const percentFormatter = new Intl.NumberFormat('de-GE', {
    style: 'percent',
    minimumFractionDigits: 1,
    // maximumFractionDigits: 2,
});


export const showLevelStatus = (groupTitle: string | undefined, title: string) => {
    if (groupTitle === undefined) {
        return 0
    } else {
        if (groupTitle === title) {
            // return groupTitle;
            return groupTitle;
        } else {
            // return groupTitle + " " + title
            return groupTitle;
        }
    }
};


export const checkInvestStatus = (status: string) => {
    // status === "InCompleted" ||

    return status === "Pending"
        || status === "Rejected"
        || status === "Accepted"
};
