import Axios from 'axios';
// import { History } from './history';
import {toast} from 'react-toastify';
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import URLs from "../../constants/URLs";

const ApiCaller = (apiCallerConfig) => {
  Axios.interceptors.request.use(
    async (config) => {
      config.headers.Authorization = `bearer ${localStorage.getItem('userAuth')}`;
      config.headers.SA_LANG = `${localStorage.getItem('i18nextLng').includes('en') ? 'en' : 'de'}`
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  Axios.interceptors.response.use(
    (response) => {

      return response;
    },
    (err) => {
      if (err.response.data.data) {
        const error = JSON.parse(Decrypt(err.response.data.data))
        if (!error) {
          let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
          toast.error(err)
          return false;
        }

        if (error.status >= 500) {
          toast.error('Server internal error');
          return false;
        }

        if (error.status === 401 && error.path === "/api/auth/login" && error.code === "EXC_CDCCC9A8") {
          let title = localStorage.getItem('i18nextLng')?.includes('en')
            ? 'Please first verify your account using the code sent to your email'
            : 'Bitte überprüfen Sie zuerst Ihr Konto mit dem Code, der an Ihre E-Mail gesendet wurde';
          toast.dismiss();
          toast.error(title);
          localStorage.setItem('id', error.parameters.id)
          localStorage.setItem('email', error.parameters.email)
          setTimeout(() => {
            window.location.href = `${URLs.app_site_URL}/auth/verify`
          }, 2000)
          return false;
        }

        if ((error.status === 406) && (error.code === "EXC_A79DADFE" || error.code === "EXC_83EFF9A3") && error.path !== "/api/auth/login") {
          let title = localStorage.getItem('i18nextLng')?.includes('en')
            ? 'Sorry, your account is no longer active'
            : 'Ihr Konto ist leider nicht mehr aktiv'
          let modalMessage = {
            title,
            button: "OK"
          }
          localStorage.setItem('modalMessage', JSON.stringify(modalMessage));
          setTimeout(() => {
            window.location.href = `${URLs.app_site_URL}/logout`;
          }, 4000)
          return false;
        }

        if ((error.status === 406) && (error.code === "EXC_A79DADFE" || error.code === "EXC_83EFF9A3") && error.path === "/api/auth/login") {
          toast.error(error.client_message || "Error, Try Again!")
          return false;
        }

        toast.error(error.client_message || "Error, Try Again!")
        return error;
      } else {
        toast.error(err.response.data.message || "Error, Try Again!")
        return err.response;
      }
    }
  );
  return Axios.request(apiCallerConfig);
};

export default ApiCaller;
