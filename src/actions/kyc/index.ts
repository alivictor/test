import {Dispatch} from "redux";
import {FileValuesI, ValuesI} from '../../containers/Dashboard/KYC/interfaces'
import {indexApi} from "../../apis";
import URLs from "../../constants/URLs";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {FETCH_KYC_STATUS, SEND_KYC_FAILURE, SEND_KYC_REQUEST, SEND_KYC_SUCCESS} from "../../constants/actionTypes";
import {FetchKYCStatusService, PostKYCRequestService} from "../../apis/kyc";

export type KYCTypes =
  FetchKYCStatus
  | SendKYCRequest
  | SendKYCFailure
  | SendKYCStatus

export const FetchKYCStatus = () => async (dispatch: Dispatch<KYCTypes>) => {
  try {
    const res = await FetchKYCStatusService();
    if (res.status === 200 || res.status === 201) {
      dispatch({
        type: FETCH_KYC_STATUS,
        payload: {
          kyc_status: JSON.parse(Decrypt(res.data.data)).kyc_status,
          kyc_request: JSON.parse(Decrypt(res.data.data)).kyc_request
        }
      })
    }


  } catch (e) {
  }
};

export const SendKYCVerification = (data: ValuesI, files: FileValuesI) => async (dispatch: Dispatch<KYCTypes>) => {
  try {
    let id;
    dispatch({type: SEND_KYC_REQUEST, payload: {}});
    const response = await PostKYCRequestService(data);

    if (response.status === 200 || response.status === 201) {
      id = JSON.parse(Decrypt(response.data.data)).id;
      //  waiting for upload...
      dispatch({
        type: FETCH_KYC_STATUS,
        payload: {
          kyc_status: false,
          kyc_request: JSON.parse(Decrypt(response.data.data))
        }
      });
      dispatch({
        type: SEND_KYC_SUCCESS,
        payload: {
          status: "Waiting"
        }
      });
      let formData = new FormData();
      files.documents && formData.append('documents', files.documents[0]);
      files.bankStatements && formData.append('bankStatements', files.bankStatements[0]);
      files.documentBackSides && formData.append('documentBackSides', files.documentBackSides[0]);
      files.additionals && formData.append('additionals', files.additionals[0]);
      formData.append('id', id);
      indexApi.post(URLs.api_kyc_request_upload, formData)
        .then(res => {
          if (res.status === 200 || res.status === 201) {
            dispatch({
              type: FETCH_KYC_STATUS,
              payload: {
                kyc_status: false,
                kyc_request: JSON.parse(Decrypt(res.data.data))
              }
            });
            dispatch({
              type: SEND_KYC_SUCCESS,
              payload: {
                status: "Success"
              }
            });
          } else {
            dispatch({
              type: SEND_KYC_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(res.data.data)).client_message
              }
            });
          }
        })
        .catch(err => {
          if (err.response) {
            dispatch({
              type: SEND_KYC_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(err.response.data.data)).client_message
              }
            });
          } else {
            let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
            dispatch({
              type: SEND_KYC_FAILURE,
              payload: {
                error: err
              }
            });
          }
        })
    } else {
      if (response) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
          type: SEND_KYC_FAILURE,
          payload: {
            error: response.data.message || err
          }
        });
      }
    }

  } catch (e) {
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SEND_KYC_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

//interfaces

interface FetchKYCStatus {
  type: typeof FETCH_KYC_STATUS,
  payload: { kyc_status: boolean, kyc_request: any | null }
}

interface SendKYCRequest {
  type: typeof SEND_KYC_REQUEST,
  payload: {}
}

interface SendKYCFailure {
  type: typeof SEND_KYC_FAILURE,
  payload: { error: string }
}

interface SendKYCStatus {
  type: typeof SEND_KYC_SUCCESS,
  payload: { status: string }
}
