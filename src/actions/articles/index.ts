import {Dispatch} from "redux";
import {articleApi} from "../../apis";
import URLs from "../../constants/URLs";
import {SET_ARTICLES, SET_ARTICLES_FAILURE} from "../../constants/actionTypes";

export type ArticlesTypes = SetArticles | SetArticlesFailure

export const FetchArticles = () => async (dispatch: Dispatch<ArticlesTypes>) => {
  try {
    //  fetch arbitrage
    articleApi.get(
      // "https://cors-anywhere.herokuapp.com/"+
      URLs.api_get_articles)
      .then(res => {
        //set to reducer
        dispatch({
          type: SET_ARTICLES,
          payload: {
            articles: Array.isArray(res.data) ? res.data:[]
          }
        })
      })
      .catch(err => {
        //handle error
        let error = localStorage.getItem('i18nextLng')?.includes('en') ? 'Failed to load articles' : 'Artikel konnten nicht geladen werden'
        dispatch({
          type: SET_ARTICLES_FAILURE,
          payload: {
            error: error
          }
        });
      })
  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_ARTICLES_FAILURE,
      payload: {
        error: err
      }
    });
  }
};


//interfaces

export interface IArticles {
  id: number;
  cmb2: {
    sa_metabox:{
      news_link: string,
      news_agency_logo: string,
      banner_image: string,
    }
  };
}


export interface SetArticles {
  type: typeof SET_ARTICLES,
  payload: {
    articles: IArticles[]
  }
}
export interface SetArticlesFailure {
  type: typeof SET_ARTICLES_FAILURE,
  payload: {
    error: string
  }
}
