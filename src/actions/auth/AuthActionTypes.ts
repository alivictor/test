import UserModel from "../../models/user.model";
import {LevelItemInterface} from "../../models/levels.model";
import {ReferralHistory, ReferralReducer} from "../../reducers/auth";
import {INotification} from "../../components/DropDowns/NotificationItem";

export const AUTH_LOADING = "AUTH_LOADING";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const FINANCIAL_PROFILE = "FINANCIAL_PROFILE";

export const GET_REFERRAL_INFO = "GET_REFERRAL_INFO";
export const GET_REFERRAL_INFO_REQUEST = "GET_REFERRAL_INFO_REQUEST";
export const GET_REFERRAL_INFO_FAILURE = "GET_REFERRAL_INFO_FAILURE";
export const GET_REFERRAL_HISTORY = "GET_REFERRAL_HISTORY";
export const GET_REFERRAL_HISTORY_REQUEST = "GET_REFERRAL_HISTORY_REQUEST";
export const GET_REFERRAL_HISTORY_FAILURE = "GET_REFERRAL_HISTORY_FAILURE";

export const GET_NOTIFICATIONS_REQUEST = "GET_NOTIFICATIONS_REQUEST";
export const GET_NOTIFICATIONS_SUCCESS = "GET_NOTIFICATIONS_SUCCESS";
export const GET_NOTIFICATIONS_FAILURE = "GET_NOTIFICATIONS_FAILURE";

export const POST_EMAIL_SETTINGS_REQUEST = "POST_EMAIL_SETTINGS_REQUEST";
export const POST_EMAIL_SETTINGS_SUCCESS = "POST_EMAIL_SETTINGS_SUCCESS";
export const POST_EMAIL_SETTINGS_FAILURE = "POST_EMAIL_SETTINGS_FAILURE";

export const GET_FINANCIAL_DETAILS_REQUEST = "GET_FINANCIAL_DETAILS_REQUEST";
export const GET_FINANCIAL_DETAILS_SUCCESS = "GET_FINANCIAL_DETAILS_SUCCESS";
export const GET_FINANCIAL_DETAILS_FAILURE = "GET_FINANCIAL_DETAILS_FAILURE";

export type AuthType = {
  user: UserModel,
  loading: boolean,
  token: string,
  error: string,
  level: LevelItemInterface,
  hasInvest: boolean,
  canInvest: boolean,
  isPremium: boolean,
}

export type FinancialProfileType = {
  totalDeposits: number;
  totalProfits: number;
  totalBalance: number;
  totalSMBToken: number;
  smbPrice: number;
  smbBalanceValue: number;
  smbMarketCap: number;
  pointTotal: number;
  pointReferral: number;
  pointGift: number;
  totalBalanceFromToken: number;
  totalInvestToken: number;
  totalProfitToken: number;

}

export interface AuthLoading {
  type: typeof AUTH_LOADING
}

export interface AuthFail {
  type: typeof AUTH_FAIL,
  payload: AuthType
}

export interface AuthSuccess {
  type: typeof AUTH_SUCCESS,
  payload: AuthType
}

export interface GetFinancialProfile {
  type: typeof FINANCIAL_PROFILE,
  payload: FinancialProfileType
}

export interface GetReferralInfo {
  type: typeof GET_REFERRAL_INFO,
  payload: ReferralReducer
}

export interface GetReferralInfoRequest {
  type: typeof GET_REFERRAL_INFO_REQUEST,
  payload: { loading: boolean, error: string }
}

export interface GetReferralInfoFailure {
  type: typeof GET_REFERRAL_INFO_FAILURE,
  payload: { loading: boolean, error: string }
}

export interface GetReferralHistory {
  type: typeof GET_REFERRAL_HISTORY,
  payload: ReferralHistory
}

export interface GetReferralHistoryRequest {
  type: typeof GET_REFERRAL_HISTORY_REQUEST,
  payload: { history: { loading: boolean, error: string } }
}

export interface GetReferralHistoryFailure {
  type: typeof GET_REFERRAL_HISTORY_FAILURE,
  payload: { history: { loading: boolean, error: string } }
}

export interface GetNotificationsRequest {
  type: typeof GET_NOTIFICATIONS_REQUEST,
  payload: { loading: boolean, error: string }
}

export interface GetNotificationsSuccess {
  type: typeof GET_NOTIFICATIONS_SUCCESS,
  payload: { data: INotification[], reset: boolean, total: number }
}

export interface GetNotificationsFailure {
  type: typeof GET_NOTIFICATIONS_FAILURE,
  payload: { loading: boolean, error: string }
}

export interface PostEmailSettingsRequest {
  type: typeof POST_EMAIL_SETTINGS_REQUEST,
}

export interface PostEmailSettingsSuccess {
  type: typeof POST_EMAIL_SETTINGS_SUCCESS,
  payload: { status: string, user: UserModel }
}

export interface PostEmailSettingsFailure {
  type: typeof POST_EMAIL_SETTINGS_FAILURE,
  payload: { error: string }
}

export interface GetFinancialDetailsRequest {
  type: typeof GET_FINANCIAL_DETAILS_REQUEST,
}

export interface GetFinancialDetailsSuccess {
  type: typeof GET_FINANCIAL_DETAILS_SUCCESS,
  payload: { data: any }
}

export interface GetFinancialDetailsFailure {
  type: typeof GET_FINANCIAL_DETAILS_FAILURE,
  payload: { error: string }
}

export type AuthDispatchTypes =
  AuthLoading | AuthFail | AuthSuccess | GetFinancialProfile |

  GetReferralInfo | GetReferralInfoRequest | GetReferralInfoFailure | GetReferralHistory |
  GetReferralHistoryRequest | GetReferralHistoryFailure | GetNotificationsRequest |
  GetNotificationsSuccess | GetNotificationsFailure  |

  PostEmailSettingsRequest | PostEmailSettingsSuccess | PostEmailSettingsFailure |
  GetFinancialDetailsRequest | GetFinancialDetailsSuccess | GetFinancialDetailsFailure
