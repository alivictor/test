import {Dispatch} from "redux";
import {
    AUTH_FAIL,
    AUTH_SUCCESS,
    AuthDispatchTypes,
    FINANCIAL_PROFILE, GET_FINANCIAL_DETAILS_FAILURE, GET_FINANCIAL_DETAILS_REQUEST, GET_FINANCIAL_DETAILS_SUCCESS,
    GET_NOTIFICATIONS_FAILURE,
    GET_NOTIFICATIONS_REQUEST,
    GET_NOTIFICATIONS_SUCCESS,
    GET_REFERRAL_HISTORY,
    GET_REFERRAL_HISTORY_FAILURE,
    GET_REFERRAL_HISTORY_REQUEST,
    GET_REFERRAL_INFO,
    GET_REFERRAL_INFO_FAILURE,
    POST_EMAIL_SETTINGS_FAILURE,
    POST_EMAIL_SETTINGS_REQUEST,
    POST_EMAIL_SETTINGS_SUCCESS
} from "./AuthActionTypes";
import UserModel from "../../models/user.model";
import {indexApi, uploadApi} from "../../apis";
import {Decrypt, Encrypt} from "../../apis/encryption/encryption/encryption";
import URLs from "../../constants/URLs";
import {LevelItemInterface} from "../../models/levels.model";
import {filterI} from "../../containers/Dashboard/WithDrawal";
import {
    GetCurrentNotificationsService,
    GetReferralHistoryService, GetReferralInfoService, GetUserFinancialProfileService,
    GetUserInfoService,
    PostSettingService, UpdateProfileService
} from "../../apis/auth";
import {GetFinancialDetailsService} from "../../apis/auth";
import {setLocalStorage} from "../../assets/helpers/storage";


export const LoginAction = (data: { user: UserModel, level: LevelItemInterface, hasInvest: boolean, canInvest: boolean, isPremium: boolean }) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        dispatch({
            type: AUTH_SUCCESS,
            payload: {
                loading: false,
                token: localStorage.getItem('userAuth') || '',
                error: "",
                user: data.user,
                level: data.level || undefined,
                hasInvest: data.hasInvest,
                canInvest: data.canInvest,
                isPremium: data.isPremium,
            }
        })

    } catch (e) {
        dispatch({
            type: AUTH_FAIL,
            payload: {
                error: "",
                loading: false,
                token: "",
                user: unknownUser,
                level: unknownLevel,
                hasInvest: false,
                canInvest: false,
                isPremium: false,
            }
        })
    }
};

export const GetUserInfoAction = () => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {

        const res = await GetUserInfoService();
        if (res.status === 200 || res.status === 201) {
            localStorage.setItem('HI', JSON.stringify(JSON.parse(Decrypt(res.data.data)).hasInvest));
            localStorage.setItem('CI', JSON.stringify(JSON.parse(Decrypt(res.data.data)).canInvest));
            localStorage.setItem('IP', JSON.stringify(JSON.parse(Decrypt(res.data.data)).isPremium));
            localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(res.data.data)).user));
            localStorage.setItem('userLevel', JSON.stringify(JSON.parse(Decrypt(res.data.data)).level));
            setLocalStorage('memberships', JSON.parse(Decrypt(res.data.data)).memberships)
            let canWithdrawal = JSON.parse(Decrypt(res.data.data)).canWithdrawal || false;
            let canWithdrawalAt = JSON.parse(Decrypt(res.data.data)).canWithdrawalAt || false;


            localStorage.setItem('canWithdrawal', JSON.stringify(canWithdrawal));
            localStorage.setItem('canWithdrawalAt', JSON.stringify(canWithdrawalAt));
            dispatch({
                type: AUTH_SUCCESS,
                payload: {
                    loading: false,
                    token: localStorage.getItem('userAuth') || "",
                    error: "",
                    user: JSON.parse(Decrypt(res.data.data)).user,
                    level: JSON.parse(Decrypt(res.data.data)).level || undefined,
                    hasInvest: JSON.parse(JSON.parse(Decrypt(res.data.data)).hasInvest),
                    canInvest: JSON.parse(JSON.parse(Decrypt(res.data.data)).canInvest),
                    isPremium: JSON.parse(JSON.parse(Decrypt(res.data.data)).isPremium),
                }
            })

        } else {
            //  set error
            dispatch({
                type: AUTH_FAIL,
                payload: {
                    error: "Unauthorized",
                    loading: false,
                    token: "",
                    user: unknownUser,
                    level: unknownLevel,
                    hasInvest: false,
                    canInvest: false,
                    isPremium: false,
                }
            })
        }

    } catch (e) {
        dispatch({
            type: AUTH_FAIL,
            payload: {
                error: "",
                loading: false,
                token: "",
                user: unknownUser,
                level: unknownLevel,
                hasInvest: false,
                canInvest: false,
                isPremium: false,
            }
        })
    }
};

export const GetUserFinancialProfileAction = () => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        const res = await GetUserFinancialProfileService();
        if (res.status === 200) {
            const financial = JSON.parse(Decrypt(res.data.data));
            dispatch({
                type: FINANCIAL_PROFILE,
                payload: {
                    ...financial
                }
            })
        }


    } catch (e) {

    }
};

export const UpdateProfile = (data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        setSubmitting(true);
        setStatus(null);

        const res = await UpdateProfileService(data);
        if (res.status === 200 || res.status === 201) {
            setSubmitting(false);
            if (resetFormFlag) {
                resetForm();
            }
            setStatus('Updated');
            localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(res.data.data))));
            localStorage.setItem('HI', JSON.stringify(JSON.parse(Decrypt(res.data.data)).hasInvest));
            localStorage.setItem('CI', JSON.stringify(JSON.parse(Decrypt(res.data.data)).canInvest));
            localStorage.setItem('IP', JSON.stringify(JSON.parse(Decrypt(res.data.data)).isPremium));

            dispatch({
                type: AUTH_SUCCESS,
                payload: {
                    loading: false,
                    token: localStorage.getItem('userAuth') || '',
                    error: "",
                    user: JSON.parse(Decrypt(res.data.data)),
                    level: JSON.parse(localStorage.getItem('userLevel') || "") || unknownLevel,
                    hasInvest: JSON.parse(JSON.stringify(JSON.parse(Decrypt(res.data.data)).hasInvest) || 'false'),
                    canInvest: JSON.parse(JSON.stringify(JSON.parse(Decrypt(res.data.data)).canInvest) || 'false'),
                    isPremium: JSON.parse(JSON.stringify(JSON.parse(Decrypt(res.data.data)).isPremium) || 'false'),
                }
            })
        } else {
            setSubmitting(false);
            if (res.data) {
                setStatus("Error, " + JSON.parse(Decrypt(res.data.data)).client_message)
            } else {
                let networkError = localStorage.getItem('i18nextLng')?.includes('en') ? 'Network Error' : 'Netzwerkfehler'
                setStatus("Error, " + networkError)
            }
        }


    } catch (e) {

    }
};

export const UploadAvatar = (data: any, setStatus: any) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        let failed = localStorage.getItem('i18nextLng')?.includes('en') ? 'Failed To Upload !' : 'Upload fehlgeschlagen!'
        let uploading = localStorage.getItem('i18nextLng')?.includes('en') ? 'Uploading...' : 'Hochladen...'
        setStatus(uploading);
        uploadApi.post(URLs.api_upload, data)
            .then(res => {
                if (res.status === 200 || res.status === 201) {
                    localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(res.data.data))));

                    dispatch({
                        type: AUTH_SUCCESS,
                        payload: {
                            loading: false,
                            token: localStorage.getItem('userAuth') || '',
                            error: "",
                            user: JSON.parse(Decrypt(res.data.data)),
                            level: JSON.parse(localStorage.getItem('userLevel') || "") || unknownLevel,
                            hasInvest: JSON.parse(localStorage.getItem('HI') || 'false'),
                            canInvest: JSON.parse(localStorage.getItem('CI') || 'false'),
                            isPremium: JSON.parse(localStorage.getItem('IP') || 'false'),
                        }
                    });
                    let Successfully = localStorage.getItem('i18nextLng')?.includes('en') ? 'Avatar Uploaded Successfully' : 'Avatar erfolgreich hochgeladen'
                    setStatus(Successfully)
                } else {
                    setStatus(failed)
                }
            })
            .catch(err => {
                setStatus(failed)
            })

    } catch (e) {
        let failed = localStorage.getItem('i18nextLng')?.includes('en') ? 'Failed To Upload !' : 'Upload fehlgeschlagen!'
        setStatus(failed)
    }
};

export const GetReferralInfo = () => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        const res = await GetReferralInfoService();
        const data = JSON.parse(Decrypt(res.data.data));
        if (res.status === 200 || res.status === 201) {

            dispatch({
                type: GET_REFERRAL_INFO,
                payload: {
                    links: data.links,
                    totalPoint: data.totalPoint,
                    userCount: data.userCount,
                    loading: false,
                    error: ''
                }
            });
        } else {
            dispatch({
                type: GET_REFERRAL_INFO_FAILURE,
                payload: {
                    loading: false,
                    error: data.message
                }
            });
        }

    } catch (e) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_REFERRAL_INFO_FAILURE,
            payload: {
                loading: false,
                error: err
            }
        });
    }
};

export const GetReferralHistory = (filter: filterI) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        dispatch({type: GET_REFERRAL_HISTORY_REQUEST, payload: {history: {loading: true, error: ''}}});
        let slug = `take=${filter.take}&skip=${filter.skip}`;
        const res = await GetReferralHistoryService(slug);
        const data = JSON.parse(Decrypt(res.data.data));
        if (res.status === 200 || res.status === 201) {
            dispatch({
                type: GET_REFERRAL_HISTORY,
                payload: {
                    items: data.data,
                    loading: false,
                    total: data.total,
                    error: '',
                }
            });
        } else {
            dispatch({
                type: GET_REFERRAL_HISTORY_FAILURE,
                payload: {
                    history: {
                        loading: false,
                        error: data.client_message,
                    }
                }
            });
        }


    } catch (e) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_REFERRAL_HISTORY_FAILURE,
            payload: {
                history: {
                    loading: false,
                    error: err
                }
            }
        });

    }
};

export const updateFirebaseToken = (token: string) => async () => {
    try {
        indexApi.put(URLs.api_update_firebase_token, {"data": Encrypt(JSON.stringify({firebaseToken: token}))})
            .then()
            .catch()

    } catch (e) {
    }
};

// @ts-ignore
export const fetchNotifications = (data?: { take: number, skip: number }, reset: boolean) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    try {
        dispatch({
            type: GET_NOTIFICATIONS_REQUEST,
            payload: {
                error: "",
                loading: true
            }
        });
        let slug = `take=${data ? data.take : 5}&skip=${data ? data.skip : 0}`;
        const res = await GetCurrentNotificationsService(slug);
        const notifications = JSON.parse(Decrypt(res.data.data));
        if (res.status === 200 || res.status === 201) {

            dispatch({
                type: GET_NOTIFICATIONS_SUCCESS,
                payload: {
                    data: notifications.data,
                    reset: reset,
                    total: notifications.total
                }
            });
        } else {
            dispatch({
                type: GET_NOTIFICATIONS_FAILURE,
                payload: {
                    loading: false,
                    error: notifications.client_message,
                }
            });
        }
    } catch (e) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_NOTIFICATIONS_FAILURE,
            payload: {
                loading: false,
                error: err
            }
        });
    }
};

export const UpdateSettings = (postData: any) => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    dispatch({type: POST_EMAIL_SETTINGS_REQUEST});
    try {

        const response = await PostSettingService(postData);
        if (response.status === 200 || response.status === 201) {
            // const data = JSON.parse(Decrypt(response.data.data));
            localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(response.data.data))));


            dispatch({
                type: POST_EMAIL_SETTINGS_SUCCESS,
                payload: {
                    status: "Updated",
                    user: JSON.parse(Decrypt(response.data.data))
                }
            });
        } else {
            const data = JSON.parse(Decrypt(response.data.data));
            dispatch({
                type: POST_EMAIL_SETTINGS_FAILURE,
                payload: {
                    error: "Error," + data.client_message
                }
            });
        }


    } catch (e) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: POST_EMAIL_SETTINGS_FAILURE,
            payload: {
                error: err
            }
        });
    }
};

export const GetFinancialDetails = () => async (dispatch: Dispatch<AuthDispatchTypes>) => {
    dispatch({type: GET_FINANCIAL_DETAILS_REQUEST});
    try {
        const response = await GetFinancialDetailsService();
        console.log(response);

        if (response.status === 200 || response.status === 201) {

            dispatch({
                type: GET_FINANCIAL_DETAILS_SUCCESS,
                payload: {
                    data: JSON.parse(Decrypt(response.data.data))
                }
            });
        } else {
            const data = JSON.parse(Decrypt(response.data.data));
            dispatch({
                type: GET_FINANCIAL_DETAILS_FAILURE,
                payload: {
                    error: "Error," + data.client_message
                }
            });
        }

    } catch (e) {
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_FINANCIAL_DETAILS_FAILURE,
            payload: {
                error: err
            }
        });
    }
};

//interface
export interface updateProfileI {
    params: any,
    setStatus: any,
    setSubmitting: any,
}

export interface ReferralInfoResponse {
    last5User: string[],
    userCount: number,
    totalPoint: number,
    totalBalance: number
}


//variables

export const unknownUser = {
    id: "",
    mail: undefined,
    firstName: undefined,
    lastName: undefined,
    phone: undefined,
    createdAt: undefined,
    country: undefined,
    registerMembership: "",
    point: 0,
    photo: {
        path: "",
        name: "",
    },
    kyc: {verify: false},
    promotional: undefined,
    wallet: undefined,
    eurNameOfTheClient: undefined,
    eurAddressOfTheClient: undefined,
    eurBankName: undefined,
    eurBankAddress: undefined,
    eurIBAN: undefined,
    eurSwiftOrBIC: undefined,
    eurCountryOfTheClient: undefined,
    eurCountryOfTheBank: undefined,
    eurComments: undefined,
    firebaseNotificationId: undefined,
    gender: undefined,
    gender_label: undefined,
    balanceBlocked: 0,
    role: "",
    balance: 0,
    setting: {
        emailLanguage: ""
    },
    fullName: "",
    eurCountryBank: undefined,
    eurCountryClient: undefined,
    startedAt: "",
    startedAtToken: "",
};

export const unknownLevel: LevelItemInterface = {
    id: 0,
    title: "",
    displayTitle: "",
    displayAmount: "",
    market: "",
    profitAverage: 0,
    minScore: "0",
    maxScore: "0",
    groupMaxRange: "",
    groupMinRange: "",
    minMonthlyProfit: "0",
    maxMonthlyProfit: "0",
    minYearlyProfit: "0",
    maxYearlyProfit: "0",
    eurBankTransfer: false,
    withdrawals24Support: false,
    accessToOurInstitutionalAccounts: false,
    accessToTheOtcPreciousMetalsMarket: false,
    accessToTheOtcOilAndCryptoMarket: false,
    accessToTheRealEstateMarkets: false,
    accessToTheOtcForexMarket: false,
    additionalOtcMarketsWithHigherMargins: false,
    additionalSMTTokenBonusPerTrade: false,
    conciergeServiceForManualHigherMarginTrades: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    isActive: false,
    groupId: 5,
    color: {
        text: "FE5578",
        background: "FEEAE7"
    },
};
