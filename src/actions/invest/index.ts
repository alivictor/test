import {Dispatch} from "redux";
import URLs from "../../constants/URLs";
import {filterI} from "../../containers/Dashboard/WithDrawal";
import {Decrypt, Encrypt} from "../../apis/encryption/encryption/encryption";
import {FileI} from "../../containers/Dashboard/Invest";
import UserModel from "../../models/user.model";
import axios from "axios";
import {
  CHANGE_INVEST_STEP,
  GET_TOKEN_INVESTS_FAILURE,
  GET_TOKEN_INVESTS_REQUEST,
  GET_TOKEN_INVESTS_SUCCESS,
  SEND_INVEST_FAILURE,
  SEND_INVEST_REQUEST,
  SEND_INVEST_SUCCESS,
  SET_ACTIVE_INVEST,
  SET_INVESTS,
  SET_INVESTS_FAILURE,
  SET_INVESTS_REQUEST,
  UPLOAD_INVEST_FILE_FAILURE,
  UPLOAD_INVEST_FILE_REQUEST,
  UPLOAD_INVEST_FILE_SUCCESS
} from "../../constants/actionTypes";

import {FetchInvestsService, FetchTokenInvestsService} from "../../apis/invest";
import {unknownUser} from "../auth";
import {setLocalStorage} from "../../assets/helpers/storage";

export type InvestsTypes =
  SetInvests
  | UploadInvestRequest
  | UploadInvestFailure
  | UploadInvestSuccess
  | SetInvestsFailure
  | SendInvestFailure
  | SendInvestRequest
  | SendInvestSuccess
  | SetInvestsRequest
  | changeStepNumber
  | setActiveInvest
  | getTokenInvestsRequest
  | getTokenInvestsSuccess
  | getTokenInvestsFailure

export const FetchInvests = (filter: filterI) => async (dispatch: Dispatch<InvestsTypes>) => {
  try {
    dispatch({type: SET_INVESTS_REQUEST, payload: {}});
    //  fetch arbitrage
    // &search=${data.search}
    let slug = `take=${filter.take}&skip=${filter.skip}`;

    const res = await FetchInvestsService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));
      //set to reducer
      dispatch({
        type: SET_INVESTS,
        payload: {
          items: data.data,
          total: data.total,
          page: filter.skip / filter.take + 1,
          reset: true,
        }
      })
    } else {
      dispatch({
        type: SET_INVESTS_FAILURE,
        payload: {
          error: 'Internet access error.'
        }
      });
    }

  } catch (e) {
    //handle error
    dispatch({
      type: SET_INVESTS_FAILURE,
      payload: {
        error: 'Internet access error.'
      }
    });
  }
};

export const sendInvestRequest = (data: investRequest, reset: boolean) => async (dispatch: Dispatch<InvestsTypes>) => {
  try {
    dispatch({type: SEND_INVEST_REQUEST, payload: {}});
    if (data.amount === 0 || data.amount < 0) {
      dispatch({
        type: SEND_INVEST_FAILURE,
        payload: {
          error: "Amount Can't be zero or less.",
        }
      });
    } else {
      const lang = localStorage.getItem('i18nextLng')?.includes('en') ? 'en' : 'de'

      const investApi = axios.create({
        baseURL: URLs.api_base_URL,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Authorization': `bearer ${localStorage.getItem('userAuth')}`,
          'SA_LANG': lang,
        }
      });
      investApi.post(URLs.api_invest_request, {"data": Encrypt(JSON.stringify(data))})
        .then(res => {
          if (res.status === 200 || res.status === 201) {
            const invest = JSON.parse(Decrypt(res.data.data));
            localStorage.setItem('HI', 'true');
            //set item localstorage: reference, amount, iban, bic , invest id and step number
            // localStorage.setItem('active_invest', invest);

            setLocalStorage('active_invest', invest)
            dispatch({
              type: SEND_INVEST_SUCCESS,
              payload: {
                status: 'Success',
                data: ""
              }
            });

            dispatch({
              type: CHANGE_INVEST_STEP,
              payload: {
                stepNumber: 2
              }
            });

            dispatch({
              type: SET_ACTIVE_INVEST,
              payload: {
                data: invest
              }
            });

          } else {
            dispatch({
              type: SEND_INVEST_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(res.data.data)).client_message,
              }
            });
          }

        })
        .catch(err => {
          if (err.response.data && err.response.data.data) {
            dispatch({
              type: SEND_INVEST_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(err.response.data.data)).client_message,
              }
            });
          } else {
            dispatch({
              type: SEND_INVEST_FAILURE,
              payload: {
                error: err.response.data.message,
              }
            });
          }
        })
    }
  } catch (err) {
    dispatch({
      type: SEND_INVEST_FAILURE,
      payload: {
        error: 'Error',
      }
    });
  }
};

export const uploadInvestDocument = (id: number, file: FileI) => async (dispatch: Dispatch<InvestsTypes>) => {
  try {
    dispatch({type: UPLOAD_INVEST_FILE_REQUEST, payload: {}});
    let formData = new FormData();

    if (file && file.file !== undefined) {
      formData.append("file", file.file);
      formData.append("id", id.toString());
      const lang = localStorage.getItem('i18nextLng')?.includes('en') ? 'en' : 'de'

      const investApi = axios.create({
        baseURL: URLs.api_base_URL,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Authorization': `bearer ${localStorage.getItem('userAuth')}`,
          'SA_LANG': lang,
        }
      });
      investApi.post(URLs.api_invest_upload, formData)
        .then(res => {
          if (res.status === 200 || res.status === 201) {
            localStorage.setItem('HI', 'true');
            const invest = JSON.parse(Decrypt(res.data.data));

            setLocalStorage('active_invest', invest)
            dispatch({
              type: UPLOAD_INVEST_FILE_SUCCESS,
              payload: {
                status: 'Success',
              }
            });

          } else {
            dispatch({
              type: UPLOAD_INVEST_FILE_FAILURE,
              payload: {
                error: 'Error',
              }
            });
          }
        })
        .catch(err => {
          if (err.response.data.data) {
            dispatch({
              type: UPLOAD_INVEST_FILE_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(err.response.data.data)).client_message,
              }
            });
          } else {
            dispatch({
              type: UPLOAD_INVEST_FILE_FAILURE,
              payload: {
                error: err.response.data.message,
              }
            });
          }
        })
    } else {
      let text = localStorage.getItem('i18nextLng')?.includes('en') ? 'Please upload the payment receipt file' : 'Bitte laden Sie die Zahlungsbelegdatei hoch'
      dispatch({
        type: UPLOAD_INVEST_FILE_FAILURE,
        payload: {
          error: text,
        }
      });
    }

  } catch (err) {
    dispatch({
      type: UPLOAD_INVEST_FILE_FAILURE,
      payload: {
        error: 'Error',
      }
    });
  }
};

export const clearInvestStates = () => async (dispatch: Dispatch<InvestsTypes>) => {
  try {
    dispatch({
      type: UPLOAD_INVEST_FILE_SUCCESS,
      payload: {
        status: '',
        data: ""
      }
    });
    dispatch({
      type: SEND_INVEST_SUCCESS,
      payload: {
        status: '',
        data: ""
      }
    });
  } catch (err) {
    dispatch({
      type: UPLOAD_INVEST_FILE_FAILURE,
      payload: {
        error: 'Error',
      }
    });
  }
};

export const FetchTokenInvests = (filter: filterI) => async (dispatch: Dispatch<InvestsTypes>) => {
  try {
    dispatch({type: GET_TOKEN_INVESTS_REQUEST, payload: {}});

    let slug = `take=${filter.take}&skip=${filter.skip}`;

    const res = await FetchTokenInvestsService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));
      //set to reducer
      dispatch({
        type: GET_TOKEN_INVESTS_SUCCESS,
        payload: {
          items: data.data,
          total: data.total,
          page: filter.skip / filter.take + 1,
          reset: true,
        }
      })
    } else {
      dispatch({
        type: GET_TOKEN_INVESTS_FAILURE,
        payload: {
          error: 'Internet access error.'
        }
      });
    }

  } catch (e) {
    //handle error
    dispatch({
      type: GET_TOKEN_INVESTS_FAILURE,
      payload: {
        error: 'Internet access error.'
      }
    });
  }
};


//interfaces
export interface IInvestRequests {
  id: number,
  activities: any,
  createdAt: string,
  isActive: boolean,
  lastActivity: {
    amountAccepted: string,
    amount: string,
    description: string,
    status: string,
    createdAt: string,
  },
  paidAt: string,
  saBIC: string,
  saIBAN: string,
  saAccountOwner: string,
  saBank: string,
  saTrackingCode: string,
  trackingCode: string,
  user: UserModel
}

export const activeInvestDefaultState = {
  id: 0,
  activities: [],
  createdAt: "",
  isActive: true,
  lastActivity: {
    amountAccepted: "0",
    amount: "0",
    description: "",
    status: "Disable",
    createdAt: "",
  },
  paidAt: "",
  saBIC: "",
  saIBAN: "",
  saTrackingCode: "",
  saAccountOwner: "",
  saBank: "",
  trackingCode: "",
  user: unknownUser
};

export interface changeStepNumber {
  type: typeof CHANGE_INVEST_STEP,
  payload: { stepNumber: number }
}

export interface setActiveInvest {
  type: typeof SET_ACTIVE_INVEST,
  payload: { data: IInvestRequests }
}

export interface investRequest {
  amount: number,
  description: string
}

export interface SetInvestsRequest {
  type: typeof SET_INVESTS_REQUEST,
  payload: {}
}

export interface UploadInvestRequest {
  type: typeof UPLOAD_INVEST_FILE_REQUEST,
  payload: {}
}

export interface UploadInvestFailure {
  type: typeof UPLOAD_INVEST_FILE_FAILURE,
  payload: { error: string }
}

export interface UploadInvestSuccess {
  type: typeof UPLOAD_INVEST_FILE_SUCCESS,
  payload: { status: string }
}

export interface SetInvests {
  type: typeof SET_INVESTS,
  payload: {
    items: IInvestRequests[],
    total: number,
    page: number,
    reset: boolean
  }
}

export interface SetInvestsFailure {
  type: typeof SET_INVESTS_FAILURE,
  payload: {
    error: string
  }
}


export interface SendInvestRequest {
  type: typeof SEND_INVEST_REQUEST,
  payload: {}
}

export interface SendInvestSuccess {
  type: typeof SEND_INVEST_SUCCESS,
  payload: {
    status: string,
    data: any
  }
}

export interface SendInvestFailure {
  type: typeof SEND_INVEST_FAILURE,
  payload: {
    error: string
  }
}

export interface getTokenInvestsRequest {
  type: typeof GET_TOKEN_INVESTS_REQUEST,
  payload: {}
}

export interface getTokenInvestsSuccess {
  type: typeof GET_TOKEN_INVESTS_SUCCESS,
  payload: {
    items: any[],
    total: number,
    page: number,
    reset: boolean
  }
}

export interface getTokenInvestsFailure {
  type: typeof GET_TOKEN_INVESTS_FAILURE,
  payload: {
    error: string
  }
}
