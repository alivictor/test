import {Dispatch} from "redux";
// import URLs from "../../constants/URLs";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
// import axios from "axios";
import {SET_FAQ, SET_FAQ_FAILURE, SET_FAQ_REQUEST} from "../../constants/actionTypes";
import {FetchFAQService} from "../../apis/faq";

export type FAQTypes =
    SetFAQ
    | SetFAQFailure
    | SetFAQRequest

export const FetchFAQ = (lang: string) => async (dispatch: Dispatch<FAQTypes>) => {
    try {
        dispatch({type: SET_FAQ_REQUEST, payload: {}});

        const res = await FetchFAQService();
        if (res.status === 200 || res.status === 201) {
            dispatch({
                type: SET_FAQ,
                payload: {
                    items: JSON.parse(Decrypt(res.data.data)),
                }
            })
        }else {
            dispatch({
                type: SET_FAQ_FAILURE,
                payload: {
                    error: JSON.parse(Decrypt(res.data.data)).message
                }
            });
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: SET_FAQ_FAILURE,
            payload: {
                error: err
            }
        });
    }
};

//interfaces
export interface SetFAQRequest {
    type: typeof SET_FAQ_REQUEST,
    payload: {}
}

export interface SetFAQ {
    type: typeof SET_FAQ,
    payload: {
        items: FAQItemI[],
    }
}

export interface SetFAQFailure {
    type: typeof SET_FAQ_FAILURE,
    payload: {
        error: string
    }
}

export interface FAQItemI {
    title: string,
    content: string
}
