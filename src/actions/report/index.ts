import {Dispatch} from "redux";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {
    GET_MARKET_DETAIL_FAILURE,
    GET_MARKET_DETAIL_REQUEST,
    GET_MARKET_DETAIL_SUCCESS,
    GET_PROFIT_DATA_FAILURE,
    GET_PROFIT_DATA_REQUEST,
    GET_PROFIT_DATA_SUCCESS,
    GET_RESOURCE_DETAIL_FAILURE,
    GET_RESOURCE_DETAIL_REQUEST,
    GET_RESOURCE_DETAIL_SUCCESS,
    GET_RESOURCE_DETAIL_WITH_DATE_FAILURE,
    GET_RESOURCE_DETAIL_WITH_DATE_REQUEST,
    GET_RESOURCE_DETAIL_WITH_DATE_SUCCESS,
    GET_USER_FULL_DETAILS_FAILURE,
    GET_USER_FULL_DETAILS_REQUEST,
    GET_USER_FULL_DETAILS_SUCCESS,
    GET_USER_TOKEN_FULL_DETAILS_FAILURE,
    GET_USER_TOKEN_FULL_DETAILS_REQUEST,
    GET_USER_TOKEN_FULL_DETAILS_SUCCESS,
    GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE,
    GET_USER_TOKEN_SMB_PRICE_CHART_REQUEST,
    GET_USER_TOKEN_SMB_PRICE_CHART_SUCCESS,

} from "../../constants/actionTypes";
import {
    GetMarketDetailService, GetProfitDataService,
    GetResourceDetailService,
    GetResourceDetailWithDateService,
    GetUserFullDetailsService, GetUserTokenFullDetailsService, GetUserTokenSMBPriceChartService
} from "../../apis/report";
import {setLocalStorage} from "../../assets/helpers/storage";
import {FetchLiveSMBGraphService} from "../../apis/arbitrage";

export type ReportActionTypes =
    getUserFullDetailsRequest | getUserFullDetailsSuccess | getUserFullDetailsFailure |
    getMarketDetailFailure | getMarketDetailRequest | getMarketDetailSuccess |
    getResourceDetailFailure | getResourceDetailSuccess | getResourceDetailRequest |
    getResourceDetailWithDateFailure | getResourceDetailWithDateSuccess | getResourceDetailWithDateRequest |
    getProfitDataFailure | getProfitDataSuccess | getProfitDataRequest |
    getUserTokenFullDetailsRequest | getUserTokenFullDetailsSuccess | getUserTokenFullDetailsFailure |
    GetUserTokenSMBPriceChartRequest | GetUserTokenSMBPriceChartSuccess | GetUserTokenSMBPriceChartFailure

export const GetUserFullDetails = () => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_USER_FULL_DETAILS_REQUEST,
    })
    try {
        //
        const res = await GetUserFullDetailsService();
        if (res.status === 200) {
            setLocalStorage('userFullDetails', JSON.parse(Decrypt(res.data.data)))
            dispatch({
                type: GET_USER_FULL_DETAILS_SUCCESS,
                payload: JSON.parse(Decrypt(res.data.data))
            })
        } else {
            dispatch({
                type: GET_USER_FULL_DETAILS_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_USER_FULL_DETAILS_FAILURE,
            payload: err
        })
    }
};

export const GetMarketDetail = () => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_MARKET_DETAIL_REQUEST,
    })
    try {
        const res = await GetMarketDetailService();
        if (res.status === 200) {
            setLocalStorage('marketDetail', JSON.parse(Decrypt(res.data.data)))
            dispatch({
                type: GET_MARKET_DETAIL_SUCCESS,
                payload: JSON.parse(Decrypt(res.data.data))
            })
        } else {
            dispatch({
                type: GET_MARKET_DETAIL_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_MARKET_DETAIL_FAILURE,
            payload: err
        })
    }
};

export const GetResourceDetail = () => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_RESOURCE_DETAIL_REQUEST,
    })
    try {
        const res = await GetResourceDetailService();
        if (res.status === 200) {
            dispatch({
                type: GET_RESOURCE_DETAIL_SUCCESS,
                payload: JSON.parse(Decrypt(res.data.data))
            })
        } else {
            dispatch({
                type: GET_RESOURCE_DETAIL_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_RESOURCE_DETAIL_FAILURE,
            payload: err
        })
    }
};

export const GetResourceDetailWithDate = (postData: any) => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_RESOURCE_DETAIL_WITH_DATE_REQUEST,
    })
    try {
        const res = await GetResourceDetailWithDateService(postData);
        if (res.status === 200) {
            dispatch({
                type: GET_RESOURCE_DETAIL_WITH_DATE_SUCCESS,
                payload: {
                    data: JSON.parse(Decrypt(res.data.data)),
                    type: postData.type
                }
            })
        } else {
            dispatch({
                type: GET_RESOURCE_DETAIL_WITH_DATE_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_RESOURCE_DETAIL_WITH_DATE_FAILURE,
            payload: err
        })
    }
};

export const GetProfitData = (postData: any) => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_PROFIT_DATA_REQUEST,
    })
    try {
        const res = await GetProfitDataService(postData);
        if (res.status === 200) {
            dispatch({
                type: GET_PROFIT_DATA_SUCCESS,
                payload: {
                    data: JSON.parse(Decrypt(res.data.data)),
                    type: postData.type
                }
            })

        } else {
            dispatch({
                type: GET_PROFIT_DATA_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_PROFIT_DATA_FAILURE,
            payload: err
        })
    }
};


//USER TOKEN ACTIONS
export const GetUserTokenFullDetails = () => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_USER_TOKEN_FULL_DETAILS_REQUEST,
    })
    try {
        //
        const res = await GetUserTokenFullDetailsService();
        if (res.status === 200) {
            setLocalStorage('userTokenFullDetails', JSON.parse(Decrypt(res.data.data)))
            dispatch({
                type: GET_USER_TOKEN_FULL_DETAILS_SUCCESS,
                payload: JSON.parse(Decrypt(res.data.data))
            })
        } else {
            dispatch({
                type: GET_USER_TOKEN_FULL_DETAILS_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_USER_TOKEN_FULL_DETAILS_FAILURE,
            payload: err
        })
    }
};


export const GetUserTokenSMBPriceChart = () => async (dispatch: Dispatch<ReportActionTypes>) => {
    dispatch({
        type: GET_USER_TOKEN_SMB_PRICE_CHART_REQUEST,
    })
    try {
        //
        // const res = await GetUserTokenSMBPriceChartService();
        const res = await FetchLiveSMBGraphService();

        if (res.status === 200) {
            dispatch({
                type: GET_USER_TOKEN_SMB_PRICE_CHART_SUCCESS,
                payload: JSON.parse(Decrypt(res.data.data))
            })
        } else {
            dispatch({
                type: GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE,
                payload: "Error"
            })
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE,
            payload: err
        })
    }
};




//interfaces
export interface getUserFullDetailsRequest {
    type: typeof GET_USER_FULL_DETAILS_REQUEST,
}

export interface getUserFullDetailsSuccess {
    type: typeof GET_USER_FULL_DETAILS_SUCCESS,
    payload: any
}

export interface getUserFullDetailsFailure {
    type: typeof GET_USER_FULL_DETAILS_FAILURE,
    payload: any
}

export interface getMarketDetailRequest {
    type: typeof GET_MARKET_DETAIL_REQUEST,
}

export interface getMarketDetailSuccess {
    type: typeof GET_MARKET_DETAIL_SUCCESS,
    payload: any
}

export interface getMarketDetailFailure {
    type: typeof GET_MARKET_DETAIL_FAILURE,
    payload: any
}

export interface getResourceDetailRequest {
    type: typeof GET_RESOURCE_DETAIL_REQUEST,
}

export interface getResourceDetailSuccess {
    type: typeof GET_RESOURCE_DETAIL_SUCCESS,
    payload: any
}

export interface getResourceDetailFailure {
    type: typeof GET_RESOURCE_DETAIL_FAILURE,
    payload: any
}

export interface getResourceDetailWithDateRequest {
    type: typeof GET_RESOURCE_DETAIL_WITH_DATE_REQUEST,
}

export interface getResourceDetailWithDateSuccess {
    type: typeof GET_RESOURCE_DETAIL_WITH_DATE_SUCCESS,
    payload: any
}

export interface getResourceDetailWithDateFailure {
    type: typeof GET_RESOURCE_DETAIL_WITH_DATE_FAILURE,
    payload: any
}


export interface getProfitDataRequest {
    type: typeof GET_PROFIT_DATA_REQUEST,
}

export interface getProfitDataSuccess {
    type: typeof GET_PROFIT_DATA_SUCCESS,
    payload: any
}

export interface getProfitDataFailure {
    type: typeof GET_PROFIT_DATA_FAILURE,
    payload: any
}


//USER TOKEN interfaces
export interface getUserTokenFullDetailsRequest {
    type: typeof GET_USER_TOKEN_FULL_DETAILS_REQUEST,
}

export interface getUserTokenFullDetailsSuccess {
    type: typeof GET_USER_TOKEN_FULL_DETAILS_SUCCESS,
    payload: any
}

export interface getUserTokenFullDetailsFailure {
    type: typeof GET_USER_TOKEN_FULL_DETAILS_FAILURE,
    payload: any
}


export interface GetUserTokenSMBPriceChartRequest {
    type: typeof GET_USER_TOKEN_SMB_PRICE_CHART_REQUEST,
}

export interface GetUserTokenSMBPriceChartSuccess {
    type: typeof GET_USER_TOKEN_SMB_PRICE_CHART_SUCCESS,
    payload: any
}

export interface GetUserTokenSMBPriceChartFailure {
    type: typeof GET_USER_TOKEN_SMB_PRICE_CHART_FAILURE,
    payload: any
}

