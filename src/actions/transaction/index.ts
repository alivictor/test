import {Dispatch} from "redux";
import {filterI} from "../../containers/Dashboard/WithDrawal";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {TransactionDto} from "../../models/responses/transaction.model";
import {
  GET_TOKEN_TRANSACTION_HISTORY_FAILURE,
  GET_TOKEN_TRANSACTION_HISTORY_REQUEST,
  GET_TOKEN_TRANSACTION_HISTORY_SUCCESS,
  SET_TRANSACTION_HISTORY,
  SET_TRANSACTION_HISTORY_FAILURE,
  SET_TRANSACTION_HISTORY_REQUEST
} from "../../constants/actionTypes";
import {FetchTokenTransactionHistoryService, FetchTransactionHistoryService} from "../../apis/transaction";

export type TransactionTypes =
  SetTransactionRequest
  | SetTransaction
  | SetTransactionFailure
  | getTokenTransactionRequest
  | getTokenTransactionSuccess
  | getTokenTransactionFailure

export const FetchTransactionHistory = (data: filterI) => async (dispatch: Dispatch<TransactionTypes>) => {
  try {
    dispatch({type: SET_TRANSACTION_HISTORY_REQUEST, payload: {}});

    let slug = `take=${data.take}&skip=${data.skip}`;
    const res = await FetchTransactionHistoryService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));

      //set to reducer
      dispatch({
        type: SET_TRANSACTION_HISTORY,
        payload: {
          items: data.data,
          total: data.total,
          reset: true,
        }
      })
    } else {
      // handle error
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: SET_TRANSACTION_HISTORY_FAILURE,
        payload: {
          error: err
        }
      });
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_TRANSACTION_HISTORY_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

export const FetchTokenTransactionHistory = (data: filterI) => async (dispatch: Dispatch<TransactionTypes>) => {
  try {
    dispatch({type: GET_TOKEN_TRANSACTION_HISTORY_REQUEST, payload: {}});

    let slug = `take=${data.take}&skip=${data.skip}`;
    const res = await FetchTokenTransactionHistoryService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));

      //set to reducer
      dispatch({
        type: GET_TOKEN_TRANSACTION_HISTORY_SUCCESS,
        payload: {
          items: data.data,
          total: data.total,
          reset: true,
        }
      })
    } else {
      // handle error
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: GET_TOKEN_TRANSACTION_HISTORY_FAILURE,
        payload: {
          error: err
        }
      });
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_TRANSACTION_HISTORY_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

//interfaces
export interface SetTransactionRequest {
  type: typeof SET_TRANSACTION_HISTORY_REQUEST,
  payload: {}
}

export interface SetTransaction {
  type: typeof SET_TRANSACTION_HISTORY,
  payload: {
    items: TransactionDto[],
    total: number,
    reset: boolean
  }
}

export interface SetTransactionFailure {
  type: typeof SET_TRANSACTION_HISTORY_FAILURE,
  payload: {
    error: string
  }
}

//
export interface getTokenTransactionRequest {
  type: typeof GET_TOKEN_TRANSACTION_HISTORY_REQUEST,
  payload: {}
}

export interface getTokenTransactionSuccess {
  type: typeof GET_TOKEN_TRANSACTION_HISTORY_SUCCESS,
  payload: {
    items: TransactionDto[],
    total: number,
    reset: boolean
  }
}

export interface getTokenTransactionFailure {
  type: typeof GET_TOKEN_TRANSACTION_HISTORY_FAILURE,
  payload: {
    error: string
  }
}
