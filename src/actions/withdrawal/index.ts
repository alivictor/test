import {Dispatch} from "redux";
import {indexApi} from "../../apis";
import URLs from "../../constants/URLs";
import {filterI} from "../../containers/Dashboard/WithDrawal";
import {Decrypt, Encrypt} from "../../apis/encryption/encryption/encryption";
import {
  SEND_WITHDRAWAL_FAILURE,
  SEND_WITHDRAWAL_REQUEST, SEND_WITHDRAWAL_SUCCESS,
  SET_WITHDRAWALS,
  SET_WITHDRAWALS_FAILURE,
  SET_WITHDRAWALS_REQUEST
} from "../../constants/actionTypes";
import {FetchWithdrawalsService} from "../../apis/withdrawal";

export type WithdrawalsTypes =
  SetWithdrawals
  | SetWithdrawalsFailure
  | SendWithdrawalFailure
  | SendWithdrawalRequest
  | SendWithdrawalSuccess
  | SetWithdrawalsRequest

export const FetchWithdrawals = (data: filterI) => async (dispatch: Dispatch<WithdrawalsTypes>) => {
  try {
    dispatch({type: SET_WITHDRAWALS_REQUEST, payload: {}});
    //  fetch arbitrage
    // &search=${data.search}
    let slug = `take=${data.take}&skip=${data.skip}`;
    const res = await FetchWithdrawalsService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));

      //set to reducer
      dispatch({
        type: SET_WITHDRAWALS,
        payload: {
          items: data.data,
          total: data.total,
          reset: true,
        }
      })
    } else {
      // handle error
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: SET_WITHDRAWALS_FAILURE,
        payload: {
          error: err
        }
      });
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_WITHDRAWALS_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

export const sendWithdrawalRequest = (data: withDrawRequest, reset: boolean) => async (dispatch: Dispatch<WithdrawalsTypes>) => {
  try {
    dispatch({type: SEND_WITHDRAWAL_REQUEST, payload: {}});
    if (data.amount === 0) {
      let amount = localStorage.getItem('i18nextLng')?.includes('en') ? "Amount Can't be zero." : 'Betrag Kann nicht Null sein.'
      dispatch({
        type: SEND_WITHDRAWAL_FAILURE,
        payload: {
          error: amount,
        }
      });
    } else {
      indexApi.post(URLs.api_withdrawal_request, {"data": Encrypt(JSON.stringify(data))})
        .then(res => {
          const data = JSON.parse(Decrypt(res.data.data));
          dispatch({
            type: SEND_WITHDRAWAL_SUCCESS,
            payload: {
              status: 'Success',
            }
          });
          dispatch({
            type: SET_WITHDRAWALS,
            payload: {
              items: [data],
              total: 0,
              reset: reset
            }
          })
        })
        .catch(err => {
          if (err.response.data.data) {
            dispatch({
              type: SEND_WITHDRAWAL_FAILURE,
              payload: {
                error: JSON.parse(Decrypt(err.response.data.data)).client_message,
              }
            });
          } else {
            dispatch({
              type: SEND_WITHDRAWAL_FAILURE,
              payload: {
                error: err.response.data.message,
              }
            });
          }
        })
    }
  } catch (err) {
    dispatch({
      type: SEND_WITHDRAWAL_FAILURE,
      payload: {
        error: 'Error',
      }
    });
  }
};


//interfaces

export interface IWithdrawalRequests {
  id: number;

}

export interface withDrawRequest {
  amount: number
}

export interface SetWithdrawalsRequest {
  type: typeof SET_WITHDRAWALS_REQUEST,
  payload: {}
}

export interface SetWithdrawals {
  type: typeof SET_WITHDRAWALS,
  payload: {
    items: IWithdrawalRequests[],
    total: number,
    reset: boolean
  }
}

export interface SetWithdrawalsFailure {
  type: typeof SET_WITHDRAWALS_FAILURE,
  payload: {
    error: string
  }
}


export interface SendWithdrawalRequest {
  type: typeof SEND_WITHDRAWAL_REQUEST,
  payload: {}
}

export interface SendWithdrawalSuccess {
  type: typeof SEND_WITHDRAWAL_SUCCESS,
  payload: {
    status: string
  }
}

export interface SendWithdrawalFailure {
  type: typeof SEND_WITHDRAWAL_FAILURE,
  payload: {
    error: string
  }
}
