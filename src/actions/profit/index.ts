import {Dispatch} from "redux";
import {filterI} from "../../containers/Dashboard/WithDrawal";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {ProfitDto} from "../../models/responses/profit.model";
import {FetchProfitChartService, FetchProfitHistoryService} from "../../apis/profit";
import {
  SET_PROFIT_CHART_HISTORY, SET_PROFIT_CHART_HISTORY_FAILURE, SET_PROFIT_CHART_REQUEST,
  SET_PROFIT_HISTORY,
  SET_PROFIT_HISTORY_FAILURE,
  SET_PROFIT_HISTORY_REQUEST
} from "../../constants/actionTypes";

export type ProfitTypes =
  SetProfitRequest
  | SetProfit
  | SetProfitFailure
  | SetProfitChart
  | SetProfitChartFailure
  | SetProfitChartRequest

export const FetchProfitHistory = (data: filterI) => async (dispatch: Dispatch<ProfitTypes>) => {
  try {
    dispatch({type: SET_PROFIT_CHART_REQUEST, payload: {}});
    //  fetch arbitrage
    // &search=${data.search}
    let slug = `take=${data.take}&skip=${data.skip}`;
    const res = await FetchProfitHistoryService(slug);
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));

      //set to reducer
      dispatch({
        type: SET_PROFIT_HISTORY,
        payload: {
          items: data.data,
          total: data.total,
          reset: true,
        }
      })
    } else {
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: SET_PROFIT_HISTORY_FAILURE,
        payload: {
          error: err
        }
      });
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_PROFIT_CHART_HISTORY_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

export const FetchProfitChart = () => async (dispatch: Dispatch<ProfitTypes>) => {
  try {
    dispatch({type: SET_PROFIT_HISTORY_REQUEST, payload: {}});

    const res = await FetchProfitChartService();
    if (res.status === 200 || res.status === 201) {
      const data = JSON.parse(Decrypt(res.data.data));
      //set to reducer
      dispatch({
        type: SET_PROFIT_CHART_HISTORY,
        payload: {
          items: data.items,
        }
      })
    } else {
      // handle error
      let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
      dispatch({
        type: SET_PROFIT_CHART_HISTORY_FAILURE,
        payload: {
          error: err
        }
      });
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_PROFIT_CHART_HISTORY_FAILURE,
      payload: {
        error: err
      }
    });
  }
};

//interfaces

export interface SetProfitRequest {
  type: typeof SET_PROFIT_HISTORY_REQUEST,
  payload: {}
}

export interface SetProfitChartRequest {
  type: typeof SET_PROFIT_CHART_REQUEST,
  payload: {}
}

export interface SetProfit {
  type: typeof SET_PROFIT_HISTORY,
  payload: {
    items: ProfitDto[],
    total: number,
    reset: boolean
  }
}

export interface SetProfitFailure {
  type: typeof SET_PROFIT_HISTORY_FAILURE,
  payload: {
    error: string
  }
}

export interface SetProfitChart {
  type: typeof SET_PROFIT_CHART_HISTORY,
  payload: {
    items: ChartI[],
  }
}

export interface ChartI {
  date: string,
  value: string
}


export interface SetProfitChartFailure {
  type: typeof SET_PROFIT_CHART_HISTORY_FAILURE,
  payload: {
    error: string
  }
}
