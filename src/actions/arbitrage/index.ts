import {Dispatch} from "redux";
import {coinMarketCapApi} from "../../apis";
import URLs from "../../constants/URLs";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {LiveArbitrageDefaultStateI} from "../../reducers/arbitrage";
import {
    FetchLiveArbitrageCandleGraphService,
    FetchLiveArbitrageGraphService,
    FetchLiveArbitrageService,
    FetchLiveSMBGraphService
} from "../../apis/arbitrage";

import {
    SET_ARBITRAGE_FAILURE,
    SET_ARBITRAGE_GRAPH,
    SET_ARBITRAGE_GRAPH_FAILURE,
    SET_ARBITRAGE_GRAPH_REQUEST,
    SET_ARBITRAGE_SMB_GRAPH,
    SET_ARBITRAGE_SMB_GRAPH_FAILURE,
    SET_ARBITRAGE_SMB_GRAPH_REQUEST,
    SET_ARBITRAGE_TABLE
} from "../../constants/actionTypes";
import MarketModel from "../../models/market.model";

export type ArbitrageTypes =
    SetArbitrageTable
    | SetArbitrageFailure
    | SetArbitrageGraph
    | SetArbitrageSMBGraph
    | SetArbitrageSMBGraphFailure
    | SetArbitrageGraphFailure
    | SetArbitrageGraphRequest
    | SetArbitrageSMBGraphRequest

export const FetchLiveArbitrage = () => async (dispatch: Dispatch<ArbitrageTypes>) => {
    try {
        //  fetch arbitrage
        const res = await FetchLiveArbitrageService();
        if (res.status === 200 || res.status === 201) {
            dispatch({
                type: SET_ARBITRAGE_TABLE,
                payload: {
                    currencies: JSON.parse(Decrypt(res.data.data)).currencies,
                    exchangers: JSON.parse(Decrypt(res.data.data)).exchangers,
                    lastUpdate: JSON.parse(Decrypt(res.data.data)).lastUpdatedAt
                }
            })
        } else {
            dispatch({
                type: SET_ARBITRAGE_FAILURE,
                payload: {
                    error: 'Server access error.'
                }
            });
        }

    } catch (e) {
        //handle error
        dispatch({
            type: SET_ARBITRAGE_FAILURE,
            payload: {
                error: 'Internet access error.'
            }
        });
    }
};

export const setLiveArbitrage = (data: LiveArbitrageDefaultStateI) => async (dispatch: Dispatch<ArbitrageTypes>) => {
    try {
        //  set arbitrage
        dispatch({
            type: SET_ARBITRAGE_TABLE,
            payload: {
                currencies: data.currencies,
                exchangers: data.exchangers,
                lastUpdate: data.lastUpdate
            }
        })
    } catch (e) {
        //handle error
        dispatch({
            type: SET_ARBITRAGE_FAILURE,
            payload: {
                error: 'Internet access error.'
            }
        });
    }
};

export const FetchLiveArbitrageGraph = (data: ArbitrageGraphFilter) => async (dispatch: Dispatch<ArbitrageTypes>) => {
    try {
        dispatch({type: SET_ARBITRAGE_GRAPH_REQUEST, payload: {loading: true}});

        let linear = await FetchLiveArbitrageGraphService(data.symbol);
        let candle = await FetchLiveArbitrageCandleGraphService(data.symbol);
        if (linear.status === 200) {

            dispatch({
                type: SET_ARBITRAGE_GRAPH,
                payload: {
                    linear: JSON.parse(Decrypt(linear.data.data)),
                    candle: JSON.parse(Decrypt(candle.data.data))
                }
            })
        } else {
            dispatch({
                type: SET_ARBITRAGE_GRAPH_FAILURE,
                payload: {
                    error: ""
                }
            });
        }

    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: SET_ARBITRAGE_GRAPH_FAILURE,
            payload: {
                error: err
            }
        });
    }
};

export const FetchLiveSMBGraph = () => async (dispatch: Dispatch<ArbitrageTypes>) => {
    try {
        const res = await FetchLiveSMBGraphService();
        if (res.status === 200 || res.status === 201) {
            dispatch({
                type: SET_ARBITRAGE_SMB_GRAPH,
                payload: {
                    data: JSON.parse(Decrypt(res.data.data)).items
                }
            })
        } else {
            let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
            dispatch({
                type: SET_ARBITRAGE_SMB_GRAPH_FAILURE,
                payload: {
                    error: err
                }
            });
        }
    } catch (e) {
        //handle error
        let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
        dispatch({
            type: SET_ARBITRAGE_SMB_GRAPH_FAILURE,
            payload: {
                error: err
            }
        });
    }
};

//interfaces

export interface IExchangers {
    id: number;
    name: string;
    title: string;
    logo: string;
    lastUpdate: Date;
    createdAt: Date;
    updatedAt: Date;
    isActive: boolean;
}

export interface ICurrencies {
    id: number;
    name: string;
    title: string;
    logo: string;
    arbitrage: string;
    lastUpdate: Date;
    createdAt: Date;
    updatedAt: Date;
    isActive: boolean;
    markets: market[];
}

export interface ArbitrageGraphFilter {
    symbol: string;
    time_period: string;
    convert: string;
    interval: string;
    count: string;
    skip_invalid: boolean;
    // time_start: string;
    // time_end: string;
    // slug: string,
}

export interface CoinMarketArbitrageGraphResponse {
    id: number,
    name: string,
    symbol: string,
    quotes: ArbitrageGraph[]
}

export interface ArbitrageGraph {
    time_open: string,
    time_close: string,
    time_high: string,
    time_low: string,
    quote: {
        EUR: {
            open: number,
            high: number,
            low: number,
            close: number,
            volume: number,
            market_cap: number,
            timestamp: string,
        }
    }
}

export interface market {
    id: number;
    price: string;
    change24Hour: string;
    change24PctHour: string;
    changeDay: string;
    changeHour: string;
    changePctDay: string;
    changePctHour: string;
    lastUpdatePrice: string;
    lastUpdateVolume: string;
    createdAt: Date;
    updatedAt: Date;
    pct: string,
    isActive: boolean;
    exchanger: IExchangers
}

export interface SetArbitrageTable {
    type: typeof SET_ARBITRAGE_TABLE,
    payload: {
        currencies: ICurrencies[],
        exchangers: IExchangers[],
        lastUpdate: string
    }
}

export interface SetArbitrageGraph {
    type: typeof SET_ARBITRAGE_GRAPH,
    payload: {
        linear: CoinMarketArbitrageGraphResponse,
        candle: any
    }
}

export interface SetArbitrageSMBGraph {
    type: typeof SET_ARBITRAGE_SMB_GRAPH,
    payload: {
        data: any[]
    }
}

export interface SetArbitrageSMBGraphFailure {
    type: typeof SET_ARBITRAGE_SMB_GRAPH_FAILURE,
    payload: {
        error: string
    }
}

export interface SetArbitrageGraphFailure {
    type: typeof SET_ARBITRAGE_GRAPH_FAILURE,
    payload: {
        error: string
    }
}

export interface SetArbitrageSMBGraphRequest {
    type: typeof SET_ARBITRAGE_SMB_GRAPH_REQUEST,
    payload: {
        loading: true
    }
}

export interface SetArbitrageGraphRequest {
    type: typeof SET_ARBITRAGE_GRAPH_REQUEST,
    payload: {
        loading: true
    }
}

export interface SetArbitrageFailure {
    type: typeof SET_ARBITRAGE_FAILURE,
    payload: {
        error: string
    }
}

export const marketResponse: market = {
    id: 0,
    price: "0",
    change24Hour: "0",
    change24PctHour: "0",
    changeDay: "0",
    changeHour: "0",
    changePctDay: "0",
    changePctHour: "0",
    lastUpdatePrice: "0",
    lastUpdateVolume: "0",
    createdAt: new Date(),
    updatedAt: new Date(),
    pct: "0",
    isActive: false,
    exchanger: {
        id: 0,
        name: '-',
        title: '-',
        logo: '',
        lastUpdate: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
        isActive: false
    }
};

export const ArbitrageGraphFilterDefaultState: ArbitrageGraphFilter = {
    symbol: "BTC",
    convert: "EUR",
    interval: "1d",
    skip_invalid: false,
    time_period: "daily",
    count: "90",
    // time_end: "",
    // time_start: "",
    // slug: "binance"
};
