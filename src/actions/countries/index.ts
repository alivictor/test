import {Dispatch} from "redux";
import CountryModel, {CountryCodeModel} from "../../models/country.model";
import {Decrypt} from "../../apis/encryption/encryption/encryption";
import {SET_COUNTRIES, SET_COUNTRIES_CODES} from "../../constants/actionTypes";
import {FetchCountriesCodesServices, FetchCountriesService} from "../../apis/countries";

export type CountriesTypes = SetCountries | SetCountriesCodes

export const FetchCountries = () => async (dispatch: Dispatch<CountriesTypes>) => {
  try {
    //
    const res = await FetchCountriesService();
    if (res.status === 200 || res.status === 201) {
      dispatch({
        type: SET_COUNTRIES,
        payload: {
          countries: [...JSON.parse(Decrypt(res.data.data))],
        }
      })
    } else {
      dispatch({
        type: SET_COUNTRIES,
        payload: {
          countries: [{id: 0, name: "Internet access error.", code: "unavailable"}],
        }
      })
    }

  } catch (e) {
    //handle error
    let err = localStorage.getItem('i18nextLng')?.includes('en') ? 'Internet access error.' : 'Fehler beim Internetzugang.'
    dispatch({
      type: SET_COUNTRIES,
      payload: {
        countries: [{id: 0, name: err, code: "unavailable"}],
      }
    })
  }
};

export const FetchCountriesCodes = () => async (dispatch: Dispatch<CountriesTypes>) => {
  try {
    //
    const res = await FetchCountriesCodesServices();
    if (res.status === 200 || res.status === 201) {
      dispatch({
        type: SET_COUNTRIES_CODES,
        payload: {
          codes: [...JSON.parse(Decrypt(res.data.data))],
        }
      })
    } else {
      dispatch({
        type: SET_COUNTRIES_CODES,
        payload: {
          codes: [{name: "unavailable", code: "unavailable", dial_code: "0"}],
        }
      })
    }


  } catch (e) {
    //handle error
    dispatch({
      type: SET_COUNTRIES_CODES,
      payload: {
        codes: [{name: "unavailable", code: "unavailable", dial_code: "0"}],
      }
    })
  }
};

//interfaces

export interface SetCountries {
  type: typeof SET_COUNTRIES,
  payload: {
    countries: CountryModel[]
  }
}

export interface SetCountriesCodes {
  type: typeof SET_COUNTRIES_CODES,
  payload: {
    codes: CountryCodeModel[]
  }
}
