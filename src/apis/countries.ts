import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const FetchCountriesService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/country/all`,
  });
};

export const FetchCountriesCodesServices = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/country/country-code/all`,
  });
};