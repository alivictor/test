import axios from 'axios';
import URLs from "../constants/URLs";

const lang = localStorage.getItem('i18nextLng')?.includes('en') ? 'en' : 'de'


export const indexApi = axios.create({
  baseURL: URLs.api_base_URL,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': `bearer ${localStorage.getItem('userAuth')}`,
    'SA_LANG': lang,
  }
});

export const coinMarketCapApi = axios.create({
  baseURL: URLs.api_base_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'X-CMC_PRO_API_KEY':URLs.coin_market_cap_api_key
  }
});

export const uploadApi = axios.create({
  baseURL: URLs.api_base_URL,
  headers: {
    'Content-Type': 'multipart/form-data',
    'Access-Control-Allow-Origin': '*',
    'Authorization': `bearer ${localStorage.getItem('userAuth')}`,
    'SA_LANG': lang,
  }
});

export const publicApi = axios.create({
  baseURL: "",
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Request-Headers': '*',
  }
});

export const articleApi = axios.create({
  baseURL: "",
  headers: {
    'Content-Type': 'application/json',
  }
});
