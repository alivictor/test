import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const FetchWithdrawalsService = (slug: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/withdrawal/current?${slug}`,
  });
};
