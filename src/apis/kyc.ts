import ApiCaller from '../assets/helpers/apiCaller';
import {Encrypt} from "./encryption/encryption/encryption";
import URLs from "../constants/URLs";

export const PostKYCRequestService = (postData: any) => {
    console.log(postData);
    return ApiCaller({
        method: 'post',
        url: `${URLs.api_base_URL}/auth/kyc-request`,
        data: {"data": Encrypt(JSON.stringify(postData))}
    });
};

export const UploadKYCRequestService = (postData: any) => {
    return ApiCaller({
        method: 'post',
        url: `${URLs.api_base_URL}/auth/kyc-request/upload`,
        data: postData
    });
};


export const FetchKYCStatusService = () => {
    return ApiCaller({
        method: 'get',
        url: `${URLs.api_base_URL}/auth/kyc-request/status`,
    });
};
