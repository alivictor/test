import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";


export const FetchLiveArbitrageService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/market/live`,
  });
};

export const FetchLiveSMBGraphService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/smb/chart`,
  });
};

export const FetchLiveArbitrageGraphService = (symbol: string) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/market/historical/${symbol}`,
  });
};
export const FetchLiveArbitrageCandleGraphService = (symbol: string) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/market/historical/candle/${symbol}`,
  });
};