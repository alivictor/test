import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const FetchFAQService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/app/faq`,
  });
};

