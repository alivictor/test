import URLs from "../constants/URLs";
import ApiCaller from "../assets/helpers/apiCaller";

export const FetchLevelsService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/level`,
  });
};
