import ApiCaller from '../assets/helpers/apiCaller';
import {Encrypt} from "./encryption/encryption/encryption";
import URLs from "../constants/URLs";

export const PostSettingService = (postData: any) => {
  return ApiCaller({
    method:'put',
    url: `${URLs.api_base_URL}/auth/update/setting`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const PostLoginService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/login`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const PostRegisterService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/register`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const PostVerifyService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/verify`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const PostForgetPasswordService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/forget`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const PostResetPasswordService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/reset`,
    data: {"data": Encrypt(JSON.stringify(postData))}
  });
};

export const GetUserInfoService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/auth/profile`,
  });
};

export const GetCurrentNotificationsService = (slug: any) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/notification/current?${slug}`,
  });
};

export const GetReferralHistoryService = (slug: any) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/auth/referral-history/current?${slug}`,
  });
};

export const GetReferralInfoService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/auth/referral-info/current`,
  });
};


export const UpdateProfileService = (postData: any) => {
  return ApiCaller({
    method:'post',
    url: `${URLs.api_base_URL}/auth/update`,
    data: {"data": Encrypt(JSON.stringify(postData))}

  });
};

export const GetUserFinancialProfileService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/auth/financial-profile/current`,
  });
};


export const GetFinancialDetailsService = () => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/auth/financial-details/current`,
  });
};


