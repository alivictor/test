import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";

export const FetchInvestsService = (slug: any) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/deposit/request/current?${slug}`,
  });
};

export const FetchTokenInvestsService = (slug: any) => {
  return ApiCaller({
    method:'get',
    url: `${URLs.api_base_URL}/deposit/token-deposit/current?${slug}`,
  });
};