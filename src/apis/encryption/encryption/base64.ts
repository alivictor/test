export const Base64Encode = (data: string) => {
    return Buffer.from(data).toString('base64');
};

export const Base64Decode = (hash: string) => {
    return Buffer.from(hash, 'base64').toString('ascii');
};