import {Base64Decode, Base64Encode} from "./base64";
import {AES128_Decrypt, AES128_Encrypt} from "./aes";
let randomstring  = require("randomstring");

// Secret Key
let secret = Base64Decode('NWRhNjVzZDE2YTVzZHNkYQ==');
// Random Hash 32 Character
let hash: string = Base64Encode(randomstring.generate(32)).substr(0, 32);

export const Encrypt = (data: string): string => {
    let iv = Base64Encode(randomstring.generate(32)).substr(0, 16);
    let encrypted = AES128_Encrypt(data, secret, iv);
    return Base64Encode(Base64Encode(iv) + hash + encrypted);
};


export const Decrypt = (hash : any): any => {
    let final = Base64Decode(hash);
    let iv2Base64 = final.substr(0, 24);
    let iv2 = Base64Decode(iv2Base64);
    // let hash2 = final.substr(24, 32);
    let hashBase64 = final.substr(56);
    return AES128_Decrypt(hashBase64, secret, iv2);
};
