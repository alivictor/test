import {createCipheriv, createDecipheriv} from "crypto";


export const AES128_Encrypt = (data:string, secret:string, iv:string):string => {

    const cipher = createCipheriv("AES-128-CBC", secret, iv);
    return (
      cipher.update(data, "utf8", "base64") +
      cipher.final("base64")
    );
};



export const AES128_Decrypt = (hash:string, secret:string, iv:string):string => {
    const decipher = createDecipheriv("AES-128-CBC", secret, iv);
    return (
      decipher.update(hash, "base64", "utf8") +
      decipher.final("utf8")
    );
};


