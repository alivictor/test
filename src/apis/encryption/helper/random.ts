
export const RandomBytes = (
    (typeof self !== 'undefined' && (self.crypto ))
        ? function() { // Browsers
            const crypto = (self.crypto), QUOTA = 65536;
            return function(n : any) {
                const a = new Uint8Array(n);
                for (let i = 0; i < n; i += QUOTA) {
                    crypto.getRandomValues(a.subarray(i, i + Math.min(n - i, QUOTA)));
                }
                return a;
            };
        }
        : function() { // Node
            return require("crypto").randomBytes;
        }
)();


export const RandomNumber = (max:number,min:number)=>{
    return Math.floor(Math.random() * max) + min;
}