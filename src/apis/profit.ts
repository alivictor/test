import ApiCaller from '../assets/helpers/apiCaller';
import URLs from "../constants/URLs";


export const FetchProfitHistoryService = (slug: any) => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/profit/history/current?${slug}`,
  });
};


export const FetchProfitChartService = () => {
  return ApiCaller({
    method: 'get',
    url: `${URLs.api_base_URL}/profit/chart-data/current`,
  });
};
