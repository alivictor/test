export interface IDefaultResult {

  target: {name: string, title: string},
  color: {text: string, background: string},
  maxMonthlyProfit: string | number,
  minMonthlyProfit: string | number,
  maxEstimatedProfit: string | number,
  minEstimatedProfit: string | number,
  minEstimatedProfitPercent: string | number,
  maxEstimatedProfitPercent: string | number
}
export const defaultResult: IDefaultResult = {
  target: {name: '-', title: '-'},
  color: {text: 'FE5578', background: 'FD7E6F'},
  maxMonthlyProfit: '-',
  minMonthlyProfit: '-',
  maxEstimatedProfit: '-',
  minEstimatedProfit: '-',
  minEstimatedProfitPercent: '-',
  maxEstimatedProfitPercent: '-'
};
