import React, {Suspense, useEffect, useState} from 'react';
import InputRange from "react-input-range";
import './calculator.scss';
import EuroSymbol from "../UiKits/EuroSymbol/EuroSymbol";
import 'react-input-range/lib/css/index.css';
import Loading from "../UiKits/Loading/Loading";
// import {GetUserInfoAction} from "../../actions/auth/AuthActions";
import {connect} from "react-redux";
import {FetchLevels} from "../../actions/levels/LevelsActions";
import {defaultResult, IDefaultResult} from "./interface";
import {currencyFormatter, formatter2, showLevelStatus} from "../../assets/helpers/utilities";
import {levelInterface, LevelItemInterface} from "../../models/levels.model";
import {Link} from "react-router-dom";
import URLs from "../../constants/URLs";
// import mail from "../../assets/img/svg/icons/Message.svg";
import {useTranslation} from "react-i18next";

interface IProps {
    levels: levelInterface[],
    fetchLevels: any,
}

const Calculator = (Props: IProps) => {
    const periodMaxValue = 60;
    const periodMinValue = 12;
    const maxValue = 250501;
    const minValue = 2500;
    const [value, setValue] = useState<number>(minValue);
    const [periodValue, setPeriodValue] = useState<number>(periodMinValue);
    const [calculateResult, setCalculateResult] = useState<IDefaultResult>(defaultResult);
    const [error, setError] = useState<null | string>('');

    const {
        t,
    } = useTranslation();

    useEffect(() => {
        let maximumInput: any = document.querySelector('.input-range__label--max span');

        maximumInput.innerHTML = 250;
        Props.levels.length === 0 && Props.fetchLevels();
        calculate(value, periodValue)
    }, [Props]);

    const handleSetValue = (inputValue: number) => {
        setValue(inputValue);

        calculate(inputValue, periodValue)
    };

    const handleSetPeriodValue = (periodValue: number) => {
        setPeriodValue(periodValue);
        calculate(value, periodValue)

    };

    const calculate = (inputValue: number, inputPeriodValue: number) => {

        //  send data to calculator plan
        let level = findOneLevelWithPoint(inputValue);
        //  send data to calculator levels of plan
        let target = level && filterLevel(level, inputValue);

        //validation
        if (!target || !target.item || inputValue < minValue) {
            //handle Error
            setError('Level Not Found')
        } else {
            setError('');
            let groupMinRange = parseFloat(level.groupMinRange);
            let groupMaxRange = parseFloat(level.groupMaxRange);
            let minEstimatedProfitPercent = groupMinRange * inputPeriodValue;
            let maxEstimatedProfitPercent = groupMaxRange * inputPeriodValue;
            let minEstimatedProfit = parseFloat(String((minEstimatedProfitPercent * inputValue) / 100))+parseFloat(String(inputValue));
            let maxEstimatedProfit = parseFloat(String((maxEstimatedProfitPercent * inputValue) / 100))+parseFloat(String(inputValue));

            setCalculateResult({
                target: {name: target.name, title: target.item.displayTitle},
                color: target.item.color,
                minMonthlyProfit: groupMinRange.toFixed(2).replace('.',',')+ "%",
                maxMonthlyProfit: groupMaxRange.toFixed(2).replace('.',',')+ "%",
                maxEstimatedProfit: currencyFormatter(maxEstimatedProfit),
                minEstimatedProfit: currencyFormatter(minEstimatedProfit),
                minEstimatedProfitPercent: minEstimatedProfitPercent.toFixed(2).replace('.',',')+ "%",
                maxEstimatedProfitPercent:maxEstimatedProfitPercent.toFixed(2).replace('.',',')+ "%"
            })
        }
    };

    const findOneLevelWithPoint = (value: number) => {

        return (
            Props.levels.filter(item =>
                parseInt(item.levels[0].minScore) <= value
                &&
                parseInt(item.levels[item.levels.length - 1].maxScore) >= value
            )[0]
        )
    }

    const filterLevel = (level: levelInterface, value: number) => {

        return (
            {
                name: level.title,
                item: level.levels.filter((item: LevelItemInterface) =>
                    parseInt(item.minScore) <= value &&  parseInt(item.maxScore) >= value)[0],
            }
        )

    }
    return (
        <Suspense fallback={<Loading/>}>
            <div className="calculate bg-white flex flex-wrap w-full py-12 px-6 my-12 h-24 rounded-xl">
                <div className="calculate-left pt-8 pb-4 relative">
                    <div className="px-4 calculate-left--section">
                        <div className="home-title m-5 text-normal ">{t('calculator.arbitrage_amount')}</div>

                        <div className="m-4">
                            <div className={`w-full px-4 `}>
                                <p className="text-2xl text-minsk font-bold inline-block mr-2">
                                    <EuroSymbol/>
                                </p>
                                <input type="number"
                                       className={`
                                         ${value <= 9999 && "w-20"}
                                         ${value > 9999 && value <= 99999999 && "w-32"}
                                         ${value > 99999999 && value <= 99999999999 && "w-32"}
                                         ${value > 99999999999 && "w-40"}
                                         `}
                                       maxLength={maxValue}
                                       min={500}
                                       max={maxValue}
                                       value={value}
                                       step={500}
                                       onChange={(e: any) => e.target.value <= maxValue && handleSetValue(e.target.value)}/>
                            </div>

                            <div className={
                                `w-full mt-4 range-container price 
                                  ${value <= 1000 && "small"}
                                  ${value > 1000 && value < 100000 && "normal"}
                                  ${value > 100000 && value < maxValue && "big"}
                                  ${value >= maxValue - 50000 && "huge"}
                                  `}>
                                <InputRange
                                    maxValue={maxValue}
                                    minValue={minValue}
                                    value={value}
                                    step={500}
                                    onChange={(value: any) => handleSetValue(value)}/>
                            </div>
                        </div>
                    </div>

                    <div className="px-4 calculate-left--section">
                        <div className="home-title mx-5 my-3 text-normal ">{t('calculator.arbitrage_period')}</div>

                        <div className="m-4">
                            <div className={`w-full px-4`}>
                                <input type="number"
                                       className={`
                                         ${periodValue <= 99999 && "w-16"}
                                         ${periodValue > 99999 && "w-24"}
                                         `}
                                       minLength={periodMinValue}
                                       min={periodMinValue}
                                       max={periodMaxValue}
                                       maxLength={periodMaxValue}
                                       value={periodValue}
                                       onChange={(e: any) => e.target.value <= periodMaxValue && handleSetPeriodValue(e.target.value)}/>
                                &nbsp; <span className="text-minskdarkBlue">{t('calculator.months')}</span>
                            </div>

                            <div className={
                                `w-full mt-4 range-container period 
                                  ${periodValue === 0 && "small"}
                                  ${periodValue > 800 && periodValue < 90000 && "normal"}
                                  ${periodValue > 90000 && periodValue < periodMaxValue && "big"}
                                  ${periodValue >= periodMaxValue - 150 && "huge"}
                                  `}>
                                <InputRange
                                    maxValue={periodMaxValue}
                                    minValue={periodMinValue}
                                    value={periodValue}
                                    onChange={(value: any) => handleSetPeriodValue(value)}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="calculate-right px-2 py-4">
                    <div className="flex flex-wrap justify-start">
                        {
                            error
                                ? <div className={`calculate-button w-48 error`}>{error}</div>
                                : <div className={`calculate-button w-48 ${calculateResult.target.name}`}
                                       style={{
                                           color: `#${calculateResult.color.text}`,
                                           backgroundColor: `#${calculateResult.color.background}`
                                       }}
                                >
                                    {
                                        !showLevelStatus(calculateResult.target.name, calculateResult.target.title)
                                            ? <Link to={URLs.dashboard_invest}>Invest Now</Link>
                                            : calculateResult.target.title
                                    }
                                </div>
                        }
                    </div>
                    {!error && <div className="calculate-right--result">
                        <div className="flex flex-wrap justify-between items-center mt-4">
                            <div className="home-title m-4 text-big font-bold ml-0">
                                {
                                    calculateResult.target.name === 'Concierge'
                                        ? t('calculator.support_title')
                                        : t('calculator.euro_account')
                                }
                            </div>
                            {/*
              {
                value < maxValue
                  && <div className="greenButton w-32 mr-8">Submit</div>
              }
              */}
                        </div>

                        <div className="table-parent flex flex-wrap justify-between items-center mt-4">
                            {
                                calculateResult.target.name === 'Concierge'
                                    ? <div className="w-full fade">
                                        <p className="w-full mt-5 text-minsk  text-lg pr-5">
                                            {t('calculator.support_message')}
                                        </p>
                                        <p className="w-full text-minsk mt-10 xl:mt-16 text-lg">
                                            <img src={"/assets/img/svg/icons/Message.svg"} alt="" className="inline-block mr-2"/>
                                            {t('calculator.contact')} support@pntcap.com
                                        </p>
                                    </div>
                                    : <table className="w-full fade">
                                        <thead>
                                        <tr>
                                            <th>{t('calculator.estimated_monthly_return')}</th>
                                            <th>{t('calculator.estimated_total_profits')}</th>
                                            <th>{t('calculator.estimated_roi')}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{calculateResult.minMonthlyProfit} - {calculateResult.maxMonthlyProfit}
                                            </td>
                                            <td>
                                                {calculateResult.minEstimatedProfit}&nbsp;-&nbsp;
                                                {calculateResult.maxEstimatedProfit}
                                            </td>
                                            <td>{calculateResult.minEstimatedProfitPercent}
                                                - {calculateResult.maxEstimatedProfitPercent}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                            }

                        </div>
                    </div>}
                </div>
            </div>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        levels: state.levels.items
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchLevels: () => dispatch(FetchLevels()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);
