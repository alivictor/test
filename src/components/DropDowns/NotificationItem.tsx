import React from 'react';
import moment from 'moment';

export interface INotification {
  title: string,
  message: string,
  createdAt: string,
  isSeen: boolean,
  readAt: null | string,
  id: number,
  isSend: boolean
}

interface IProps {
  item: INotification,
  onChangeDropDown: any
}

const NotificationItem = (Props: IProps) => {

  return (
    <div
      onClick={() => Props.onChangeDropDown(false)}
      className="notification-item flex flex-no-wrap justify-between px-1 py-2 text-sm leading-5"
    >
      <span className="ui-circle" style={{borderColor:"#FD7E6F"}}/>
      <span className="ui-column"/>

      <div className="text">
        <span className="title">{Props.item.title}</span>
        <p className="body">{Props.item.message}</p>
      </div>
      <div className="createdDate">
        <div className="date">
          {moment(Props.item.createdAt).fromNow(true)}
        </div>
      </div>
    </div>
  );
};

export default NotificationItem;
