import React, {useState} from 'react';
import DropDown from "../DropDown";

import {useTranslation} from "react-i18next";
import {flags} from "../../../layouts/dashboard/Sidebar/Sidebar";

const LanguageDropDown = () => {
  const [isOnLang, setIsOnLang] = useState<boolean>(false);

  const {
    i18n
  } = useTranslation();
  let language = localStorage.getItem('i18nextLng') || 'de-GE';

  return (
    <>
      <div className="languages-dropdown ml-3 relative inline-block">
        <div className="languages-dropdown--button rounded-full cursor-pointer h-8 w-8 flex flex-no-wrap"
             onClick={()=>setIsOnLang(prevState => !prevState)}>
          {language === "en-US" && <img src={"/assets/img/svg/icons/US-flag.svg"} alt="ENGLISH" className="w-full h-full"/>}
          {language === "de-GE" && <img src={"/assets/img/svg/icons/Germany-flag.svg"} alt="GERMANY" className="w-full h-full"/>}
          <img src={"/assets/img/svg/icons/Color.svg"} alt="" className="ml-2"/>
        </div>
        {
          isOnLang && <DropDown classes="languages-dropdown--menu mt-4" items={flags}
                                kind="languages"
                                onChangeLang={(data: string) => i18n.changeLanguage(data)}
                                onChangeDropDown={(data: boolean) => setIsOnLang(data)}/>
        }
      </div>
    </>
  );
};

export default LanguageDropDown;
