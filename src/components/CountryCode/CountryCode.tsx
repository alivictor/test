import React, {Suspense, useEffect} from 'react';
import Loading from "../UiKits/Loading/Loading";
import {CountryCodeModel} from "../../models/country.model";
import {FetchCountriesCodes} from "../../actions/countries";
import {connect} from "react-redux";

interface IProps {
  codes: CountryCodeModel[],
  fetchCodes: any,
  activeCode?: string
}
const CountryCode = (Props: IProps) => {
  useEffect(() => {
    //  get countries
    setTimeout(()=>{
      Props.codes.length === 0 && Props.fetchCodes();
      clearTimeout()
    }, 3000)
  }, [Props]);

  return (
    <Suspense fallback={<Loading/>}>
      <option key={1212121} value={"+49"}>
        Germany  (+49)
      </option>

      {Props.codes.map((item: CountryCodeModel, index: number) =>
          <option selected={Props.activeCode?.trim()===item.dial_code.slice(1,item.dial_code.length).trim()} key={index} value={item.dial_code}>
        {item.name}
        &nbsp;
        ({item.dial_code})
      </option>)}
    </Suspense>
  );
};

const mapStateToProps = (state: any) => {
  return {
    codes: state.country.codes
  }
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchCodes: () => dispatch(FetchCountriesCodes())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CountryCode);
