import React, {useRef, useLayoutEffect, Suspense} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Loading, {LoadingStyleNormal} from "../../UiKits/Loading/Loading";
import './customChart.scss';
import moment from "moment-timezone";

am4core.useTheme(am4themes_animated);

interface IProps {
  data: { date: string, amount: number }[],
  paddingRight?: { date: string, amount: number }[],
}

function ProfitBarChart(Props: IProps) {
  const chart: any = useRef(null);

  useLayoutEffect(() => {
    let x = am4core.create("profitbarchartdiv", am4charts.XYChart);
    //=============================configs
    x.dataSource.parser = new am4core.CSVParser();
    // @ts-ignore
    x.dataSource.parser.options.useColumnNames = true;
    // @ts-ignore
    x.dataSource.parser.options.reverse = true;

    x.paddingRight = 20;

    let dateAxis = x.xAxes.push(new am4charts.DateAxis());
    dateAxis.minZoomCount = 1;

    //===============valueAxis
    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
// height of axis
    valueAxis.height = am4core.percent(100);
    valueAxis.renderer.fontSize = "0.8em";

    //===============end valueAxis

    //===============series
    let series = x.series.push(new am4charts.ColumnSeries());
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "amount";
    series.yAxis = valueAxis;
    series.name = "MSFT: Volume";
    series.background.fill = am4core.color("#FFF");
    series.columns.template.column.fill =  am4core.color("#3670FB");
    series.columns.template.tooltipText = "{name}: {dateX}: {valueY}";
    series.tooltipText = "{valueY.value}";
    x.cursor = new am4charts.XYCursor();

    // volume should be summed
    // @ts-ignore
    series.tooltip.background.cornerRadius = 20;
    // @ts-ignore
    series.tooltip.background.fillOpacity = 0.5;
    series.defaultState.transitionDuration = 0;
//     //===============end series

    chart.current = x;
    //=============================end configs
    let array: any = [];
    Props.data.map(item => array.push({
      date: moment(new Date(item.date)).tz("Europe/Berlin").format('YYYY-MM-DD'),
      amount: item.amount > 1 ? item.amount.toFixed(2) : item.amount.toFixed(4),
    }));

    x.data = array;

    return () => {
      x.dispose();
    };
  }, []);

  // When the paddingRight prop changes it will update the chart
  useLayoutEffect(() => {
    let array: any = [];
    Props.data.map(item => array.push({
      date: moment(new Date(item.date)).tz("Europe/Berlin").format('YYYY-MM-DD'),
      amount: item.amount > 1 ? item.amount.toFixed(2) : item.amount.toFixed(4),
    }));
    chart.current.paddingRight = array;
    chart.current.data = array;
  }, [Props.paddingRight]);

  return (
    <Suspense fallback={<Loading/>}>
      <div className="w-full px:3 md:px-12 h-48 relative bg-white">
        {
          Props.data.length > 0
            ? <div id="profitbarchartdiv" style={{width: "100%", height: "100%"}}/>
            : <Loading stylesProp={LoadingStyleNormal}/>
        }
      </div>
    </Suspense>
  );
}

export default ProfitBarChart;