import React, {useRef, useLayoutEffect, Suspense} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Loading, {LoadingStyleNormal} from "../../UiKits/Loading/Loading";
import './customChart.scss';
import {useTranslation} from "react-i18next";
import moment from "moment-timezone";

am4core.useTheme(am4themes_animated);

interface IProps {
    data: { date: string, amount: number }[],
    paddingRight?: { date: string, amount: number }[],
    profitKind?: string;
    chartTitle?: string;
    bullet: boolean
}

function ProfitChart(Props: IProps) {
    const chart: any = useRef(null);

    useLayoutEffect(() => {
        let x = am4core.create("profitchartdiv", am4charts.XYChart);
        //=============================configs

        x.paddingRight = 20;

        let dateAxis = x.xAxes.push(new am4charts.DateAxis());
        dateAxis.minZoomCount = 1;
        dateAxis.renderer.fontSize = 0;

        let valueAxis: any = x.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.fontSize = "0.9em";

        //END Y VALUE AXIS

        let series = x.series.push(new am4charts.LineSeries());
        series.dataFields.dateX = "date";
        series.dataFields.valueY = "amount";
        series.tensionX = 0.8;
        series.strokeWidth = 3;
        series.stroke = am4core.color("#3670FB");
        // @ts-ignore
        series.tooltip.background.fillOpacity = 0.5;
        // @ts-ignore
        series.tooltip.background.fill = am4core.color("#3670FB");
        series.tooltipText = "{valueY.value}";
        x.cursor = new am4charts.XYCursor();

        let series2 = x.series.push(new am4charts.LineSeries());
        series2.dataFields.dateX = "date";
        series2.dataFields.valueY = "amount";
        series2.tensionX = 0.8;
        series2.strokeWidth = 0;
        series2.stroke = am4core.color("#3670FB");
        // @ts-ignore
        series2.tooltip.background.fillOpacity = 0;
        // @ts-ignore
        series2.tooltip.background.fill = am4core.color("#3670FB");
        series2.tooltipText = "{valueY.amount}";
        x.cursor = new am4charts.XYCursor();

        if(Props.bullet) {
            var bullet = series.bullets.push(new am4charts.CircleBullet());
            bullet.circle.radius = 6;
            bullet.circle.fill = am4core.color("#3670FB");
            bullet.circle.strokeWidth = 2;
            bullet.circle.stroke = am4core.color("#FFF");
        }


        // let scrollbarX = new am4charts.XYChartScrollbar();
        // scrollbarX.series.push(series2);
        // x.scrollbarX = scrollbarX;

        chart.current = x;
        //=============================end configs

        let array: any = [];
        Props.data.map(item => array.push({
            date: moment(new Date(item.date)).tz("Europe/Berlin").format('YYYY-MM-DD'),
            amount: item.amount > 1 ? item.amount.toFixed(2) : item.amount.toFixed(4),
        }));

        x.data = array
        return () => {
            x.dispose();
        };
    }, []);

    // When the paddingRight prop changes it will update the chart
    useLayoutEffect(() => {
        let array: any = [];
        Props.data.map(item => array.push({
            date: moment(new Date(item.date)).tz("Europe/Berlin").format('YYYY-MM-DD'),
            amount: item.amount > 1 ? item.amount.toFixed(2) : item.amount.toFixed(4),
        }));
        chart.current.paddingRight = array;
        chart.current.data = array;
    }, [Props.paddingRight]);
    const {
        t,
    } = useTranslation();

    return (
        <Suspense fallback={<Loading/>}>
            <div className="mt-10 md;mt-6 live-arbitrage--graph">
                <div className="pt-6 pb-4 px:3 md:px-12 live-arbitrage--table rounded-lg bg-white">
                    {
                        Props.profitKind
                            ?
                            <div className="header px-4 rounded-tl-xl">
                            <span className="text-darkBlue font-bold">
                              {Props.profitKind === "Daily" && t('home.daily_profit')}
                                {Props.profitKind === "Weekly" && t('home.weekly_profit')}
                                {Props.profitKind === "Monthly" && t('home.monthly_profit')}
                                {Props.profitKind === "Yearly" && t('home.yearly_profit')}
                                {t('home.chart')}
                            </span>
                            </div>
                            :
                            <div className="header px-4 rounded-tl-xl">
                            <span className="text-darkBlue font-bold">
                                {Props.chartTitle}
                            </span>
                            </div>
                    }

                    <div className="pt-6 pb-4 w-full  custom-chart relative">
                        {
                            Props.data.length > 0
                                ? <div id="profitchartdiv" style={{width: "100%", height: "100%"}}/>
                                : <Loading stylesProp={LoadingStyleNormal}/>
                        }
                    </div>
                </div>
            </div>

        </Suspense>
    );
}

export default ProfitChart;

// import React, {useRef, useLayoutEffect, Suspense} from 'react';
// import * as am4core from "@amcharts/amcharts4/core";
// import * as am4charts from "@amcharts/amcharts4/charts";
// import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import Loading, {LoadingStyleNormal} from "../../UiKits/Loading/Loading";
// import './customChart.scss';
// import {useTranslation} from "react-i18next";
// import prices from './linear.json';
//
// am4core.useTheme(am4themes_animated);
//
// interface IProps {
//   data: { date: string, amount: number }[],
//   paddingRight?: { date: string, amount: number }[],
//   profitKind?: string
// }
//
// function ProfitChart(Props: IProps) {
//   const chart: any = useRef(null);
//
//   useLayoutEffect(() => {
//     let x = am4core.create("profitchartdiv", am4charts.XYChart);
//     //=============================configs
//
//     x.paddingRight = 20;
//
//     let dateAxis = x.xAxes.push(new am4charts.DateAxis());
//     dateAxis.minZoomCount = 1;
//
//     let valueAxis: any = x.yAxes.push(new am4charts.ValueAxis());
//     valueAxis.renderer.fontSize = "0.9em";
//
//     //END Y VALUE AXIS
//
//     let series = x.series.push(new am4charts.LineSeries());
//     series.dataFields.dateX = "date";
//     series.dataFields.valueY = "value";
//     series.tensionX = 0.8;
//     series.strokeWidth = 3;
//     series.stroke = am4core.color("#09B66D");
//     // @ts-ignore
//     series.tooltip.background.fillOpacity = 0.5;
//     // @ts-ignore
//     series.tooltip.background.fill = am4core.color("#09B66D");
//     series.tooltipText = "{valueY.amount}";
//     x.cursor = new am4charts.XYCursor();
//
//     let series2 = x.series.push(new am4charts.LineSeries());
//     series2.dataFields.dateX = "date";
//     series2.dataFields.valueY = "value";
//     series2.tensionX = 0.8;
//     series2.strokeWidth = 0;
//     series2.stroke = am4core.color("#09B66D");
//     // @ts-ignore
//     series2.tooltip.background.fillOpacity = 0.5;
//     // @ts-ignore
//     series2.tooltip.background.fill = am4core.color("#09B66D");
//     series2.tooltipText = "{valueY.value}";
//     x.cursor = new am4charts.XYCursor();
//
//     let scrollbarX = new am4charts.XYChartScrollbar();
//     scrollbarX.series.push(series2);
//     x.scrollbarX = scrollbarX;
//
//     chart.current = x;
//     //=============================end configs
//     // x.data = Props.data;
//
//     return () => {
//       x.dispose();
//     };
//   }, []);
//
//   // When the paddingRight prop changes it will update the chart
//   useLayoutEffect(() => {
//     // chart.current.paddingRight = Props.paddingRight;
//     // chart.current.data = Props.data;
//     chart.current.paddingRight = prices;
//     chart.current.data = prices;
//   }, [Props.paddingRight]);
//   const {
//     t,
//   } = useTranslation();
//
//   return (
//     <Suspense fallback={<Loading/>}>
//       <div className="mb-4 mt-4 live-arbitrage--graph">
//         <div className="live-arbitrage--table mt-6 rounded-lg bg-white">
//           <div className="header pt-6  px-6 rounded-tl-xl">
//             <span className="text-darkBlue font-bold">
//               {Props.profitKind === "Daily" && t('home.daily_profit')}
//               {Props.profitKind === "Weekly" && t('home.weekly_profit')}
//               {Props.profitKind === "Monthly" && t('home.monthly_profit')}
//               {Props.profitKind === "Yearly" && t('home.yearly_profit')}
//               {t('home.chart')}
//             </span>
//           </div>
//
//           <div className="w-full pr-10 custom-chart relative">
//             {
//               Props.data.length > 0
//                 ? <div id="profitchartdiv" style={{width: "100%", height: "100%"}}/>
//                 : <Loading stylesProp={LoadingStyleNormal}/>
//             }
//           </div>
//         </div>
//       </div>
//
//     </Suspense>
//   );
// }
//
// export default ProfitChart;
