import React, {useRef, useLayoutEffect, Suspense} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Loading from "../../UiKits/Loading/Loading";
import './customChart.scss';

am4core.useTheme(am4themes_animated);

interface IProps {
  data: { date: string, value: number }[],
  paddingRight?: any,
}

function CustomChart(Props: IProps) {
  const chart: any = useRef(null);

  useLayoutEffect(() => {
    let x = am4core.create("smbchartdiv", am4charts.XYChart);

    x.paddingRight = 20;

    let dataArray: { date: string, value: number | string }[] = [];
    Props.data.map((item) => dataArray.push({
      date: item.date,
      value: item.value !== undefined ? item.value > 1 ? item.value.toFixed(2) : item.value.toFixed(4) : 0
    }));
    x.data = dataArray;
    let dateAxis = x.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis: any = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;
    valueAxis.renderer.fontSize = "0.9em";

    let series = x.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "value";
    // @ts-ignore
    series.tooltip.background.cornerRadius = 20;
    // @ts-ignore
    series.tooltip.background.fillOpacity = 0.5;
    series.stroke = am4core.color("#3670fb");
    // @ts-ignore
    series.tooltip.background.fill = am4core.color("#3670fb");
    series.tooltipText = "{valueY.value}";

    let bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.radius = 0;
    bullet.circle.fill = am4core.color("#3670fb");
    bullet.circle.strokeWidth = 1;
    bullet.circle.stroke = am4core.color("#3670fb");

    x.cursor = new am4charts.XYCursor();

    chart.current = x;

    return () => {
      x.dispose();
    };
  }, []);

  // When the paddingRight prop changes it will update the chart
  useLayoutEffect(() => {
    chart.current.paddingRight = Props.paddingRight;
    let dataArray: { date: string, value: number | string }[] = [];
    Props.data.map((item) => dataArray.push({
      date: item.date,
      value: item.value !== undefined ? item.value > 1 ? item.value.toFixed(2) : item.value.toFixed(4) : 0
    }));
    chart.current.data = dataArray;
  }, [Props.paddingRight]);

  return (
    <Suspense fallback={<Loading/>}>
      <div className="custom-chart relative">
        <div id="smbchartdiv" style={{width: "100%", height: "100%"}}/>
      </div>
    </Suspense>
  );
}

export default CustomChart
