import React from 'react';
import 'apexcharts';
import ReactApexChart from 'react-apexcharts'
import './donut-chart.scss';
import {currencyFormatter, formatter2} from "../../../assets/helpers/utilities";

class DonutChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      series: props.kind === "percent" ? props.data : props.amount,
      options: {
        chart: {
          width: 450,
          height: 450,
          type: 'donut',
        },
        dataLabels: {
          enabled: true,
          style: {
            fontSize: '10px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 'bold',
            colors: ['white']
          },
          formatter: function (val, timestamp) {
            if(props.kind === "percent") {
              return val > 1 ? val.toFixed(2) + "%" : val.toFixed(4) + "%"
            }else {
              return currencyFormatter(timestamp.w.config.series[timestamp.seriesIndex])
            }
          },

        },
        labels: props.labels,
        colors: props.colors,
        responsive: [
          {
            breakpoint: 3000,
            options: {
              chart: {
                height: 350,
                width: 400
              },
            }
          },
          {
            breakpoint: 1950,
            options: {
              chart: {
                height: 350,
                width: 350
              },
            }
          },
          {
            breakpoint: 1770,
            options: {
              chart: {
                width: 300
              },
            }
          },
          {
            breakpoint: 1700,
            options: {
              chart: {
                width: 230
              },
              dataLabels: {
                style: {
                  fontSize: '8px',
                  fontWeight: 'normal',
                },
              },
              legend: {
                height: 60,
                fontSize: '12px',
              }
            }
          },
          {
            breakpoint: 1480,
            options: {
              chart: {
                height: 260,
                width: 260
              },
              legend: {
                height: 40,
                fontSize: '10px',
              }
            }
          },
          {
            breakpoint: 1380,
            options: {
              chart: {
                height: 250,
                width: 250
              },
              dataLabels: {
                style: {
                  fontSize: '6px',
                  fontWeight: 'thin',
                },
              },
            }
          },
          {
            breakpoint: 1201,
            options: {
              chart: {
                height: 310,
                width: 210
              },
            }
          },
          {
            breakpoint: 1101,
            options: {
              chart: {
                width: 186
              },
            }
          },
          {
            breakpoint: 995,
            options: {
              chart: {
                width: 175
              },
            }
          },
          {
            breakpoint: 769,
            options: {
              chart: {
                width: 400
              },
              legend: {
                position: 'right',
                offsetY: 0,
                height: "100%",
                width: 200,
                fontSize: '14px',
              }
            },
          },
          {
            breakpoint: 500,
            options: {
              chart: {
                width: 350
              },
              legend: {
                width: 150,
                fontSize: '13px',
              },
              dataLabels: {
                style: {
                  fontSize: '8px',
                },
              },
            },
          },
          {
            breakpoint: 400,
            options: {
              chart: {
                width: 325
              },
              legend: {
                width: 110,
                fontSize: '12px',
              },
              dataLabels: {
                style: {
                  fontSize: '8px',
                },
              },
            },
          },
          {
            breakpoint: 350,
            options: {
              chart: {
                width: 270
              },
              legend: {
                width: 90,
                fontSize: '11px',
              },
              dataLabels: {
                style: {
                  fontSize: '7px',
                },
              },
            },
          },
        ],
        legend: {
          position: 'bottom',
          offsetY: 0,
          height: 100,
          width: "100%",
          fontSize: '14px',
        }
      },

    };

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps !== this.props) {
      this.setState({
        ...this.state,
        series: this.props && this.props.kind === "percent" ? this.props.data : this.props.amount,
        labels: this.props.labels,
        colors: this.props.colors,
        options: {
          dataLabels: {
            formatter: (val, timestamp) => {
              if(this.props.kind === "percent") {
                return val > 1 ? val.toFixed(2) + "%" : val.toFixed(4) + "%"
              }else {
                return currencyFormatter(timestamp.w.config.series[timestamp.seriesIndex])
              }
            },
          },
        }
      })
    }
  }

  render() {
    return (
      <div>
        <div className="chart-wrap mx-auto w-full flex items-center justify-center">
          <div id="chart">
            <ReactApexChart options={this.state.options} series={this.state.series} type="donut"/>
          </div>
        </div>
      </div>
    )
  }
}

export default DonutChart