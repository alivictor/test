import React, {useState} from 'react';
import 'apexcharts'
import Chart from 'react-apexcharts';
import './bar-chart.scss';
import {currencyFormatter, formatter2} from "../../../assets/helpers/utilities";

interface IProps {
  data: any[],
  labels: string[],
  colors: string[],
}

const BarChart = (Props: IProps) => {

  const [series, setSeries] = useState([{
    name: 'Amount',
    data: Props.data
  }]);
  const [options, setOptions] = useState({
    chart: {
      type: 'bar',
      height: 350,
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight:
          Props.data.length < 3
            ? Props.data.length < 2 ? "10px" : '15px'
            : Props.data.length > 5 ? '35px' : '25px',
        borderRadius: navigator.appVersion.includes('Chroome') ? 35 : 0,
        radiusOnLastStackedBar: true,
        distributed: true,
        dataLabels: {
          enabled: false
        },
        states: {
          normal: {
            filter: {
              type: 'none',
              value: 0,
            }
          },
          states: {
            hover: {
              filter: 'none'
            }
          },
          active: {
            allowMultipleDataPointsSelection: false,
            filter: {
              type: 'darken',
              value: 1,
            }
          },
        },

      }
    },

    dataLabels: {
      enabled: true,
      textAnchor: 'start',
      offsetX: -10,
      dropShadow: {
        enabled: true
      },
      style: {
        fontSize: Props.data.length < 3 ? '12px' : "10px",
        colors: ['#FFF']
      },
      formatter: function (val: any) {
        return currencyFormatter(val)
      },
    },

    colors: Props.colors || ["#F17767", "#6418C3", "#00ADA3", "#38327C"],

    xaxis: {
      categories: Props.labels,
    },

    tooltip: {
      shared: true,
      y: {
        formatter: function (val: any) {
          return currencyFormatter(val)
        }
      }
    },

    responsive: [
      {
        breakpoint: 3000,
        options: {
          chart: {
            height: 400
          },
        }
      },
      {
        breakpoint: 1400,
        options: {
          chart: {
            height: 300
          },
        }
      },
      {
        breakpoint: 600,
        options: {}
      },
    ],


    // chart: {
    //   type: 'bar',
    //   height: 350
    // },
    // plotOptions: {
    //   bar: {
    //     horizontal: true,
    //   }
    // },
    // dataLabels: {
    //   enabled: false
    // },
    // xaxis: {
    //   categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
    //     'United States', 'China', 'Germany'
    //   ],
    // }

  });

  return (
    <div className="bar-chart">

      <Chart options={options} series={series} type="bar"/>
    </div>
  );
};

export default BarChart;
