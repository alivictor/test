import styled from 'styled-components'

export const Btn = styled.button`
  background: plum;
  padding: 0;
  border: none;
  border-radius: 7px;
  transition: all 150ms linear;
  a,div {
    width: 100%;
    height: 100%;
    display: flex;
    padding: 5px 10px 5px 10px;
    text-decoration: none !important;
    align-items: center;
    justify-content: center;
    flex-direction: ${props => props.reverse ? 'row-reverse' : 'row'}
  }
  svg {
    margin: ${props => props.hasName === 'true' ? props.reverse ? '0 0 0 8px' : '0 8px 0 0' : '0'};
  }
  span {
    flex: 1;
    display: flex;
    align-items: center;
  }
  &[data-model="accent"] {
    background: #3670FB !important;
    color: white !important;
  }
  &[data-model="accent"] a,
  &[data-model="accent"] div,
  &[data-model="accent"] path {
    color: white !important;
    fill: white !important;
  }
  &[data-model="accent"]:hover {
    background: #3061d9 !important;
  }
`;