import React from 'react';
import '../../../containers/Dashboard/Home/home.scss'
export interface IFLexItem {
  src: string,
  text: string,
  value: any,
    break?: boolean
}

const FlexItem = (Props: IFLexItem) => {

  return (
    <div className="summary-item flex flex-wrap flex-1 justify-center items-center">
      <div>
        <img src={Props.src} alt=""/>
      </div>
      <div>
        <p>
            {Props.text}
            {Props.break && <br/>}
            <span className="relative">{Props.value}</span>
        </p>
      </div>
    </div>
  );
};

export default FlexItem;
