import React from 'react';
import './modal.scss';
import Loading from "../Loading/Loading";
import {useTranslation} from "react-i18next";

interface IProps {
    callBackConfirm: any,
    callBackCancel: any,
    callBackClose: any,
    title: string,
    content: string,
    icon: string,
    loading: boolean,
    kind: string
}

const ConfirmModal = (Props: IProps) => {
    const {
        t,
    } = useTranslation();

    return (
        <div className="fixed z-10 inset-0 overflow-y-auto modal">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

                <div className="fixed inset-0 transition-opacity" aria-hidden="true">
                    <div className="absolute inset-0 bg-gray-500 opacity-75"
                         onClick={() => Props.loading ? console.log('') : Props.callBackCancel()}/>
                </div>

                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

                <div
                    className="modal-content inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                    role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                    <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div
                                className={`mx-auto flex-shrink-0 flex items-center
                     justify-center h-12 w-12 rounded-full bg-${Props.kind}-100 sm:mx-0 sm:h-10 sm:w-10`}>
                                <svg className={`h-6 w-6 text-${Props.kind}-600`} xmlns="http://www.w3.org/2000/svg"
                                     fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                          d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"/>
                                </svg>
                            </div>
                            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <h3 className="text-lg leading-6 font-medium text-gray-900" id="modal-headline">
                                    {Props.title}
                                </h3>
                                <div className="mt-2">
                                    <p className="text-sm text-gray-500">
                                        {Props.content}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button type="button"
                                onClick={() => Props.callBackConfirm()}
                                disabled={Props.loading}
                                className={`w-full inline-flex items-center justify-center relative
                       rounded-md border border-transparent shadow-sm
                       ${Props.loading && 'opacity-50'}
                        px-4 py-2 bg-${Props.kind}-600 focus:ring-${Props.kind}-500 hover:bg-${Props.kind}-700
                         text-white focus:outline-none text-base font-medium
                          focus:ring-2 focus:ring-offset-2 sm:ml-3 sm:w-auto sm:text-sm`}>
                            {t('confirmSection.confirm')}
                            {Props.loading && <Loading stylesProp2={{
                                width: '35px',
                                height: 'auto',
                                position: 'absolute',
                                top: "8%",
                                left: '30%'
                            }}/>}
                        </button>
                        <button type="button"
                                disabled={Props.loading}
                                onClick={() => Props.callBackCancel()}
                                className=" mt-3 w-full inline-flex items-center justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                            {t('confirmSection.cancel')}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ConfirmModal;
