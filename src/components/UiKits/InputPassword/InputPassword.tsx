import React, {useState} from 'react';
import {Field} from "formik";
import '../../../layouts/auth/authWraper.scss';

interface HandleChangeProps {
  name: string,
  placeholder: string,
  label: string,
}

const InputPassword = (Props: HandleChangeProps) : JSX.Element => {

  const [hidePw, setHidePw] = useState<boolean>(true);
  return (
    <>
      <label className="block login-container--label
               text-left text-gray-700 text-sm font-bold mb-4" htmlFor="password">
        {Props.label}
      </label>
      <div className="form--control" id="passwordInput">

        <Field
          name={Props.name}
          id={Props.name}
          minLength={8}
          required={true}
          className="appearance-none border login-container--input
                border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
          type={hidePw ? "password" : "text"}
          placeholder={Props.placeholder}/>
        <span onClick={() => setHidePw((prevHidePw: boolean) => !prevHidePw)}
              className="flex justify-center align-middle cursor-pointer">
              {
                hidePw
                  ? <img src={"/assets/img/svg/icons/hide-input-icon.svg"} alt=""/>
                  : <img src={"/assets/img/svg/icons/Eye.svg"} alt=""/>
              }
            </span>
      </div>
    </>
  );
};

export default InputPassword;
