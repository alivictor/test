import React from 'react';

const EuroSymbol = () => {

  return (
    <span>‎€</span>
  );
};

export default EuroSymbol;
