import React from 'react';

const StatusMessage = (Props: any) => {
  const status: string = Props.status;

  return (
    <div>
      {
        (status === "Updated" || (status && status.includes('Error')))
        &&
        <div
          className={`withdrawal-button transition-all 
                       ${status === "Updated" ? "bg-green-haze" : ""} 
                       ${status && status.includes('Error') ? "btn-danger text-white " : ""} 
                       w-auto min-w-48 font-bold py-3 px-4 rounded-xl`}>
          {status === "Updated" ?
            localStorage.getItem('i18nextLng')?.includes('en')
              ? Props.message || 'Updated'
              : Props.message || 'Aktualisiert'
            : status}
        </div>
      }
    </div>
  )
};

export default StatusMessage;
