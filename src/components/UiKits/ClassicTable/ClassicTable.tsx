import React, {Suspense} from 'react';
import './table.scss';
import Loading from "../Loading/Loading";

export interface thead {
    title: string
}

interface IProps {
    classes?: string,
    loading: boolean,
    error: string,
    headers: thead[],
    total: number,
    children: any,
}


const ClassicTable = (Props: IProps) => {

    return (
        <Suspense fallback={<Loading/>}>
            <div className="bg-white pt-4 pb-10 rounded-xl classic-table overflow-x-auto">
                <table className={`w-full ${Props.classes}`}>
                    <thead className="h-32 py-6">
                    <tr className="h-20 font-normal">
                        {
                            Props.headers.map((item, index) => <th key={index}>{item.title}</th>)
                        }
                    </tr>
                    </thead>

                    {
                        Props.error !== ""
                            ? <tbody className="w-full mt-5">
                            <tr>
                                <td/>
                                {Props.headers.length > 4 && <td/>}
                                {Props.headers.length > 5 && <td className="mobile-hidden"/>}
                                <td className="h-32 text-center text-danger">{Props.error}</td>
                            </tr>
                            </tbody>
                            : Props.total === 0 || Props.loading
                            ? <tbody className="w-full mt-5">
                            <tr>
                                <td/>
                                {Props.headers.length > 4 && <td/>}
                                {Props.headers.length > 5 && <td className="mobile-hidden"/>}
                                {Props.headers.length > 6 && <td className="mobile-hidden"/>}
                                <td className="h-32">
                                    {
                                        Props.loading ? <Loading/> :
                                            <img src={"/assets/img/svg/EmptyTable.svg"} className="m-0 m-auto " alt=""/>
                                    }
                                </td>
                            </tr>
                            </tbody>
                            :
                            <tbody className="text-center">
                            {Props.children}
                            </tbody>
                    }


                </table>

            </div>
        </Suspense>
    );
};

export default ClassicTable;
