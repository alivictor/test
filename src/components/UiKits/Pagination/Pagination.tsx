import React from 'react';


export const NextButton = <svg
    className="h-5 w-5 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                               fill="currentColor">
    <path fillRule="evenodd"
          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
          clipRule="evenodd"/>
</svg>;

export const PrevButton =  <svg
    className="h-5 w-5 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor">
    <path fillRule="evenodd"
          d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
          clipRule="evenodd"/>
</svg>;


const Pagination = () => {

    return (
        <div className="pt-10">
            <div className="pagination  sm:flex-1 sm:flex sm:items-center
           sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                <div>
                    <nav
                        className="relative z-0 inline-flex shadow-sm p-2 border border-gray-200 rounded-lg border-gray-300">
                        <div
                            className="cursor-pointer relative inline-flex items-center px-2 py-2 rounded-lg text-darkBlue text-sm leading-5 font-medium focus:z-10 focus:outline-none focus:shadow-outline-blue active:text-gray-500 transition ease-in-out duration-150"
                            aria-label="Previous">
                            <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fillRule="evenodd"
                                      d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                      clipRule="evenodd"/>
                            </svg>
                        </div>
                        <div
                            className="cursor-pointer -ml-px relative inline-flex items-center px-4 py-2 rounded-lg bg-darkblue text-sm leading-5 font-medium text-white focus:z-10 focus:outline-none transition ease-in-out duration-150">
                            1
                        </div>
                        <div
                            className="cursor-pointer -ml-px relative inline-flex items-center px-4 py-2 rounded-lg text-sm leading-5 font-medium text-black focus:z-10 focus:outline-none transition ease-in-out duration-150">
                            2
                        </div>

                        <span
                            className="-ml-px text-darkBlue relative inline-flex items-center px-4 py-2 bg-white text-sm leading-5 font-medium text-darkBlue">
                          ...
                        </span>
                        <div
                            className="cursor-pointer -ml-px relative inline-flex items-center px-4 py-2 rounded-lg text-sm leading-5 font-medium text-black focus:z-10 focus:outline-none transition ease-in-out duration-150">
                            5
                        </div>

                        <div
                            className="cursor-pointer -ml-px relative inline-flex items-center px-2 py-2 rounded-r-md text-darkBlue text-sm leading-5 font-medium focus:z-10 focus:outline-none focus:shadow-outline-blue active:text-gray-500 transition ease-in-out duration-150"
                            aria-label="Next">

                            <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fillRule="evenodd"
                                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                      clipRule="evenodd"/>
                            </svg>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    );
};

export default Pagination;