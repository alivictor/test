import React, {Suspense, useEffect} from 'react';
import {connect} from 'react-redux';

import './swiper.scss'
import Loading, {LoadingStyleNormal} from "../UiKits/Loading/Loading";
import {FetchArticles, IArticles} from "../../actions/articles";

interface IProps {
    articles: IArticles[],
    fetchArticles: any,
    error: string
}

const Articles = (Props: IProps) => {
    useEffect(() => {
        Props.articles.length === 0 && Props.fetchArticles()
    }, [Props]);

    return (
        <Suspense fallback={<Loading/>}>

            {Props.articles && Props.articles.length > 0 ?
                <div className='w-ful flex flex-col news col-span-2'>
                    <div className='w-full flex flex-col flex-1'>
                        <div className='flex-1 flex px-3 py-5'>
                            <p className='color-black flex-1 flex items-center'>News</p>
                            <div className='circle flex items-center'>
                      <span className='block w-full h-full'>
                             <img src="/assets/img/svg/icons/oblique-arrow.svg" alt=""/>
                      </span>
                            </div>
                        </div>
                        <div className='flex-1 news-images'>
                            {
                                Props.articles.map(item =>
                                    <a href={item.cmb2.sa_metabox.news_link} className="img" target={"_blank"}
                                       rel="noopener noreferrer">
                                        <img src={item.cmb2.sa_metabox.news_agency_logo} className="w-full cursor-pointer" alt=""
                                             style={{objectFit: "contain"}}/>
                                    </a>
                                )
                            }

                        </div>


                    </div>
                </div>
                : Props.error !== '' ?
                    <span className="text-danger text-center text-2xl w-full m-0 m-auto block">{Props.error}</span> :
                    <Loading stylesProp={LoadingStyleNormal}/>
            }
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        articles: state.articles.items,
        error: state.articles.error,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchArticles: () => dispatch(FetchArticles())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Articles);
