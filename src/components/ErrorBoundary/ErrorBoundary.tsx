import React from 'react';
import ErrorMessage from "../UiKits/ErrorMessage/ErrorMessage";
import URLs from "../../constants/URLs";

const ErrorBoundary = () => {

  return (
    <div className="w-100 h-auto">
      <ErrorMessage src="/assets/img/png/500-min.png" title="Internal Server Error"
                    body="Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     Est in orci sit cras commodo. Felis nunc ac, massa proin ha."
                    buttonTitle="Go Back" buttonLink={URLs.login}/>
    </div>
  );
};


export default ErrorBoundary;
