import React, {Suspense} from 'react';
import './levelCart.scss';
import Loading from "../UiKits/Loading/Loading";
import {connect} from 'react-redux';
import {levelInterface} from "../../models/levels.model";
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {A11y, Pagination, Scrollbar, Navigation} from "swiper";
// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import 'react-input-range/lib/css/index.css';
import {useTranslation} from "react-i18next";
import {readableNumber} from "../../assets/helpers/utilities";

interface IProps {
    level: string,
    levels: levelInterface[],
}

const LevelCart = (Props: IProps) => {
    SwiperCore.use([Pagination, Scrollbar, A11y, Navigation]);

    const {
        t,
    } = useTranslation();
    return (
        <Suspense fallback={<Loading/>}>

            <Swiper
                className="col-span-8 xl:col-span-7 lg-col-span-7 grid grid-cols-1 sm:grid-cols-3 w-100"
                spaceBetween={20}
                slidesPerView={3}
                // navigation
                // pagination
                breakpoints={{
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 10,
                    },
                    767: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    850: {
                        slidesPerView: 3,
                        spaceBetween: 5
                    },
                    1600: {
                        slidesPerView: 3,
                        spaceBetween: 19
                    }
                }}
            >
                {
                    Props.levels.map((item, index) => {

                        return (
                            index !== Props.levels.length - 1 &&
                            <SwiperSlide key={index}
                                         className="col-span-1 level-cart">
                              <ul className="w-full">
                                <li >
                                  <h2 className="font-bold text-center mx-auto text-2xl">{item.title}</h2>
                                  <div className="w-full">
                                    <p className=" text-center mx-auto text-lg">{t('accounts.investment_amount')}</p>
                                    <p className=" text-center mx-auto text-xl">€ {item.amount}</p>
                                  </div>
                                </li>

                                <li className="w-full px-1 flex flex-wrap justify-between hidden">
                                  <p className="mobile-title">{t('accounts.investment_amount')} (€)</p>
                                    {
                                        item.title !== item.levels[0].title
                                            ? item.levels.map((item2, index) =>
                                            item.title !== item2.title &&
                                          <div key={index}>
                                            <p className="title">Level {index + 1}</p>
                                            <p>€ {item2.minScore}+</p>
                                          </div>
                                            )
                                            : <div>€ {item.amount}</div>
                                    }
                                </li>

                                <li>
                                  <p className="mobile-title">{t('invest.market')}</p>
                                  <p>{item.levels[item.levels.length - 1].market}</p>
                                </li>

                                <li className="hidden">
                                  <p className="mobile-title">{t('accounts.estimated_monthly_returns_in_percent')}</p>
                                  <div className="w-full flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              <div key={index} className="mx-auto text-center ">
                                                  <p>
                                                      <small>{item2.minMonthlyProfit.replace('.', ',')}%
                                                          - {item2.maxMonthlyProfit.replace('.', ',')}%</small>
                                                  </p>
                                              </div>
                                          )
                                      }

                                  </div>
                                </li>

                                <li>
                                  <p className="mobile-title">{t('accounts.estimated_annual_returns_in_percent')}</p>
                                  <div className="w-full  flex flex-wrap justify-between text-center">
                                    <div className="mx-auto text-center ">
                                      <p className="font-bold">
                                          {readableNumber(item.levels[0].profitAverage)} % - {readableNumber(item.levels[item.levels.length-1].profitAverage)} %
                                      </p>
                                    </div>
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p
                                    className="mobile-title">{t('accounts.euro_bank_transfer_BTC_ETH_SMT_withdrawals')}</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span className={item2.eurBankTransfer ? 'true' : 'false'}

                                            >
                                            <img src={`${item2.eurBankTransfer
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">{t('accounts.support')}</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span className={item2.withdrawals24Support ? 'true' : 'false'}

                                            >
                                            <img src={`${item2.withdrawals24Support
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Access to our institional accounts</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.accessToOurInstitutionalAccounts ? 'true' : 'false'}

                                            >
                                            <img src={`${item2.accessToOurInstitutionalAccounts
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Access to the otc precious metals market</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.accessToTheOtcPreciousMetalsMarket ? 'true' : 'false'}

                                            >
                                            <img src={`${item2.accessToTheOtcPreciousMetalsMarket
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Access to the otc oil & crypto market</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.accessToTheOtcOilAndCryptoMarket ? 'true' : 'false'}
                                            >
                                            <img src={`${item2.accessToTheOtcOilAndCryptoMarket
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                  {/*<li className=" boolean">*/}
                                  {/*    <p className="mobile-title">Access to the real estate markets</p>*/}
                                  {/*    <div className="w-full px-3 flex flex-wrap justify-between text-center">*/}
                                  {/*        {*/}
                                  {/*            item.levels.map((item2, index) =>*/}
                                  {/*                <span*/}
                                  {/*                    className={item2.accessToTheRealEstateMarkets ? 'true' : 'false'}*/}
                                  {/*                >
                                            <img src={`${item2.accessToTheRealEstateMarkets
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>*/}
                                  {/*            )*/}
                                  {/*        }*/}
                                  {/*    </div>*/}
                                  {/*</li>*/}

                                <li className=" boolean">
                                  <p className="mobile-title">Access to the Otc Forex market</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.accessToTheOtcForexMarket ? 'true' : 'false'}
                                            >
                                            <img src={`${item2.accessToTheOtcForexMarket
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Additional otc markets with higher margins</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.additionalOtcMarketsWithHigherMargins ? 'true' : 'false'}
                                            >
                                            <img src={`${item2.additionalOtcMarketsWithHigherMargins
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Additional SMT token bonus per trade</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.additionalSMTTokenBonusPerTrade ? 'true' : 'false'}
                                            >
                                            <img src={`${item2.additionalSMTTokenBonusPerTrade
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                                <li className=" boolean">
                                  <p className="mobile-title">Concierge service for manual higher margin
                                    trades</p>
                                  <div className="w-full px-3 flex flex-wrap justify-between text-center">
                                      {
                                          item.levels.map((item2, index) =>
                                              item.levels.length - 1 === index &&
                                            <span
                                              className={item2.conciergeServiceForManualHigherMarginTrades ? 'true' : 'false'}
                                            >
                                            <img src={`${item2.conciergeServiceForManualHigherMarginTrades
                                                ? '/assets/img/svg/icons/check.svg' : '/assets/img/svg/icons/close.svg'}`} alt=""/>
                                            </span>
                                          )
                                      }
                                  </div>
                                </li>

                              </ul>
                            </SwiperSlide>
                        )
                    })
                }
            </Swiper>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        level: state.auth.level ? state.auth.level.group.title + " " + state.auth.level.title : "",

    }
};


export default connect(mapStateToProps)(LevelCart);
