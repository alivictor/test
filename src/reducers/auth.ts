import {
  AUTH_FAIL,
  AUTH_LOADING,
  AUTH_SUCCESS,
  AuthDispatchTypes,
  FINANCIAL_PROFILE,
  FinancialProfileType,
  GET_FINANCIAL_DETAILS_FAILURE,
  GET_FINANCIAL_DETAILS_REQUEST,
  GET_FINANCIAL_DETAILS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE,
  GET_NOTIFICATIONS_REQUEST,
  GET_NOTIFICATIONS_SUCCESS,
  GET_REFERRAL_HISTORY,
  GET_REFERRAL_HISTORY_FAILURE,
  GET_REFERRAL_HISTORY_REQUEST,
  GET_REFERRAL_INFO,
  GET_REFERRAL_INFO_FAILURE,
  GET_REFERRAL_INFO_REQUEST,
  POST_EMAIL_SETTINGS_FAILURE,
  POST_EMAIL_SETTINGS_REQUEST,
  POST_EMAIL_SETTINGS_SUCCESS
} from "../actions/auth/AuthActionTypes";
import UserModel from "../models/user.model";
import {LevelItemInterface} from "../models/levels.model";
import {INotification} from "../components/DropDowns/NotificationItem";

interface DefaultStateI {
  loading: boolean,
  token: string | undefined,
  user?: UserModel,
  level?: LevelItemInterface,
  hasInvest: boolean,
  canInvest: boolean,
  isPremium: boolean,
  error: string,
  financial?: FinancialProfileType,
  referral: ReferralReducer,
  notifications: {
    loading: boolean,
    error: string,
    data: INotification[],
    new: number,
    total: number,
  },
  setting: {
    email: {
      data: any,
      loading: boolean,
      error: string,
      status: string
    }
  },
  financialDetails: {
    loading: boolean,
    error: string,
    data: financialDetails
  }
}

export interface financialDetails {
  status: string,
  details: {
    startDate: string,
    currentDate: string,
    totalDeposits: string,
    totalProfits: string,
    total: string,
    roiPercent: string,
    days: {
      count: number,
      amount: number,
      roi: number,
      chart: {
        date: string,
        value: string,
        roi: string,
      }[]
    },
    week: {
      count: number,
      amount: number,
      roi: number,
      chart: {
        date: string,
        value: string,
        roi: string,

      }[]
    },
    months: {
      count: number,
      amount: number,
      roi: number,
      chart: {
        date: string,
        value: string,
        roi: string,

      }[]
    }
  }
}

export interface ReferralReducer {
  history?: ReferralHistory,
  userCount: number,
  totalPoint: number,
  links: referralLinks[],
  loading: boolean,
  error: string,
}

export interface ReferralHistory {
  items: any[],
  total: number,
  loading: boolean,
  error: string
}

export interface referralLinks {
  ln: string,
  link: string
}

const defaultState: DefaultStateI = {
  loading: false,
  token: undefined,
  user: undefined,
  level: undefined,
  hasInvest: false,
  canInvest: true,
  isPremium: true,
  //TODO:: if error is network error, show in program with toast message
  //TODO:: if error is 401, show toast with countdown for redirecting to login page
  error: "",
  financial: undefined,
  referral: {
    history: {
      items: [],
      total: 0,
      loading: false,
      error: '',
    },
    links: [],
    userCount: 0,
    totalPoint: 0,
    loading: false,
    error: '',
  },
  notifications: {
    loading: false,
    error: '',
    data: [],
    new: 0,
    total: 0,
  },
  setting: {
    email: {
      error: "",
      data: null,
      loading: false,
      status: ""
    }
  },
  financialDetails: {
    loading: false,
    error: "",
    data: {
      status: "-",
      details: {
        startDate: "-",
        currentDate: "-",
        totalDeposits: "-",
        totalProfits: "-",
        total: "-",
        roiPercent: "-",
        days: {
          count: 0,
          amount: 0,
          roi: 0,
          chart:[]
        },
        week: {
          count: 0,
          amount: 0,
          roi: 0,
          chart:[]
        },
        months: {
          count: 0,
          amount: 0,
          roi: 0,
          chart:[]
        }
      }
    }
  }
};

const auth = (state: DefaultStateI = defaultState, action: AuthDispatchTypes): DefaultStateI => {
    switch (action.type) {
      case AUTH_LOADING:
        return {
          ...state,
          loading: true,
          token: undefined,
          error: ""
        };
      case AUTH_FAIL:
        return {
          ...state,
          loading: false,
          token: undefined,
          error: action.payload.error,
        };
      case AUTH_SUCCESS:
        return {
          ...state,
          loading: false,
          token: action.payload.user.id,
          user: action.payload.user,
          level: action.payload.level,
          hasInvest: action.payload.hasInvest,
          canInvest: action.payload.canInvest,
          isPremium: action.payload.isPremium,
          error: ""
        };

      case FINANCIAL_PROFILE:
        const financial = action.payload;
        return {
          ...state,
          financial: {...financial}
        };

      case GET_REFERRAL_INFO_REQUEST:
        return {
          ...state,
          referral: {
            ...state.referral,
            loading: true,
            error: ''
          }
        };
      case GET_REFERRAL_INFO:
        return {
          ...state,
          referral: {
            ...state.referral,
            userCount: action.payload.userCount,
            totalPoint: action.payload.totalPoint,
            links: action.payload.links,
            loading: false,
            error: ''
          }
        };
      case GET_REFERRAL_INFO_FAILURE:
        return {
          ...state,
          referral: {
            ...state.referral,
            loading: false,
            error: action.payload.error
          }
        };

      case GET_REFERRAL_HISTORY_REQUEST:
        return {
          ...state,
          referral: {
            ...state.referral,
            history: {
              ...state.referral.history,
              items: state.referral.history?.items || [],
              total: state.referral.history?.total || 0,
              loading: action.payload.history.loading,
              error: action.payload.history.error
            }
          }
        };
      case GET_REFERRAL_HISTORY:
        return {
          ...state,
          referral: {
            ...state.referral,
            history: {
              ...state.referral.history,
              ...action.payload
            }
          }
        };
      case GET_REFERRAL_HISTORY_FAILURE:
        return {
          ...state,
          referral: {
            ...state.referral,
            history: {
              ...state.referral.history,
              items: state.referral.history?.items || [],
              total: state.referral.history?.total || 0,
              loading: action.payload.history.loading,
              error: action.payload.history.error
            }
          }
        };

      case GET_NOTIFICATIONS_REQUEST:
        return {
          ...state,
          notifications: {
            ...state.notifications,
            data: state.notifications.data || [],
            loading: action.payload.loading,
            error: action.payload.error,
          }
        };
      case GET_NOTIFICATIONS_FAILURE:
        return {
          ...state,
          notifications: {
            ...state.notifications,
            loading: action.payload.loading,
            error: action.payload.error,
          }
        };
      case GET_NOTIFICATIONS_SUCCESS:
        return {
          ...state,
          notifications: {
            ...state.notifications,
            data: action.payload.reset ? action.payload.data : [...state.notifications.data, ...action.payload.data],
            loading: false,
            error: '',
            total: action.payload.total,
            new:
              action.payload.reset
                ? action.payload.data.filter(item => !item.isSeen).length
                : action.payload.data.filter(item => !item.isSeen).length + state.notifications.new

          }
        };

      case POST_EMAIL_SETTINGS_REQUEST:
        return {
          ...state,
          setting: {
            ...state.setting,
            email: {
              ...state.setting.email,
              loading: true,
              error: "",
              status: ""
            }
          }
        };
      case POST_EMAIL_SETTINGS_SUCCESS:
        return {
          ...state,
          user: action.payload.user,
          setting: {
            ...state.setting,
            email: {
              ...state.setting.email,
              status: action.payload.status,
              loading: false,
              error: ""
            }
          }
        };
      case POST_EMAIL_SETTINGS_FAILURE:
        return {
          ...state,
          setting: {
            ...state.setting,
            email: {
              ...state.setting.email,
              ...action.payload,
              loading: false,
              status: ""
            }
          }
        };

      case GET_FINANCIAL_DETAILS_REQUEST:
        return {
          ...state,
          financialDetails: {
            ...state.financialDetails,
            error: "",
            loading: true
          }
        };
      case GET_FINANCIAL_DETAILS_SUCCESS:
        return {
          ...state,
          financialDetails: {
            ...state.financialDetails,
            error: "",
            loading: false,
            data: action.payload.data
          }
        };
      case GET_FINANCIAL_DETAILS_FAILURE:
        return {
          ...state,
          financialDetails: {
            ...state.financialDetails,
            error: action.payload.error,
            loading: false,
          }
        };

      default:
        return state
    }
  };

export default auth;