import {FAQItemI, FAQTypes} from "../actions/faq";

import {
  SET_FAQ, SET_FAQ_FAILURE, SET_FAQ_REQUEST
} from '../constants/actionTypes';

export interface InvestRequestsDefaultStateI {
  items: FAQItemI[],
  loading: boolean,
  error: string,
}

const defaultState: InvestRequestsDefaultStateI = {
  items: [],
  loading: false,
  error: '',
};


 const faq = (state: InvestRequestsDefaultStateI = defaultState, action: FAQTypes): InvestRequestsDefaultStateI => {
  switch (action.type) {
    case SET_FAQ_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SET_FAQ_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
      };
    case SET_FAQ:
      return {
        ...state,
        items: action.payload.items,
        loading: false,
        error: '',
      };
    default:
      return state
  }
};
export default faq