import {ChartI, ProfitTypes,} from "../actions/profit";

import {
  SET_PROFIT_CHART_HISTORY,
  SET_PROFIT_CHART_HISTORY_FAILURE,
  SET_PROFIT_CHART_REQUEST,
  SET_PROFIT_HISTORY,
  SET_PROFIT_HISTORY_FAILURE,
  SET_PROFIT_HISTORY_REQUEST
} from '../constants/actionTypes';

import {ProfitDto} from "../models/responses/profit.model";

export interface ProfitRequestsDefaultStateI {
  history: {
    items: ProfitDto[],
    total: number,
    loading: boolean,
    reset?: boolean,
    error: string,
  },
  chart: {
    items: ChartI[],
    error: string
    loading: boolean
  }

}

const defaultState: ProfitRequestsDefaultStateI = {
  history: {
    items: [],
    total: 0,
    loading: false,
    reset: false,
    error: '',
  },
  chart: {
    items: [],
    error: '',
    loading:false
  }

};

const setCurrentProfit = (state: ProfitRequestsDefaultStateI, action: ProfitRequestsDefaultStateI) => {

  if (action.history.reset) {
    return {
      ...state,
      history: {
        items: action.history.items,
        total: action.history.total,
        error: '',
        loading: false,
      }
    }
  } else {
    return {
      ...state,
      history: {
        items: [...state.history.items, ...action.history.items],
        total: state.history.total + action.history.items.length,
        error: '',
        loading: false,
      }
    }
  }

};


 const profit = (state: ProfitRequestsDefaultStateI = defaultState, action: ProfitTypes): ProfitRequestsDefaultStateI => {
  switch (action.type) {
    case SET_PROFIT_HISTORY_REQUEST:
      return {
        ...state,
        history: {
          ...state.history,
          loading: true,
          error: '',
        }
      };
    case SET_PROFIT_HISTORY:
      return setCurrentProfit(state, {
        ...state,
        history: {
          ...state.history,
          items: action.payload.items,
          total: action.payload.total,
          reset: action.payload.reset,
        }
      });
    case SET_PROFIT_HISTORY_FAILURE:
      return {
        ...state,
        history: {
          ...state.history,
          error: action.payload.error,
          loading: false,
        }
      };
    case SET_PROFIT_CHART_REQUEST:
      return {
        ...state,
        chart: {
          ...state.chart,
          loading: true,
          error: '',
        }
      };
    case SET_PROFIT_CHART_HISTORY:
      return {
        ...state,
        chart: {
          items: action.payload.items,
          loading: false,
          error: ''
        }
      };
    case SET_PROFIT_CHART_HISTORY_FAILURE:
      return {
        ...state,
        chart: {
          items: [...state.chart.items],
          loading: false,
          error: action.payload.error
        }
      };
    default:
      return state
  }
};
export default profit