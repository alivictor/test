import {
    ArbitrageTypes,
    CoinMarketArbitrageGraphResponse,
    ICurrencies,
    IExchangers,
    market,
    marketResponse,
} from "../actions/arbitrage";

import {
    SET_ARBITRAGE_FAILURE,
    SET_ARBITRAGE_GRAPH,
    SET_ARBITRAGE_GRAPH_FAILURE,
    SET_ARBITRAGE_GRAPH_REQUEST,
    SET_ARBITRAGE_SMB_GRAPH,
    SET_ARBITRAGE_SMB_GRAPH_FAILURE,
    SET_ARBITRAGE_SMB_GRAPH_REQUEST,
    SET_ARBITRAGE_TABLE
} from '../constants/actionTypes'


export interface LiveArbitrageDefaultStateI {
    currencies: ICurrencies[],
    exchangers: IExchangers[],
    lastUpdate: string,
    error: string,
    graph: {
        linear?: CoinMarketArbitrageGraphResponse,
        candle: any,
        error: string,
        loading: boolean
    },
    smb: {
        data?: any[]
        error: string,
        loading: boolean
    },

}

const defaultState: LiveArbitrageDefaultStateI = {
    currencies: [],
    exchangers: [],
    lastUpdate: "",
    error: '',
    graph: {
        linear: {
            id: 1,
            name: "",
            symbol: "",
            quotes: [
                {
                    time_open: "",
                    time_close: "",
                    time_high: "",
                    time_low: "",
                    quote: {
                        EUR: {
                            open: 0,
                            high: 0,
                            low: 0,
                            close: 0,
                            volume: 0,
                            market_cap: 0,
                            timestamp: "",
                        }
                    }
                }
            ]
        },
        candle: {
            id: 1,
            name: "",
            symbol: "",
            quotes: [
                {
                    open: "",
                    close: "",
                    high: "",
                    low: "",
                    quote: {
                        open: 0,
                        high: 0,
                        low: 0,
                        close: 0,
                        volume: 0,
                        market_cap: 0,
                        timestamp: "",
                    }
                }
            ]
        },
        error: '',
        loading: false
    },
    smb: {
        data: [{
            id: 1,
            name: "Bitcoin",
            symbol: "BTC",
            quotes: [
                {
                    time_open: "2020-11-26T00:00:00.000Z",
                    time_close: "2020-11-26T23:59:59.999Z",
                    time_high: "2020-11-26T00:40:34.000Z",
                    time_low: "2020-11-26T17:28:41.000Z",
                    quote: {
                        EUR: {
                            open: 15717.501595812408,
                            high: 15819.380919602578,
                            low: 13723.832329177669,
                            close: 14399.097585586302,
                            volume: 51546757189.15141,
                            market_cap: 267177055587.73538,
                            timestamp: "2020-11-26T23:59:06.000Z"
                        }
                    }
                }
            ]
        }],
        error: '',
        loading: false
    },
};

const setArbitrageTable = (state: LiveArbitrageDefaultStateI, action: LiveArbitrageDefaultStateI) => {
    action.currencies.map((currency) => {
        let markets: market[] = [];
        action.exchangers.map(() => markets.push(marketResponse));
        currency.markets.map((market) => {
            action.exchangers.map((exchanger, exchangerIndex) => {
                if (market.exchanger.id === exchanger.id) {
                    markets.splice(exchangerIndex, 1, market);
                }
                return false;
            });
            return false;
        });
        currency.markets = markets;
        markets = [];
        return false;
    });

    return {
        ...state,
        currencies: action.currencies,
        exchangers: action.exchangers,
        lastUpdate: action.lastUpdate,
        error: action.error
    }
};

const arbitrage = (state: LiveArbitrageDefaultStateI = defaultState, action: ArbitrageTypes): LiveArbitrageDefaultStateI => {
    switch (action.type) {
        case SET_ARBITRAGE_TABLE:
            return setArbitrageTable(state, {
                ...state,
                currencies: action.payload.currencies,
                exchangers: action.payload.exchangers,
                lastUpdate: action.payload.lastUpdate,
                error: '',
                graph: state.graph
            });
        case SET_ARBITRAGE_FAILURE:
            return {
                ...state,
                error: action.payload.error
            };
        case SET_ARBITRAGE_GRAPH_REQUEST:
            return {
                ...state,
                graph: {
                    ...state.graph,
                    error: "",
                    loading: action.payload.loading
                }
            };
        case SET_ARBITRAGE_GRAPH_FAILURE:
            return {
                ...state,
                graph: {
                    ...state.graph,
                    error: action.payload.error,
                    loading: false
                }
            };
        case SET_ARBITRAGE_GRAPH:
            return {
                ...state,
                graph: {
                    linear: action.payload.linear,
                    candle: action.payload.candle,
                    error: "",
                    loading: false
                }
            };
        case SET_ARBITRAGE_SMB_GRAPH_REQUEST:
            return {
                ...state,
                graph: {
                    ...state.graph,
                    error: "",
                    loading: action.payload.loading
                }
            };
        case SET_ARBITRAGE_SMB_GRAPH_FAILURE:
            return {
                ...state,
                smb: {
                    ...state.smb,
                    error: action.payload.error,
                    loading: false
                }
            };
        case SET_ARBITRAGE_SMB_GRAPH:
            return {
                ...state,
                smb: {
                    data: action.payload.data,
                    error: "",
                    loading: false
                }
            };
        default:
            return state
    }
};

export default arbitrage;