import {TransactionTypes,} from "../actions/transaction";

import {
    GET_TOKEN_TRANSACTION_HISTORY_FAILURE,
    GET_TOKEN_TRANSACTION_HISTORY_REQUEST,
    GET_TOKEN_TRANSACTION_HISTORY_SUCCESS,
    SET_TRANSACTION_HISTORY,
    SET_TRANSACTION_HISTORY_FAILURE,
    SET_TRANSACTION_HISTORY_REQUEST
} from '../constants/actionTypes';

import {TransactionDto} from "../models/responses/transaction.model";

export interface TransactionRequestsDefaultStateI {
    history: {
        items: TransactionDto[],
        total: number,
        loading: boolean,
        reset?: boolean,
        error: string,
    },
    tokenTransactionHistory: {
        items: TransactionDto[],
        total: number,
        loading: boolean,
        reset?: boolean,
        error: string,
    }

}

const defaultState: TransactionRequestsDefaultStateI = {
    history: {
        items: [],
        total: 0,
        loading: false,
        reset: false,
        error: '',
    },
    tokenTransactionHistory: {
        items: [],
        total: 0,
        loading: false,
        reset: false,
        error: '',
    }
};

const setCurrentTransaction = (state: TransactionRequestsDefaultStateI, action: TransactionRequestsDefaultStateI) => {

    if (action.history.reset) {
        return {
            ...state,
            history: {
                items: action.history.items,
                total: action.history.total,
                error: '',
                loading: false,
            }
        }
    } else {
        return {
            ...state,
            history: {
                items: [...state.history.items, ...action.history.items],
                total: state.history.total + action.history.items.length,
                error: '',
                loading: false,
            }
        }
    }

};


const Transaction = (state: TransactionRequestsDefaultStateI = defaultState, action: TransactionTypes): TransactionRequestsDefaultStateI => {
    switch (action.type) {
        case SET_TRANSACTION_HISTORY_REQUEST:
            return {
                ...state,
                history: {
                    ...state.history,
                    loading: true,
                    error: '',
                }
            };
        case SET_TRANSACTION_HISTORY:
            return setCurrentTransaction(state, {
                ...state,
                history: {
                    ...state.history,
                    items: action.payload.items,
                    total: action.payload.total,
                    reset: action.payload.reset,
                }
            });
        case SET_TRANSACTION_HISTORY_FAILURE:
            return {
                ...state,
                history: {
                    ...state.history,
                    error: action.payload.error,
                    loading: false,
                }
            };

        case GET_TOKEN_TRANSACTION_HISTORY_REQUEST:
            return {
                ...state,
                tokenTransactionHistory: {
                    ...state.tokenTransactionHistory,
                    loading: true,
                    error: '',
                }
            };
        case GET_TOKEN_TRANSACTION_HISTORY_SUCCESS:
            return {
                ...state,
                tokenTransactionHistory: {
                    ...state.tokenTransactionHistory,
                    items: action.payload.items,
                    total: action.payload.total,
                    reset: action.payload.reset,
                    loading: false
                }
            };
        case GET_TOKEN_TRANSACTION_HISTORY_FAILURE:
            return {
                ...state,
                tokenTransactionHistory: {
                    ...state.tokenTransactionHistory,
                    error: action.payload.error,
                    loading: false,
                    items: [],
                    total: 0
                }
            };
        default:
            return state
    }
};
export default Transaction