import {LevelsTypes,} from "../actions/levels/LevelsActions";
import {levelInterface} from "../models/levels.model";

import {
  SET_LEVELS, SET_LEVELS_FAILURE, SET_LEVELS_REQUEST
} from '../constants/actionTypes';

interface DefaultStateI {
  items: levelInterface[],
  loading: boolean,
  error: string
}

const defaultState: DefaultStateI = {
  items: [],
  loading: false,
  error: ""
};

 const levels = (state: DefaultStateI = defaultState, action: LevelsTypes): DefaultStateI => {
  switch (action.type) {
    case SET_LEVELS:
      return {
        ...state,
        items: action.payload.filter(item => item.title !== "Starter"),
        loading: false,
        error: '',
      };
    case SET_LEVELS_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SET_LEVELS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state
  }
}
export default levels