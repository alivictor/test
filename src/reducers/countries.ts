import {CountriesTypes} from "../actions/countries";
import CountryModel, {CountryCodeModel} from "../models/country.model";

import {
  SET_COUNTRIES, SET_COUNTRIES_CODES
} from '../constants/actionTypes';

export interface CountriesDefaultStateI {
  countries: CountryModel[],
  codes: CountryCodeModel[]
}

const defaultState: CountriesDefaultStateI = {
  countries: [],
  codes: []
};

const setCountries = (state: CountriesDefaultStateI, action: any) => {
  return {
    codes: state.codes,
    countries: action.payload.countries,
  }
};


const setCountriesCodes = (state: CountriesDefaultStateI, action: any) => {
  return {
    countries: state.countries,
    codes: action.payload.codes,
  }
};


const countryReducer = (state: CountriesDefaultStateI = defaultState,
                               action: CountriesTypes): CountriesDefaultStateI => {
  switch (action.type) {
    case SET_COUNTRIES:
      return setCountries(state, action)
    case SET_COUNTRIES_CODES:
      return setCountriesCodes(state, action)
    default:
      return state
  }
}

export default countryReducer;