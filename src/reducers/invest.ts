import {activeInvestDefaultState, IInvestRequests, InvestsTypes,} from "../actions/invest";
import {
    CHANGE_INVEST_STEP,
    GET_TOKEN_INVESTS_FAILURE,
    GET_TOKEN_INVESTS_REQUEST,
    GET_TOKEN_INVESTS_SUCCESS,
    SEND_INVEST_FAILURE,
    SEND_INVEST_REQUEST,
    SEND_INVEST_SUCCESS,
    SET_ACTIVE_INVEST,
    SET_INVESTS,
    SET_INVESTS_FAILURE,
    SET_INVESTS_REQUEST,
    UPLOAD_INVEST_FILE_FAILURE,
    UPLOAD_INVEST_FILE_REQUEST,
    UPLOAD_INVEST_FILE_SUCCESS
} from '../constants/actionTypes';
import {setLocalStorage} from "../assets/helpers/storage";
import TokenInvestModel from "../models/tokenInvest.model";

export interface InvestRequestsDefaultStateI {
    items: IInvestRequests[],
    total: number,
    page?: number,
    loading: boolean,
    reset: boolean,
    error: string,
    request: {
        status: string,
        loading: boolean,
        error: string,
        data: any
    },
    upload: {
        status: string,
        loading: boolean,
        error: string,
    },
    stepNumber: number,
    activeInvest?: IInvestRequests,
    tokenInvests: {
        items: TokenInvestModel[],
        total: number,
        page?: number,
        loading: boolean,
        reset: boolean,
        error: string,
    }

}

const defaultState: InvestRequestsDefaultStateI = {
    items: [],
    total: 0,
    loading: false,
    reset: false,
    error: '',
    request: {
        status: '',
        loading: false,
        error: '',
        data: null
    },
    upload: {
        status: '',
        loading: false,
        error: '',
    },
    stepNumber: 1,
    activeInvest: localStorage.getItem('active_invest') !== undefined ? JSON.parse(localStorage.getItem('active_invest') || JSON.stringify(activeInvestDefaultState)) : activeInvestDefaultState,
    // activeInvest: activeInvestDefaultState
    tokenInvests: {
        items: [],
        total: 0,
        loading: false,
        reset: false,
        error: '',
    }
};

const setCurrentInvests = (state: InvestRequestsDefaultStateI, action: InvestRequestsDefaultStateI) => {

    if (action.reset) {
        let activeInvest = action.items.filter((item) => item.lastActivity.status === "InCompleted")[0]
            || action.items.filter((item) => item.lastActivity.status === "Pending")[0];

        if (!activeInvest) {
            localStorage.removeItem('active_invest')
        } else {
            setLocalStorage('active_invest', activeInvest)
            // localStorage.setItem('active_invest', JSON.stringify(activeInvest))
        }

        return {
            ...state,
            items: action.items,
            total: action.total,
            error: '',
            loading: false,
            activeInvest: activeInvest ? activeInvest : action.page === 1 ? activeInvestDefaultState : state.activeInvest
        }
    } else {
        return {
            ...state,
            items: [...state.items, ...action.items],
            total: state.total + action.items.length,
            error: '',
            loading: false,
            activeInvest: state.activeInvest
        }
    }
};

const investsReducer = (state: InvestRequestsDefaultStateI = defaultState, action: InvestsTypes): InvestRequestsDefaultStateI => {
    switch (action.type) {
        case SET_INVESTS_REQUEST:
            return {
                ...state,
                loading: true,
                error: '',
            };
        case SET_INVESTS:
            return setCurrentInvests(state, {
                ...state,
                items: action.payload.items,
                total: action.payload.total,
                reset: action.payload.reset,
                page: action.payload.page
            });
        case SET_INVESTS_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                loading: false,
            };

        case SEND_INVEST_REQUEST:
            return {
                ...state,
                request: {
                    ...state.request,
                    loading: true,
                    error: '',
                    status: '',
                }
            };
        case SEND_INVEST_FAILURE:
            return {
                ...state,
                request: {
                    ...state.request,
                    error: action.payload.error,
                    loading: false,
                }
            };
        case SEND_INVEST_SUCCESS:
            return {
                ...state,
                request: {
                    ...state.request,
                    status: action.payload.status,
                    loading: false,
                    error: '',
                    data: action.payload.data
                }
            };
        case UPLOAD_INVEST_FILE_REQUEST:
            return {
                ...state,
                upload: {
                    ...state.upload,
                    loading: true,
                    error: '',
                    status: '',
                }
            };
        case UPLOAD_INVEST_FILE_FAILURE:
            return {
                ...state,
                upload: {
                    ...state.upload,
                    error: action.payload.error,
                    loading: false,
                }
            };
        case UPLOAD_INVEST_FILE_SUCCESS:
            return {
                ...state,
                upload: {
                    ...state.upload,
                    status: action.payload.status,
                    loading: false,
                    error: '',
                }
            };
        case CHANGE_INVEST_STEP:
            return {
                ...state,
                stepNumber: action.payload.stepNumber
            };
        case SET_ACTIVE_INVEST:
            return {
                ...state,
                activeInvest: action.payload.data
            };


        case GET_TOKEN_INVESTS_REQUEST:
            return {
                ...state,
                tokenInvests: {
                    ...state.tokenInvests,
                    loading: true,
                    error: '',
                }
            };
        case GET_TOKEN_INVESTS_SUCCESS:

            return {
                ...state,
                tokenInvests: {
                    ...state.tokenInvests,
                    items: !action.payload.reset ? [...state.tokenInvests.items, ...action.payload.items] : action.payload.items,
                    total: action.payload.total,
                    reset: action.payload.reset,
                    page: action.payload.page,
                    loading: false,
                }
            }
        case GET_TOKEN_INVESTS_FAILURE:
            return {
                ...state,
                tokenInvests: {
                    ...state.tokenInvests,
                    error: action.payload.error,
                    loading: false,
                }
            };
        default:
            return state
    }
};
export default investsReducer