import {
  KYCTypes,
} from "../actions/kyc";


import {
  FETCH_KYC_STATUS,
  SEND_KYC_FAILURE,
  SEND_KYC_REQUEST,
  SEND_KYC_SUCCESS
} from '../constants/actionTypes';

import CountryModel from "../models/country.model";

export interface KYCRequestsDefaultStateI {
  kyc_status: boolean | null,
  kyc_request: KYCRequestI | null,
  request: {
    status: string,
    loading: boolean,
    error: string,
  }
}

const defaultState: KYCRequestsDefaultStateI = {
  kyc_status: null,
  kyc_request: null,
  request: {
    status: '',
    loading: false,
    error: '',
  }
};

export interface KYCRequestI {
  address: string,
  attachments: { id: number, path: string, name: string, type: string }[],
  birthDate: string,
  email: string,
  firstName: string,
  id: number,
  lastName: string,
  phone: string,
  requestAt: string,
  status: string,
  country: CountryModel
}


 const Kyc = (state: KYCRequestsDefaultStateI = defaultState, action: KYCTypes): KYCRequestsDefaultStateI => {
  switch (action.type) {
    case SEND_KYC_REQUEST:
      return {
        ...state,
        request: {
          loading: true,
          error: '',
          status: '',
        }
      };
    case SEND_KYC_FAILURE:
      return {
        ...state,
        request: {
          loading: false,
          error: action.payload.error,
          status: '',
        }
      };
    case SEND_KYC_SUCCESS:
      return {
        ...state,
        request: {
          loading: false,
          error: '',
          status: action.payload.status,
        }
      };
    case FETCH_KYC_STATUS:
      return {
        ...state,
        kyc_status: action.payload.kyc_status,
        kyc_request: action.payload.kyc_request,
      };

    default:
      return state
  }
};
export default Kyc