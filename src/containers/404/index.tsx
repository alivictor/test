import React from 'react';
import ErrorMessage from "../../components/UiKits/ErrorMessage/ErrorMessage";
import URLs from "../../constants/URLs";
import {useTranslation} from "react-i18next";

const My404Com = () => {

    const {
        t,
    } = useTranslation();
  return (
    <div className="w-100 h-auto">
      <ErrorMessage src={"/assets/img/png/404-min.png"} title={t('confirmSection.page_not_found')}
                    body={t('confirmSection.page_not_found')}
                    buttonTitle={t('confirmSection.go_back')} buttonLink={URLs.dashboard_home}/>
    </div>
  );
};

export default My404Com;
