import React from 'react';
import {RouteConfigComponentProps} from "react-router-config";
import { Redirect} from 'react-router-dom';
import {getLocalStorage} from "../../assets/helpers/storage";

const Splash = ({ route }: RouteConfigComponentProps) => {

  if (localStorage.getItem('userAuth')) {
    if(getLocalStorage('userData') !== "undefined"){
      if(getLocalStorage('userData').registerMembership === "token-investor") {
        return <Redirect to={{
          pathname: "/token-dashboard/home"
        }} />
      }else if(getLocalStorage('userData').registerMembership === "arbitrage-investor") {
        return <Redirect to={{
          pathname: "/dashboard/home"
        }} />
      }
    } else {

      return <Redirect to={{
        pathname: "/dashboard/home"
      }} />
    }

  }else {
    return <Redirect to={{
      pathname: "/auth/login"
    }} />
  }
}

export default Splash;
