import React, {useRef, useEffect, useState} from 'react';
import '../../Dashboard/Home/home.scss';
import Loading from "../../../components/UiKits/Loading/Loading";

// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import SwiperCore, {Navigation, EffectFade, Autoplay} from 'swiper';
import 'swiper/swiper-bundle.min.css';
import InfoCart from "./Components/InfoCart";
import {connect} from "react-redux";
import {GetUserTokenFullDetails, GetUserTokenSMBPriceChart} from "../../../actions/report";
import {useTranslation} from "react-i18next";
import {getLocalStorage} from "../../../assets/helpers/storage";
import UserModel from "../../../models/user.model";
import {currencyFormatter, readableNumber} from "../../../assets/helpers/utilities";
import ProfitChart from "../../../components/Charts/Line/ProfitChart";
import ProfitBarChart from "../../../components/Charts/Line/ProfitBarChart";
import ClassicTable, {thead} from "../../../components/UiKits/ClassicTable/ClassicTable";
import ReactPaginate from "react-paginate";
import {NextButton, PrevButton} from "../../../components/UiKits/Pagination/Pagination";
import {filterI} from "../../Dashboard/WithDrawal";
import TokenInvestRows from "./Components/TokenInvestRows";
import {FetchTokenInvests} from "../../../actions/invest";
import TokenInvestModel from "../../../models/tokenInvest.model";
import TotalCard from "./Components/TotalCard";

SwiperCore.use([Navigation, EffectFade, Autoplay]);

const Articles = React.lazy(() => import("../../../components/Articles/Articles"));

interface IProps {
    GetUserTokenSMBPriceChart: any,
    fetchData: any,
    loading: boolean,
    error: any,
    data: any,
    chartLoading: boolean,
    chartData: { date: string, value: number, amount: number }[],
    chartError: any,

    FetchTokenInvests: any,
    FetchTokenInvestsData: TokenInvestModel[],
    FetchTokenInvestsTotal: number,
    FetchTokenInvestsLoading: boolean,
    FetchTokenInvestsError: any,
}


const Index = (Props: IProps) => {
    const [userTokenFullDetails, setUserTokenFullDetails] = useState<{
        user: UserModel,
        financial: {
            balance: number
            deposits: number
            marketCap: number
            roi: number | null
            smbPrice: string
            tokens: number
            avgSmbPrice: number
        }
    } | null>(null)
    const [filter, setFilter] = useState<filterI>({take: 5, skip: 0});
    const investHistoryRef = useRef(null)
    // General scroll to element function
    const scrollToRef = (ref: any) => window.scrollTo(0, ref.current.offsetTop)
    const executeScrollInvestHistory = () => scrollToRef(investHistoryRef)

    useEffect(() => {
        getLocalStorage('userTokenFullDetails') && setUserTokenFullDetails(getLocalStorage('userTokenFullDetails'))
        Props.fetchData();
        Props.GetUserTokenSMBPriceChart();
        Props.FetchTokenInvests(filter);
    }, [])


    useEffect(() => {
        getLocalStorage('userTokenFullDetails') && setUserTokenFullDetails(getLocalStorage('userTokenFullDetails'))
    }, [Props.loading])

    const {t} = useTranslation();


    const [activePage, setActivePage] = useState<number>(0);
    const onPageChange = (number: number) => {
        setActivePage(number);
        setFilter({...filter, skip: number * filter.take});
        Props.FetchTokenInvests({...filter, skip: number * filter.take});

    };


    const headers: thead[] = [
        {title: 'ID'},
        {title: t('invest.date')},
        {title: t('invest.amount')},
        {title: t('home.token_counts')},
        {title: t('home.token_price')},
        {title: t('invest.status')},
        {title: t('invest.tracking_code')},
    ];


    return (
        <div className="home mt-2 lg:my-4">

            <div className="grid lg:grid-cols-12 md:grid-rows-1">
                <div className="lg:col-span-8 col-span-6">
                    <TotalCard title={t('home.total_tokens_dashboard')}
                               value={userTokenFullDetails ? readableNumber(userTokenFullDetails.financial.tokens) : "-"}
                               bgColor='green'
                               mode='row'
                               button={`${t('home.average_buy_price')}: ${userTokenFullDetails ? currencyFormatter(userTokenFullDetails.financial.avgSmbPrice,6) : "-"}`} />
                    <div className='grid grid-cols-2 w-full'>
                        <div className='lg:col-span-1 md:col-span-1 sm:col-span-2 col-span-2 w-full'>
                            <TotalCard title={t('home.total_deposit')}
                                       value={userTokenFullDetails && currencyFormatter(userTokenFullDetails.financial.deposits)}
                                       bgColor='lightBlue'
                                       mode='col'
                                       button={ t('home.invest_history')} />
                        </div>
                        <div className='lg:col-span-1 md:col-span-1 sm:col-span-2 col-span-2 w-full md:pl-2 md:pr-1'>
                            <TotalCard title={t('home.total_balance')}
                                       value={userTokenFullDetails && currencyFormatter(userTokenFullDetails.financial.balance)}
                                       bgColor='blue'
                                       mode='col'
                                       button={t('home.transactions')} />
                        </div>
                        <Articles/>
                    </div>
                </div>
                <div className="mt-3 lg:col-span-4 col-span-6">
                    <div className='grid grid-rows-3 gap-5 w-full'>
                        <InfoCart icon='/assets/img/svg/icons/token-price.svg' title={t('home.token_price')} value={userTokenFullDetails && currencyFormatter(userTokenFullDetails.financial.smbPrice,6)} />
                        <InfoCart icon='/assets/img/svg/icons/market-cap.svg' title={t('home.market_cap')} value={userTokenFullDetails && currencyFormatter(userTokenFullDetails.financial.marketCap)} />
                        <InfoCart icon='/assets/img/svg/icons/ROI.svg' title={"ROI"} value={userTokenFullDetails ? readableNumber(userTokenFullDetails.financial.roi || 0) + "%" : "-"} />
                    </div>
                </div>
            </div>

            <div className="home--profit-second mt-4 bg-white py-3 md:py-4 w-full rounded-xl">
                {Props.chartError
                    ? <div className="text-center text-danger w-full">{Props.chartError}</div>
                    : Props.chartLoading
                        ? <Loading/>
                        : Props.chartData &&
                        Props.chartData.length !== 0
                            ?
                            <>
                                <ProfitChart
                                    data={Props.chartData.map(item => ({
                                        date: item.date,
                                        amount: item.value
                                    }))}
                                    paddingRight={Props.chartData.map(item => ({
                                        date: item.date,
                                        amount: item.value
                                    }))}
                                    profitKind={""}
                                    chartTitle={t('home.token_price_history')}
                                    bullet={false}
                                />
                                <p className="ml-6 text-minsk">{t('home.market_cap')}</p>
                                <div className="mt-6">
                                    <ProfitBarChart
                                        data={Props.chartData}
                                        paddingRight={Props.chartData}
                                    />
                                </div>
                            </>
                            : <div className="mb-4 mt-4 live-arbitrage--graph">
                                <div className="live-arbitrage--table rounded-lg bg-white">
                                    <div className="header px-6 rounded-tl-xl">
                                            <span className="text-minsk font-bold">
                                              Profit smb price
                                            </span>
                                    </div>
                                    <div className="flex justify-around items-center w-full h-64 my-20">
                                        {
                                            localStorage.getItem('i18nextLng')?.includes('en')
                                                ? <img src="/assets/img/svg/line-chart-nodata-en.svg"
                                                       alt=""/>
                                                : <img src="/assets/img/svg/line-chart-nodata-de.svg"
                                                       alt=""/>
                                        }
                                    </div>
                                </div>
                            </div>
                }
            </div>

            <div className="transaction-history live-arbitrage" id="invest-history" ref={investHistoryRef}>
                <div className="my-20 w-full">
                    <div className="my-3 flex flex-wrap justify-between items-center">
                        <div
                            className="pl-1 home-title text-normal text-minsk my-3">{t('invest.invest_history')}</div>
                    </div>
                    <ClassicTable error={Props.FetchTokenInvestsError} headers={headers}
                                  classes={" huge "}
                                  loading={Props.FetchTokenInvestsLoading} total={Props.FetchTokenInvestsTotal}
                                  children={<TokenInvestRows data={Props.FetchTokenInvestsData}/>}/>

                    {
                        Props.FetchTokenInvestsTotal > 5
                        &&
                        <div className="pagination sm:flex-1 sm:flex sm:items-center
                                        sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                            <ReactPaginate
                                previousLabel={PrevButton}
                                nextLabel={NextButton}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={Props.FetchTokenInvestsTotal / filter.take}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(number) => onPageChange(number.selected)}
                                forcePage={activePage}
                                containerClassName={'pagination'}
                                // subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                            />
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state: any) => {
    return {
        data: state.report.userTokenFullDetails.data,
        loading: state.report.userTokenFullDetails.loading,
        error: state.report.userTokenFullDetails.error,
        chartLoading: state.report.userTokenSMBPriceChart.loading,
        chartData: state.report.userTokenSMBPriceChart.data,
        chartError: state.report.userTokenSMBPriceChart.error,

        FetchTokenInvestsData: state.invest.tokenInvests.items,
        FetchTokenInvestsTotal: state.invest.tokenInvests.total,
        FetchTokenInvestsLoading: state.invest.tokenInvests.loading,
        FetchTokenInvestsError: state.invest.tokenInvests.error,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchData: () => dispatch(GetUserTokenFullDetails()),
        GetUserTokenSMBPriceChart: () => dispatch(GetUserTokenSMBPriceChart()),
        FetchTokenInvests: (filter: any) => dispatch(FetchTokenInvests(filter)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Index);
