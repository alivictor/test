import React from 'react';
import './TotalCard.scss';
import { currencyFormatter } from '../../../../../assets/helpers/utilities';
interface IProps {
  title: string,
  value: number | string,
  bgColor: 'green' | 'blue' | 'lightBlue',
  mode: 'col' | 'row',
  button: string,
}

const makeColor = (mode: string) => {
  switch (mode) {
    case 'blue': return { high: '#3670FB', low: '#6e98fc' };
    case 'green': return { high: '#31cd95', low: '#6bdbb3' };
    case 'lightBlue': return { high: '#edf2ff', low: '#fff' };
    default: return { high: '#3670FB', low: '#6e98fc' };
  }
}

const TotalCard = (Props: IProps) => {

  return (
    <div style={{ background: makeColor(Props.bgColor).high }}
      className="total-card w-full mt-2 lg:mt-4 ">
      <div className={`w-full flex ${Props.mode === 'col' ? 'flex-col' : 'lg:flex-row flex-col'} `}>
        <div className='infos' style={{ color: Props.bgColor === 'lightBlue' ? '#3670FB' : 'white' }}>
          <p>{Props.title}</p>
          <span>{currencyFormatter(Props.value)}</span>
        </div>
        <div className='flex items-center lg:flex-none md:flex-1 flex-1'>
          <button className={Props.mode === 'col' ? 'w-full' : 'lg:w-auto md:w-full w-full'} style={{
            background: makeColor(Props.bgColor).low,
            padding: Props.mode === 'col' ? '5px 20px' : '10px 20px',
            color: Props.bgColor === 'lightBlue' ? '#3670FB' : 'white'
          }}>{Props.button}</button>
        </div>
      </div>
    </div>
  );
};

export default TotalCard;
