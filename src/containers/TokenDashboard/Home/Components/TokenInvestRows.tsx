import React from 'react';
import {currencyFormatter, readableNumber, stringDate} from "../../../../assets/helpers/utilities";
import TokenInvestModel from "../../../../models/tokenInvest.model";


interface IProps {
    data: TokenInvestModel[]
}

const TokenInvestRows = (Props: IProps) => {
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                            <tr className="h-20" key={index}>
                                <td>{item.id}</td>
                                <td>{stringDate(item.createdAt)}</td>
                                <td className="rtl text-center"><span className="rtl text-center"> {readableNumber(parseFloat(item.price_amount))}</span></td>
                                <td className="rtl text-center"><span className="rtl text-center"> {readableNumber(parseFloat(item.token_amount))}</span></td>
                                <td className="rtl text-center"><span className="rtl text-center"> {currencyFormatter(parseFloat(item.smb_price))}</span></td>
                                <td>{item.status}</td>
                                <td>{item.saTrackingCode || "---"}</td>
                            </tr>
                    )
                })
            }


        </>
    )
};
export default TokenInvestRows;
