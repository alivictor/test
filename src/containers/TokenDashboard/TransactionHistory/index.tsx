import React, { useEffect, useState} from 'react';
import '../../Dashboard/Arbitrage/live-arbitrage.scss';
import ReactPaginate from "react-paginate";
import {NextButton, PrevButton} from "../../../components/UiKits/Pagination/Pagination";
import {filterI} from "../../Dashboard/WithDrawal";
import {connect} from "react-redux";
import {FetchTokenTransactionHistory} from "../../../actions/transaction";
import TransactionRows from "./components/TransactionRows";
import {thead} from "../../../components/UiKits/ClassicTable/ClassicTable";
import {useTranslation} from "react-i18next";
import {GET_TOKEN_TRANSACTION_HISTORY_FAILURE} from "../../../constants/actionTypes";
import {TokenTransactionModel} from "../../../models/responses/tokenTransaction.model";

const ClassicTable = React.lazy(() => import("../../../components/UiKits/ClassicTable/ClassicTable"));

interface IProps {
    fetchData: any,
    clearData: any,
    tableLoading: boolean,
    tableError: string,
    allData: TokenTransactionModel[],
    total: number,
}

const Index = (Props: IProps) => {
    const [filter, setFilter] = useState<filterI>({take: 5, skip: 0});
    useEffect(() => {
        Props.fetchData(filter);
        return function cleanup() {
            Props.clearData();
        };
    }, [filter]);
    const [activePage, setActivePage] = useState<number>(0);
    const onPageChange = (number: number) => {
        setActivePage(number);
        setFilter({...filter, skip: number * filter.take})
    };


    const {
        t,
    } = useTranslation();

    const headers: thead[] = [
        {title: t('home.token_counts')},
        {title: t('invest.amount')},
        {title: t('invest.status')},
        {title: t('invest.type')},
        {title: t('transaction_history.created_at')},
    ];


    return (
        <div className="transaction-history mt-8 live-arbitrage">
            <div className="my-12 w-full bg-white rounded-xl">
                <div className="px-8 pt-6 mb-3 flex flex-wrap ml-12  justify-between items-center">
                    <div className="pl-1 home-title text-normal text-minsk my-3">{t('sidebar.transaction_history')}</div>
                </div>
                <ClassicTable error={Props.tableError} headers={headers}
                              classes=" huge "
                              loading={Props.tableLoading} total={Props.total}
                              children={<TransactionRows data={Props.allData}/>}/>

                {
                    Props.total > 5
                    &&
                    <div className="pagination sm:flex-1 sm:flex sm:items-center
                                        sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                      <ReactPaginate
                        previousLabel={PrevButton}
                        nextLabel={NextButton}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={Props.total / filter.take}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={(number) => onPageChange(number.selected)}
                        forcePage={activePage}
                        containerClassName={'pagination'}
                          // subContainerClassName={'pages pagination'}
                        activeClassName={'active'}
                      />
                    </div>
                }
            </div>
        </div>
    );
};

const mapStateToProps = (state: any) => {
    return {
        allData: state.transaction.tokenTransactionHistory.items,
        total: state.transaction.tokenTransactionHistory.total,
        tableLoading: state.transaction.tokenTransactionHistory.loading,
        tableError: state.transaction.tokenTransactionHistory.error,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchData: (data: filterI) => dispatch(FetchTokenTransactionHistory(data)),
        clearData: () => dispatch({
            type: GET_TOKEN_TRANSACTION_HISTORY_FAILURE,
            payload: {
                items: [],
                total: 0,
                reset: true,
            }
        })
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Index);

