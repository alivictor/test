import React from 'react';
import {currencyFormatter, readableNumber, stringDate} from "../../../../assets/helpers/utilities";
import {TokenTransactionModel} from "../../../../models/responses/tokenTransaction.model";

interface IProps {
    data: TokenTransactionModel[]
}

const TransactionRows = (Props: IProps) => {
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                        <tr className="h-20" key={index}>
                            <td
                                className={`text-center rtl
                                  ${((item.type.includes('Deposit') || item.type.includes('Profit')) && item.token_amount!=0) && 'text-green-700'}   
                                  ${(item.type  === "Withdrawal" && item.token_amount!=0) && "text-red-700"}   
                                  ${(item.type  === "WithdrawalBlock" || item.token_amount==0) && "text-gray-800"}   
                                   font-bold 
                                `}
                            >

                                {readableNumber(item.token_amount)}
                                {((item.type.includes('Deposit') || item.type.includes('Profit')) && item.token_amount!=0) && ' + '}
                                {(item.type  === "Withdrawal" && item.token_amount!=0) && " - "}

                            </td>

                            <td
                                className={`text-center rtl
                                  ${((item.type.includes('Deposit') || item.type.includes('Profit')) && item.price_amount!=0) && 'text-green-700'}   
                                  ${(item.type  === "Withdrawal" && item.price_amount!=0) && "text-red-700"}   
                                  ${(item.type  === "WithdrawalBlock" || item.price_amount==0) && "text-gray-800"}   
                                   font-bold 
                                `}
                            >

                                {currencyFormatter(item.price_amount)}
                                {((item.type.includes('Deposit') || item.type.includes('Profit')) && item.price_amount!=0) && ' + '}
                                {(item.type  === "Withdrawal" && item.price_amount!=0) && " - "}

                            </td>

                            <td>{item.status}</td>
                            <td>{item.type}</td>
                            <td>{item.createdAt}</td>
                        </tr>
                    )
                })
            }
        </>
    )
};
export default TransactionRows;
