import React from 'react';
import qs from 'qs';
import URLs from "../../constants/URLs";
// import image from '../../assets/img/svg/logo-typography.svg';

import ErrorMessage from "../../components/UiKits/ErrorMessage/ErrorMessage";

interface IProps {
  location: any
}

const DeepLinks = (Props: IProps) => {
  const query: any = qs.parse(Props.location.search, {ignoreQueryPrefix: true});

  return (
    <div>
      <ErrorMessage src="/assets/img/svg/logo-typography.svg" title=""
                    body=""
                    buttonTitle="Open App" buttonLink={`${URLs.register}?ref=${query.ref}&ln=${query.ln}`}/>
    </div>
  );
};

export default DeepLinks;
