
export interface errorsType {
  email?: string,
  firstname?: string,
  lastname?: string,
  phone?: string,
  password?: string,
  confirmPassword?: string,
  membership?: string,
}


export const initialErrors : errorsType = {
  email: undefined,
  firstname: undefined,
  lastname: undefined,
  phone: undefined,
  password: undefined,
  confirmPassword: undefined,
  membership: undefined,
}

export interface Values {
  email: string,
  firstname: string,
  lastname: string,
  phone: string,
  password: string,
  confirmPassword: string,
  country: any,
  code: any,
  promo_code: string,
  membership: string,
}


export const initialValues : Values = {
  email: "",
  firstname: "",
  lastname: "",
  phone: "",
  password: "",
  confirmPassword: "",
  country: "",
  code: "",
  promo_code: "",
  membership: "",
};

