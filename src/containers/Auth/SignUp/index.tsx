import React, {Suspense, useEffect, useState} from 'react';
import Radio from "../../../components/UiKits/Radio/Radio";
import {Link, Redirect} from "react-router-dom";
import URLs from "../../../constants/URLs";
import {Formik, Field, Form, ErrorMessage} from 'formik';
import {errorsType, Values} from "./interfaces";
import {strongRegex} from "./validate";
import {Decrypt} from "../../../apis/encryption/encryption/encryption";
import Countries from "../../../components/Countries/Countries";
import AuthRegisterSubmitDtoModel from "../../../models/responses/authRegisterSubmitDto.model";
import Loading from "../../../components/UiKits/Loading/Loading";
import CountryCode from "../../../components/CountryCode/CountryCode";
import {readableCountryCode} from "../../../assets/helpers/utilities";
import qs from 'qs';
import {
    useTranslation,
} from 'react-i18next';
import {PostRegisterService} from "../../../apis/auth";
import {toast} from "react-toastify";

const Alert = React.lazy(() => import('../../../components/UiKits/Warning/Warning'));

interface IProps {
    location: any
}

const Index = (Props: IProps) => {
    const query: any = qs.parse(Props.location.search, {ignoreQueryPrefix: true});

    const [citizenRadio, setCitizenRadio] = useState<boolean>(false);
    const [ageRadio, setAgeRadio] = useState<boolean>(false);
    const [termsRadio, setTermsRadio] = useState<boolean>(false);
    const [country, setCountry] = useState<string>("79");
    const [code, setCode] = useState<string>("49");
    const [gender, seGender] = useState<string>("");
    const [isSubmited, setIsSubmited] = useState<boolean>(false);
    const [warning, setWarning] = useState<string>("");
    const [redirect, setRedirect] = useState<boolean>(false);
    const termsLink = localStorage.getItem('i18nextLng')?.includes('en') ? '/en/terms-conditions/' : '/geschaeftsbedingungen/'
    const {
        t,
        i18n
    } = useTranslation();

    useEffect(() => {
        query.ln &&
        i18n.changeLanguage('de-GE');

        return function cleanup() {
            setRedirect(false);
            setWarning("")
        };
    }, []);

    const validate = (values: Values) => {
        const errors: errorsType = {};
        if (!values.email) {
            errors.email = t('confirmSection.required');
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = t('confirmSection.invalid_email_address');
        }
        if (!values.password) {
            errors.password = t('confirmSection.required');
        } else if (strongRegex(values.password) < 2) {
            // errors.password = "Enter stronger password"
            errors.password = t('errors.weak_password');
        } else if (values.password.length < 8) {
            errors.password = t('errors.password_min_length_8');
        }

        if (!values.firstname) {
            errors.firstname = t('confirmSection.required');
        }
        if (!values.lastname) {
            errors.lastname = t('confirmSection.required');
        }

        if (!values.phone) {
            errors.phone = t('confirmSection.required');
        }
        if (!values.membership) {
            errors.membership = t('confirmSection.required');
        }

        if (!values.confirmPassword) {
            errors.confirmPassword = t('confirmSection.required');
        } else if (values.password !== values.confirmPassword) {
            errors.confirmPassword = t('errors.passwords_dont_match')
        }
        return errors;
    };

    return (
        <Suspense fallback={<Loading/>}>
            <Formik
                initialValues={{
                    email: "" || query.e,
                    firstname: "" || query.fn,
                    lastname: "" || query.lsn,
                    phone: "" || query.p,
                    membership: "" || query.membership,
                    password: "",
                    confirmPassword: "",
                    country: 79,
                    code: "49" || query.cc,
                    promo_code: query.ref || "",
                }}
                validate={validate}
                onSubmit={async (
                    values: Values,
                ) => {
                    try {
                        if (!citizenRadio || !ageRadio || !termsRadio) {
                            if (!citizenRadio) {
                                setWarning(t('errors.citizen_required'))
                            }
                            if (!ageRadio) {
                                setWarning(t('errors.years_required'))
                            }
                            if (!termsRadio) {
                                setWarning(t('errors.terms_required'))
                            }
                        } else {
                            setIsSubmited(true);
                            let data: AuthRegisterSubmitDtoModel = {
                                mail: values.email?.trim(),
                                password: values.password,
                                firstName: values.firstname?.trim(),
                                lastName: values.lastname?.trim(),
                                agreement: {
                                    isAbove18: citizenRadio,
                                    imNotUsCitizen: ageRadio
                                },
                                phone: {
                                    countryCode: readableCountryCode(code),
                                    number: values.phone.length === 11 ? values.phone.slice(1, 11).trim() : values.phone?.trim(),
                                    additionalNumber: ""
                                },
                                promotionalCode: values.promo_code,
                                country: country,
                                gender: gender,
                                membership: values.membership,
                            };

                            const res = await PostRegisterService(data);
                            if (res.status === 201) {
                                toast.dismiss();
                                localStorage.setItem('id', JSON.parse(Decrypt(res.data.data)).id);
                                localStorage.setItem('nationalCode', data.phone.countryCode);
                                localStorage.setItem('email', data.mail);
                                //  redirect to verify page
                                setRedirect(true);
                            }
                            setIsSubmited(false);
                        }
                    } catch (err) {
                        if (err && err.status) {
                            if (err.response.status === 500) {
                                setWarning(err.response.data.message)
                            } else {
                                setWarning(JSON.parse(Decrypt(err.response.data.data)).client_message)
                            }
                        } else {
                            setWarning(t('confirmSection.try_again'))
                        }
                        setIsSubmited(false);
                    }
                }}
            >
                {({values, setFieldValue}) => (
                    <Form className="px-8 pt-6 pb-8 mt-4 mb-4">
                        {redirect && <Redirect to={{pathname: URLs.verify}}/>}
                        <div className="mb-6 text-left headTitle">
                            <div className=" inline-block "><h6
                                className="font-bold text-black">{t('signUp.header')}</h6></div>
                            <div><p>{t('signUp.title')}</p></div>
                        </div>
                        <div className="mt-12 mb-2">
                            <label className="block login-container--label
                                text-left text-gray-700 text-sm font-bold mb-4" htmlFor="firstname">
                                {t('signUp.firstName')}
                            </label>
                            <Field
                                value={values.firstname}
                                required={true}
                                className=" login-container--input
                                    appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                id="firstname" type="text" placeholder={t('signUp.enter_your_name')}/>

                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                text-left text-gray-700 text-sm font-bold mb-4" htmlFor="lastname">
                                {t('signUp.lastName')}
                            </label>

                            <Field
                                value={values.lastname}
                                required={true}
                                className=" login-container--input appearance-none border rounded w-full
                                 py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                id="lastname" type="text" placeholder={t('signUp.enter_your_last_name')}/>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                text-left text-gray-700 text-sm font-bold mb-4" htmlFor="country">
                                {t('signUp.select_country')}
                            </label>
                            <div className="relative">
                                <select
                                    value={country}
                                    onChange={(e: any) => setCountry(e.target.value)}
                                    required={true}
                                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="country">
                                    <Countries kind="name" default={undefined}/>
                                </select>
                                <div
                                    className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-salmon">
                                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 20 20">
                                        <path
                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <Radio required={false}
                                   text={t('signUp.i_am_not_US_citizen_and_i_dont_live_in_US')} id="citizenRadio"
                                   onChangeStatus={(data: boolean) => setCitizenRadio(data)} checked={citizenRadio}/>
                        </div>
                        <div className="mt-6 mb-1">
                            <Radio
                                required={false}
                                text={t('signUp.i_am_18_years_old')} id="ageRadio"
                                onChangeStatus={(data: boolean) => setAgeRadio(data)}
                                checked={ageRadio}/>
                        </div>
                        <div className="mt-6 mb-2 ">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4">
                                {t('signUp.gender')}
                            </label>

                            <div className="relative">
                                <select
                                    value={gender}
                                    onChange={(e: any) => seGender(e.target.value)}
                                    required={true}
                                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="gender"
                                    name="gender"
                                >
                                    <option value="">{t('signUp.gender')}</option>
                                    <option value="Male">{t('signUp.male')}</option>
                                    <option value="Female">{t('signUp.female')}</option>
                                </select>
                                <div
                                    className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 20 20">
                                        <path
                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="email">
                                {t('signUp.e_mail')}
                            </label>

                            <Field
                                value={values.email}
                                required={true}
                                name="email"
                                style={{paddingLeft: ".75rem"}}
                                className="login-container--input appearance-none border rounded w-full
                                            py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                id="email" type="email" placeholder={t('login.email_placeholder')}/>

                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="email"/>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="code">
                                {t('signUp.code')}
                            </label>
                            <div className="relative">
                                <select
                                    value={code}
                                    onChange={(e: any) => setCode(e.target.value)}
                                    required={true}
                                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="code">
                                    <CountryCode activeCode={query.cc}/>
                                </select>
                                <div
                                    className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 20 20">
                                        <path
                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="phone">
                                {t('signUp.phone_number')}
                            </label>
                            <Field
                                value={values.phone}
                                required={true}
                                type="tel"
                                // pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                // maxLength={11}
                                style={{paddingLeft: ".75rem"}}
                                className=" login-container--input
                                    appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                id="phone" name="phone" placeholder={t('signUp.enter_phone_number')}/>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="membership">
                                {t('signUp.membership')}

                            </label>
                            <div className="relative">
                                <select
                                    value={values.membership}
                                    onChange={(e: any) => setFieldValue('membership', e.target.value)}
                                    required={true}
                                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="membership">
                                    <option value={""}>{t('home.select_account_type')}</option>
                                    <option value={"token-investor"}>{t('signUp.token_investor')}</option>
                                    <option value={"arbitrage-investor"}>{t('signUp.arbitrage_investor')}</option>
                                </select>
                                <div
                                    className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-salmon">
                                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 20 20">
                                        <path
                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="membership"/>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="password">
                                {t('signUp.password')}
                            </label>
                            <Field
                                value={values.password}
                                required={true}
                                name="password"
                                minLength={8}
                                className="appearance-none border login-container--input
                                        border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
                                id="password"
                                style={{paddingLeft: ".75rem"}}
                                type="password"
                                placeholder={t('signUp.enter_password')}/>
                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="password"/>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                    text-left text-gray-700 text-sm font-bold mb-4" htmlFor="confirmPassword">
                                {t('signUp.confirm_password')}
                            </label>
                            <Field
                                value={values.confirmPassword}
                                required={true}
                                style={{paddingLeft: ".75rem"}}
                                minLength={8}
                                className="appearance-none border login-container--input
                                    border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
                                id="confirmPassword"
                                name="confirmPassword"
                                type="password"
                                placeholder={t('signUp.enter_password')}/>

                            <div className="field-error text-red-600 text-left">
                                <ErrorMessage name="confirmPassword"/>
                            </div>
                        </div>
                        <div className="mt-6 mb-2">
                            <label className="block login-container--label
                                text-left text-gray-700 text-sm font-bold mb-4" htmlFor="promo_code">
                                {t('signUp.promotional_code')}

                            </label>
                            <Field
                                value={values.promo_code}
                                style={{paddingLeft: ".75rem"}}
                                className=" login-container--input
                                appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                id="promo_code" name="promo_code" type="text"
                                placeholder={t('signUp.enter_your_promotional_code')}/>
                        </div>
                        <div className="mt-4 mb-6">
                            {/*<a href={`https://smartbitrage.com/${lang}/terms-conditions/`} target="_blank" rel="noopener noreferrer">*/}
                            <Radio
                                required={false}
                                text={<a href={`https://pntcap.com${termsLink}`} target="_blank"
                                         rel="noopener noreferrer">{t('signUp.i_agree_with_terms_and_conditions_of_use')}</a>}
                                id="termsRadio"
                                onChangeStatus={(data: boolean) => setTermsRadio(data)}
                                checked={termsRadio}/>
                            {/*</a>*/}

                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                className="login-container--button w-full text-white font-bold py-3 px-4 rounded focus:outline-none"
                                disabled={isSubmited}
                                type="submit">
                                {t('signUp.sign_up')}
                                {isSubmited && <Loading stylesProp={{
                                    width: '35px',
                                    height: 'auto',
                                    position: 'absolute',
                                    top: "20%",
                                    left: '48%'
                                }}/>}
                            </button>
                        </div>
                        <div className="mt-4">
                            {
                                warning !== ""
                                &&
                                <Alert warning={warning} setWarning={(data: string) => setWarning(data)}/>
                            }
                        </div>
                        <div className="flex items-center justify-start flex-wrap mt-4">
                            <Link to={URLs.login}
                                  className="block align-baseline text-sm text-gray-500 hover:text-blue-800">
                                {t('signUp.already_have_an_account')} &nbsp; <span
                                className="text-dark-blue">{t('signUp.sign_in')}</span>
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Suspense>
    );
};

export default Index;
