export const strongRegex = (password : any) => {
  let point =0;
  let upperCase = new RegExp("^(?=.*[A-Z])");
  let lowerCase = new RegExp("^(?=.*[a-z])");
  let number = new RegExp("^(?=.*[0-9])");
  let specialCharacter = new RegExp("^(?=.*[!@#%&])");

  upperCase.test(password) && point++;
  lowerCase.test(password) && point++;
  number.test(password) && point++;
  specialCharacter.test(password) && point++;

  return point
}