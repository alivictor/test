export interface errorsType {
  email?: string,
  password?: string,
}


export const initialErrors : errorsType = {
  email: undefined,
  password: undefined,
}

export interface Values {
  email?: string,
  password?: string,
}


export const initialValues : Values = {
  email: "",
  password: "",
}