import React, {Suspense, useEffect, useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import URLs from "../../../constants/URLs";
import '../../../layouts/auth/authWraper.scss';
import {initialValues, Values} from "./interfaces";
import {validate} from "./validate";
import {Form, Field, Formik, FormikHelpers} from "formik";
import {Decrypt} from "../../../apis/encryption/encryption/encryption";
import {LoginAction} from "../../../actions/auth";
import {connect} from 'react-redux';
import UserModel from "../../../models/user.model";
import Loading from "../../../components/UiKits/Loading/Loading";
import {LevelItemInterface} from "../../../models/levels.model";
import {useTranslation} from "react-i18next";
import {PostLoginService} from "../../../apis/auth";
import {toast} from "react-toastify";
import {getLocalStorage} from "../../../assets/helpers/storage";

const Alert = React.lazy(() => import('../../../components/UiKits/Warning/Warning'));

const Index = (Props: any) => {

    const [isSubmited, setIsSubmited] = useState<boolean>(false);
    const [hidePw, setHidePw] = useState<boolean>(true);
    const [warning, setWarning] = useState<string>("");
    const [redirectToVerify, setRedirectToVerify] = useState<boolean>(false);

    const {
        t,
    } = useTranslation();
    useEffect(() => {
        return function cleanup() {
            setWarning("")
        };
    }, []);

    return (
        <Suspense fallback={<Loading/>}>
            <Formik
                initialValues={initialValues}
                validate={validate}
                onSubmit={async (
                    values: Values,
                    {setSubmitting}: FormikHelpers<Values>
                ) => {
                    try {
                        setIsSubmited(true);
                        let data = {
                            username: values.email,
                            password: values.password,
                        };

                        const res = await PostLoginService(data);

                        if (res.status === 201) {
                            //  redirect to dashboard home page
                            localStorage.setItem('IP', JSON.parse(Decrypt(res.data.data)).isPremium);
                            localStorage.setItem('HI', JSON.parse(Decrypt(res.data.data)).hasInvest);
                            localStorage.setItem('CI', JSON.parse(Decrypt(res.data.data)).canInvest);
                            localStorage.setItem('userAuth', JSON.parse(Decrypt(res.data.data)).token);
                            localStorage.setItem('userData', JSON.stringify(JSON.parse(Decrypt(res.data.data)).user));
                            localStorage.setItem('userLevel', JSON.stringify(JSON.parse(Decrypt(res.data.data)).level));
                            localStorage.setItem('memberships', JSON.stringify(JSON.parse(Decrypt(res.data.data)).memberships));

                            let canWithdrawal = JSON.parse(Decrypt(res.data.data)).canWithdrawal || false;
                            let canWithdrawalAt = JSON.parse(Decrypt(res.data.data)).canWithdrawalAt || false;

                            localStorage.setItem('canWithdrawal', JSON.stringify(canWithdrawal));
                            localStorage.setItem('canWithdrawalAt', JSON.stringify(canWithdrawalAt));

                            toast.dismiss();
                            Props.LoginAction({
                                user: JSON.parse(Decrypt(res.data.data)).user,
                                level: JSON.parse(Decrypt(res.data.data)).level,
                                hasEvent: JSON.parse(Decrypt(res.data.data)).hasInvest,
                                canInvest: JSON.parse(Decrypt(res.data.data)).canInvest,
                                isPremium: JSON.parse(Decrypt(res.data.data)).isPremium,
                            });
                            let registerMembership = getLocalStorage('userData').registerMembership || JSON.parse(Decrypt(res.data.data)).memberships[0].slug

                            if (registerMembership) {
                                if (registerMembership === "arbitrage-investor") {
                                    window.location.href = URLs.dashboard_home
                                }
                                if (registerMembership === "token-investor") {
                                    window.location.href = URLs.token_dashboard_home
                                }
                            } else {
                                window.location.href = URLs.dashboard_home
                            }

                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    } catch (err) {
                        if (err && err.status) {
                            setWarning(JSON.parse(Decrypt(err.data.data)).client_message);

                            if (JSON.parse(Decrypt(err.data.data)).code === "EXC_CDCCC9A8") {
                                toast.error(JSON.parse(Decrypt(err.data.data)).client_message + ". " + t('verify.error_message'));
                                localStorage.setItem('email', values.email || "");
                                localStorage.setItem('id', JSON.parse(Decrypt(err.data.data)).parameters.id);
                                setTimeout(() => {
                                    setRedirectToVerify(true)
                                }, 2000)
                            }
                        } else {
                            setWarning(t('confirmSection.internet_access_error'))
                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    }
                }}
            >
                {({values}) => (
                    <Form className="px-8 pt-6 pb-8 mt-4 mb-4">
                        {redirectToVerify && <Redirect to={{pathname: URLs.verify}}/>}
                        <div className="mb-6 text-left headTitle">
                            <div className="inline-block"><h6 className="font-bold text-black ">{t('login.header')}</h6>
                            </div>
                            <div><p>{t('login.title')}</p></div>
                        </div>
                        <div className="mt-12 mb-6">
                            <label className="block login-container--label
               text-left text-gray-700 text-sm font-bold mb-4" htmlFor="email">
                                {t('login.email')}
                            </label>
                            <div className="form--control hide" id="emailInput">
                                <Field
                                    value={values.email}
                                    required={true}
                                    name="email"
                                    className=" login-container--input
                                    appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                    id="email" type="email" placeholder={t('login.email_placeholder')}
                                />
                            </div>
                        </div>
                        <div className="mb-1">
                            <label className="block login-container--label
                                text-left text-gray-700 text-sm font-bold mb-4" htmlFor="password">
                                {t('login.password')}
                            </label>
                            <div className="form--control" id="passwordInput">

                                <Field
                                    value={values.password}
                                    minLength={8}
                                    required={true}
                                    className="appearance-none border login-container--input
                                        border-red-500 rounded w-full py-4 px-3 text-gray-700 mb-3 leading-tight focus:outline-none"
                                    id="password"
                                    name="password"
                                    type={hidePw ? "password" : "text"}
                                    placeholder={t('login.password_placeholder')}/>
                                <span onClick={() => setHidePw((prevHidePw: boolean) => !prevHidePw)}
                                      className="flex justify-center align-middle cursor-pointer">
                                  {
                                      hidePw
                                          ? <img src={"/assets/img/svg/icons/hide-input-icon.svg"} className="w-6 h-auto" alt=""/>
                                          : <img src={"/assets/img/svg/icons/Eye.svg"} className="w-6 h-auto" alt=""/>
                                  }
                                </span>
                            </div>

                        </div>

                        <div className="flex items-center justify-end flex-wrap mt-1 mb-4">
                            <Link to={URLs.forgotPassword}
                                  className="block align-baseline text-sm text-dark-blue hover:text-blue-800">
                                <span>
                                  {t('login.forgot_your_password')}
                                </span>
                            </Link>
                        </div>


                        <div className="flex items-center justify-center">
                            <button
                                className="login-container--button w-full text-white font-bold py-3 px-4 rounded focus:outline-none"
                                disabled={isSubmited}
                                type="submit">
                                {t('login.signIn')}
                                {isSubmited &&
                                <Loading stylesProp={{
                                    width: '35px',
                                    height: 'auto',
                                    position: 'absolute',
                                    top: "20%",
                                    left: '48%'
                                }}/>}

                            </button>
                        </div>
                        <div className="flex items-center justify-center mt-4">
                            {
                                warning !== ""
                                &&
                                <Alert warning={warning} setWarning={(data: string) => setWarning(data)}/>
                            }
                        </div>
                        <div className="flex items-center justify-start flex-wrap mt-4">
                            <Link to={URLs.register}
                                  className="block align-baseline text-sm text-gray-500 hover:text-blue-800">
                                {t('login.dont_have_an_password')} &nbsp; <span
                                className="text-dark-blue">{t('signUp.header')}</span>
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Suspense>
    );
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        LoginAction: (data: { user: UserModel, level: LevelItemInterface, hasInvest: boolean, canInvest: boolean, isPremium: boolean }) => dispatch(LoginAction(data))
    }
};

export default connect(null, mapDispatchToProps)(Index);
