export interface errorsType {
  code?: string,
  password?: string,
  confirmPassword?: string,

}


export const initialErrors : errorsType = {
  code: undefined,
  password: undefined,
  confirmPassword: undefined,
}

export interface Values {
  code?: string,
  password?: string,
  confirmPassword?: string,
}

export const initialValues : Values = {
  code: "",
  password: "",
  confirmPassword: "",
}