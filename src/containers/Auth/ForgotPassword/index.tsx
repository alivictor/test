import React, {Suspense, useState} from 'react';
import {Link, Redirect} from "react-router-dom";
import URLs from "../../../constants/URLs";
import {initialValues, Values} from "./interfaces";
import {Formik, Form, Field, FormikHelpers} from "formik";
import {Decrypt} from "../../../apis/encryption/encryption/encryption";
import Loading from "../../../components/UiKits/Loading/Loading";
import {useTranslation} from "react-i18next";
import {PostForgetPasswordService} from "../../../apis/auth";
import {toast} from "react-toastify";

const Alert = React.lazy(() => import('../../../components/UiKits/Warning/Warning'));

const Index = () => {
    const [isSubmited, setIsSubmited] = useState<boolean>(false);
    const [warning, setWarning] = useState<string>("");
    const [redirect, setRedirect] = useState<boolean>(false);

    const {
        t,
    } = useTranslation();

    return (
        <Suspense fallback={<Loading/>}>
            <Formik
                initialValues={initialValues}
                onSubmit={async (
                    values: Values,
                    {setSubmitting}: FormikHelpers<Values>
                ) => {
                    try {
                        setIsSubmited(true);

                        //send apis
                        let data = {
                            username: values.email?.trim(),
                        };

                        const res = await PostForgetPasswordService(data);
                        if (res.status === 201) {
                            toast.dismiss();
                            //  redirect to dashboard home page
                            localStorage.setItem('userId', JSON.parse(Decrypt(res.data.data)).id);
                            setRedirect(true);
                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    } catch (err) {
                        if (err && err.status) {
                            setWarning(JSON.parse(Decrypt(err.data.data)).message);
                        } else {
                            setWarning(t('confirmSection.try_again'))
                        }
                        setSubmitting(false);
                        setIsSubmited(false);
                    }
                }}
            >
                {({values}) => (
                    <Form className="px-8 pt-6 pb-8 mt-4 mb-4">
                        {redirect && <Redirect to={{pathname: URLs.resetPassword}}/>}
                        <div className="mb-6 text-left headTitle">
                            <div className="inline-block"><h6
                                className="font-bold text-black ">{t('forgot_password.header')}</h6></div>
                            <div><p>{t('forgot_password.description')}</p></div>
                        </div>
                        <div className="mt-12 mb-6">
                            <label className="block login-container--label
                                     text-left text-gray-700 text-sm font-bold mb-4" htmlFor="email">
                                {t('signUp.e_mail')}
                            </label>
                            <div className="form--control hide" id="emailInput">

                                <Field
                                    required={true}
                                    value={values.email}
                                    className=" login-container--input
                                    appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none"
                                    id="email" name="email" type="email" placeholder={t('login.email_placeholder')}/>
                            </div>
                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                disabled={isSubmited}
                                className="login-container--button w-full text-white font-bold py-3 px-4 rounded focus:outline-none relative"
                                type="submit">
                                {t('forgot_password.button')}
                                {isSubmited && <Loading stylesProp={{
                                    width: '35px',
                                    height: 'auto',
                                    position: 'absolute',
                                    top: "20%",
                                    left: '48%'
                                }}/>}
                            </button>
                        </div>
                        <div className="flex items-center justify-center mt-4">
                            {
                                warning !== ""
                                &&
                                <Alert warning={warning} setWarning={(data: string) => setWarning(data)}/>
                            }
                        </div>
                        <div className="flex items-center justify-start flex-wrap mt-4">
                            <Link to={URLs.login}
                                  className="block align-baseline text-sm text-gray-500 hover:text-blue-800">
                                {t('verify.remember_password')} &nbsp; <span
                                className="text-dark-blue">{t('verify.back_to_login')}</span>
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Suspense>
    );
};

export default Index;
