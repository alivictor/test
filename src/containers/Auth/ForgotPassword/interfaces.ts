export interface errorsType {
  email?: string,
}


export const initialErrors : errorsType = {
  email: undefined,
}

export interface Values {
  email?: string,
}


export const initialValues : Values = {
  email: "",
}