import React, {Suspense, useEffect} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import '../AccountDetails/account-details.scss';
import './kyc.scss';
import {connect} from 'react-redux';
import {ValuesI, FileValuesI} from "./interfaces";
import {KYCRequestI} from "../../../reducers/kyc";
import UserModel from "../../../models/user.model";
import {useTranslation} from "react-i18next";
import KYCForm from './components/form';
import {FetchKYCStatus, SendKYCVerification} from "../../../actions/kyc";


interface IProps {
    // phone: PhoneModel,
    FetchKYCStatus: any,
    SendKYCVerification: any,
    loading: boolean,
    countries: number,
    kyc_status: boolean | null,
    kyc_request: KYCRequestI | null,
    error: string,
    status: string,
    userData: UserModel
}

const Index = (Props: IProps) => {

    useEffect(() => {
        Props.FetchKYCStatus();
    }, []);

    useEffect(() => {
        if (Props.status === "Success") {
            setTimeout(() => {
                window.scrollTo(0, 0);
            }, 1500)
        }
    }, [Props.status]);

    const {
        t,
    } = useTranslation();

    return (
        <div className="account-details kyc w-full mt-12 px-8">
            <div
                className="wrapper bg-white rounded-xl p-16 w-full m-0 m-auto mb-12">
                <h3 className="referral-title text-minsk font-bold text-xl pl-4">{t('kyc.kyc_status')}</h3>

                <div className="py-4 mb-3 flex flex-wrap justify-between items-center w-full">
                    <div className="flex-auto my-2 sm:my-0">
                        {
                            Props.kyc_request && Props.kyc_request.status === 'Pending' &&
                            <img src={"/assets/img/svg/icons/pending-circle-icon.svg"} alt="" className="inline-block mr-3"/>
                        }
                        {
                            Props.kyc_request && Props.kyc_request.status === 'Reject' &&
                            <img src={"/assets/img/svg/icons/close-icon.svg"} alt="" className="inline-block mr-3"/>
                        }
                        {
                            Props.kyc_request && Props.kyc_request.status === 'InCompleted' &&
                            <img src={"/assets/img/svg/icons/close-icon.svg"} alt="" className="inline-block mr-3"/>
                        }

                        {
                            Props.kyc_request && Props.kyc_request.status !== 'Pending' && Props.kyc_status &&
                            <img src={"/assets/img/svg/icons/verify-circle-icon.svg"} alt="" className="inline-block mr-3"/>
                        }
                        {
                            !Props.kyc_request && !Props.kyc_status &&
                            <img src={"/assets/img/svg/icons/close-icon.svg"} alt="" className="inline-block mr-3"/>
                        }

                        <p className="inline-block pb-0 text-minsk font-bold">
                            {t('kyc.your_kyc_status')}:&nbsp;
                            {Props.kyc_status ? t('confirmSection.verified') : t('confirmSection.unverified')}
                        </p>
                    </div>
                    <div className="flex-auto my-2 sm:my-0">
                        {
                            Props.kyc_request && Props.kyc_request.status === 'Pending' && <div
                              className="bg-water-melon px-6 py-4 rounded-lg ml-auto block sm:w-40 md:w-48 text-center flex items-center justify-center">
                              <span className="mr-4"><img src={"/assets/img/svg/icons/pending-little-icon.svg"} alt=""/></span>
                                {t('confirmSection.pending')}
                            </div>
                        }
                        {
                            Props.kyc_request && Props.kyc_request.status === 'Reject' && <div
                              className="bg-water-melon px-6 py-4 rounded-lg ml-auto block sm:w-40 md:w-48 text-center flex items-center justify-center">
                              <span className="mr-4"><img src={"/assets/img/svg/icons/unverified-icon.svg"} alt=""/></span>
                                {t('confirmSection.rejected')}
                            </div>
                        }
                        {
                            Props.kyc_request && Props.kyc_request.status === 'InCompleted' && !Props.kyc_status &&
                            <div
                              className="bg-water-melon px-6 py-4 rounded-lg ml-auto block sm:w-40 md:w-48 text-center flex items-center justify-center">
                              <span className="mr-4"><img src={"/assets/img/svg/icons/unverified-icon.svg"} alt=""/></span>
                                {t('confirmSection.inCompleted')}
                            </div>
                        }

                        {
                            Props.kyc_request && Props.kyc_request.status !== 'Pending' && Props.kyc_status && <div
                              className="bg-green-haze px-6 py-4 rounded-lg ml-auto block sm:w-40 md:w-48 text-center flex items-center justify-center">
                              <span className="mr-4"><img src={"/assets/img/svg/icons/verified-icon.svg"} alt=""/></span>
                                {t('confirmSection.verified')}
                            </div>
                        }
                        {
                            !Props.kyc_request && !Props.kyc_status && <div
                              className="bg-water-melon px-6 py-4 rounded-lg ml-auto block sm:w-40 md:w-48 text-center flex items-center justify-center">
                              <span className="mr-4"><img src={"/assets/img/svg/icons/unverified-icon.svg"} alt=""/></span>
                                {t('confirmSection.unverified')}
                            </div>
                        }
                    </div>
                </div>
                {
                    Props.kyc_status !== null && !Props.kyc_status
                    &&
                    <>
                      <div className=" sm:w-full md:w-11/12 text-minsk my-3 py-6">
                        <p>
                            {t('kyc.description')}
                        </p>
                      </div>
                      <div className="kyc-form mt-6 pb-12 px-4">
                        <div
                          className="home-title text-normal text-minsk font-bold w-full text-left text-big pb-4">
                            {t('kyc.description_country')}
                        </div>

                        <KYCForm
                          userData={Props.userData}
                          kyc_request={Props.kyc_request}
                          onSubmit={(values: any, files: any) =>Props.SendKYCVerification(values,files)}
                          countries={Props.countries}
                          loading={Props.loading}
                          error={Props.error}
                          status={Props.status}/>
                      </div>
                    </>
                }

            </div>
        </div>
    );
};


const mapStateToProps = (state: any) => {
    return {
        loading: state.kyc.request.loading,
        error: state.kyc.request.error,
        status: state.kyc.request.status,
        kyc_status: state.kyc.kyc_status,
        kyc_request: state.kyc.kyc_request,
        userData: state.auth.user,
        countries: state.country.countries.length
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        FetchKYCStatus: () => dispatch(FetchKYCStatus()),
        SendKYCVerification: (data: ValuesI, files: FileValuesI) => dispatch(SendKYCVerification(data, files))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Index);
