export interface errorsType {
    email?: string,
    firstname?: string,
    lastname?: string,
    phone?: string,
    address?: string,
    birthDate?: string,
}

export const initialErrors: errorsType = {
    email: undefined,
    firstname: undefined,
    lastname: undefined,
    phone: undefined,
    address: undefined,
    birthDate: undefined,

};

export interface ValuesI {
    email: string,
    firstName: string,
    lastName: string,
    phone: string,
    address: string,
    birthDate: string,
    country: number,
}

export interface FileValuesI {
  additionals?: string | null,
  bankStatements: string | null,
  documentBackSides: string | null,
  documents: string | null,
}

export const initialFileValues: FileValuesI = {
    additionals: null,
    bankStatements: null,
    documentBackSides: null,
    documents: null,
};
