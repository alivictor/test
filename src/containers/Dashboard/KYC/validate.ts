import {ValuesI, errorsType} from "./interfaces";

export const validate = (values : ValuesI) => {
  const errors : errorsType = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if(!values.firstName){
    errors.firstname = "Required"
  }
  if(!values.lastName){
    errors.lastname = "Required"
  }
  if(!values.phone){
    errors.phone = "Required"
  }

  return errors;
};

