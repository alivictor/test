import React from 'react';
import './Cart.scss';
import {Link} from 'react-router-dom';
import {useTranslation} from "react-i18next";

interface IProps {
  style: any,
  color: any,
  title: string,
  value: string,
  link: {
    key: string,
    value: string,
    ref?: boolean,
    onClick?: any,
  } | null,
  badges?: {
    color?: string
    icon1?: string
    icon2: string | null
    id: number
    slug?: string
    title: string
    weight?: number
  }[],

}

const Cart = (Props: IProps) => {
  const {
    t,
  } = useTranslation();
  return (
    <div className="home-cart flex flex-wrap  items-stretch" style={Props.style}>
      <div className="w-full flex flex-nowrap justify-between items-center h-12">
        <div className="text-white home-cart--title">
          <p>{Props.title}</p>
          <p className="rtl text-left">{Props.value}</p>
        </div>

        {
          Props.link
            &&
          <Link to={Props.link.value} className="home-cart--link text-white"
                onClick={Props.link.onClick}
                style={{backgroundColor: Props.color}}>
            <p>{Props.link.key}</p>
            <div>
            <span>
              {t('home.more')} &nbsp;<img src="/assets/img/svg/icons/angle.svg" alt="" className="inline-block"/></span>
            </div>
          </Link>
        }

      </div>

      <div className="w-full flex flex-wrap justify-start items-center h-18">
        {Props.badges &&
        Props.badges.map((item) => <div key={item.id} className="home-cart--button text-white inline-flex items-center mx-1"
                                        style={{backgroundColor: Props.color}}>
          {
            item.icon2 && <span><img src={item.icon2} alt="" className="inline-block"/></span>
          }

          <span className="ml-1 rtl text-left">{item.title}</span>
        </div>)
        }
      </div>
    </div>
  );
};

export default Cart;
