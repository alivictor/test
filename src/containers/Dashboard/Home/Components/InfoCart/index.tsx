import React from 'react';
import './InfoCart.scss';

interface IProps {
  style: any,
  color: any,
  icon: string,
  title: string,
  value: string
}

const InfoCart = (Props: IProps) => {

  return (
    <div className="info-cart flex flex-wrap items-stretch mt-2 lg:mt-4 mx-2" style={Props.style}>
      <div className="w-full flex flex-nowrap justify-start items-center h-full">
        <div>
          <img src={Props.icon} alt=""/>
        </div>

        <div className="ml-4">
          <div className="text text-minsk ">{Props.title}</div>
          <div style={{color:Props.color}} className="font-bold"><p className="rtl text-left">{Props.value}</p></div>
        </div>
      </div>
    </div>
  );
};

export default InfoCart;
