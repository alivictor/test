import React from 'react';
import './GridRow.scss';

interface IProps {
  children: any
}

const GridRow = (Props: IProps) => {
  return (
    <div className={`grid-row w-full flex flex-nowrap items-center`}>
      {Props.children}
    </div>
  );
};

export default GridRow;
