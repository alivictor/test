import React, {useState} from 'react';
import {useTranslation} from "react-i18next";
import moment from "moment-timezone";
import {currencyFormatter} from "../../../../../assets/helpers/utilities";
import Loading from "../../../../../components/UiKits/Loading/Loading";
import BarChart from "../../../../../components/Charts/Bar";
import DonutChart from "../../../../../components/Charts/Donut";
import DailyProfitCard from "../../Components2/dailyProfitCard";

interface IProps {
    data: any,
    loading: boolean,
    error: string | null,
    title: string,
    setProfitDataFilter: any,
    profitDataFilter: any,
}

const Index = (Props: IProps) => {
    const [resourceDetailWithDate, setResourceDetailWithDate] = useState<string>("percent")

    const {
        t,
    } = useTranslation();

    const ProfitFilterItems = [
        {
            title: t('home.daily_profit'),
            slug: "Daily",
        },
        {
            title: t('home.weekly_profit'),
            slug: "Weekly",
        },
        {
            title: t('home.monthly_profit'),
            slug: "Monthly",
        },
        {
            title: t('home.yearly_profit'),
            slug: "Yearly",
        },
    ]

    return (
        <div className="grid grid-cols-12 pt-4 home--profit-second-bar-chart">
            <div className="col-span-9 my-3 md:my-0 h-full bg-white rounded-xl div2 px-4 md:pl-10 pt-8 pb-4">
                <div className="w-full grid grid-cols-1 md:grid-cols-4">
                    <div className="col-span-2 my-3 md:my-0 px-3">
                        <DailyProfitCard
                            value={
                                Props.data && !Props.loading
                                    ?
                                    currencyFormatter(Props.data.currentProfit)
                                    : "-"
                            }
                            title={t('home.current') + " " + Props.title}
                        />
                    </div>
                    <div className="col-span-2 my-3 md:my-0 px-3">
                        <DailyProfitCard
                            value={
                                Props.data && !Props.loading
                                    ?
                                    currencyFormatter(Props.data.expectedProfit)
                                    : "-"
                            }
                            title={t('home.expected') + " " + Props.title}
                        />
                    </div>

                </div>

                {
                    Props.error
                    && <div
                        className="text-danger text-center h-full flex items-center justify-center">{Props.error}</div>
                }
                {!Props.error &&
                Props.loading
                    ? <Loading/>
                    : <>
                        {
                            Props.data.result.length === 0
                                ?
                                <div
                                    className="text-danger text-center h-full flex items-center justify-center">
                                    {
                                        localStorage.getItem('i18nextLng')?.includes('en')
                                            ?
                                            <img src="/assets/img/svg/bar-chart-nodata-en.svg" alt=""/>
                                            :
                                            <img src="/assets/img/svg/bar-chart-nodata-de.svg" alt=""/>
                                    }
                                </div>
                                : <BarChart
                                    data={Props.data.result.map((item: any) => parseFloat(item.amount) > 1 ? parseFloat(item.amount).toFixed(2) : parseFloat(item.amount).toFixed(4))}
                                    colors={Props.data.result.map((item: any) => "#" + item.color)}
                                    labels={Props.data.result.map((item: any) => item.title)}
                                />
                        }

                    </>
                }

            </div>

            <div
                className="col-span-3 my-6 md:my-0 h-full md:ml-2 md:ml-0 bg-white rounded-xl pie-chart-container div3">
                <div className="w-1/2 xl:w-3/4 m-0 m-auto mt-2 mb-4 flex flex-wrap justify-center">
                    <div className="w-full">
                        <div className="relative">

                            <select
                                onChange={(event: any) => Props.setProfitDataFilter({type: event.target.value})}
                                className="block rounded-xl appearance-none w-full bg-gray-200 border-2 border-gray-300 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="time"
                                name="time">
                                {
                                    ProfitFilterItems.map(item =>
                                        <option selected={Props.profitDataFilter.type === item.slug} key={item.slug}
                                                value={item.slug}>{item.title}</option>
                                    )
                                }
                            </select>
                            <div className="pointer-events-none
                                    absolute inset-y-0 right-0 flex items-center px-2 text-black">
                                <svg className="fill-current h-6 w-6"
                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path
                                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>

                <div
                    className="w-full  text-center text-sm lg:text-xs mb-4 mt-2 flex items-center justify-center">
                    <span><img src="/assets/img/svg/icons/calendarGray.svg" className="w-4 mr-2" alt=""/></span>

                    {Props.data && !Props.loading
                        ?
                        <>
                            From
                            &nbsp;
                            {moment(Props.data.startAt).tz("Europe/Berlin").format('LL')}
                        </>
                        : "From - "
                    }
                    ,&nbsp;
                    {Props.data && !Props.loading
                        ?
                        <>
                            To
                            &nbsp;
                            {moment(Props.data.endAt).tz("Europe/Berlin").format('LL')}
                        </>
                        : "To - "
                    }

                </div>
                <div
                    className="w-full font-bold text-center text-sm mb-4">
                    {t('home.profit_distribution_chart')}
                </div>
                {
                    Props.error
                    && <div
                        className="text-danger text-center h-full flex items-center justify-center">{Props.error}</div>

                }

                {
                    !Props.error &&
                    Props.loading
                        ? <div
                            className=" h-full flex items-start justify-center"><Loading/>
                        </div>
                        : <>
                            {
                                Props.data.result.length === 0
                                    ?
                                    <div
                                        className="text-danger text-center h-full flex items-start justify-center">
                                        {
                                            localStorage.getItem('i18nextLng')?.includes('en')
                                                ?
                                                <img src="/assets/img/svg/pie-chart-nodata-en.svg" alt=""/>
                                                :
                                                <img src="/assets/img/svg/pie-chart-nodata-de.svg" alt=""/>
                                        }
                                    </div>
                                    : <>
                                        <div
                                            className="pie-chart-container--tab mx-auto py-1 px-2 rounded-lg bg-link-water flex justify-between items-center font-bold text-center text-sm mb-0 xl:mb-2">
                                                        <span
                                                            className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                                            ${resourceDetailWithDate === "percent" ? "bg-white text-dodglerBlue" : "text-blueBell"}`}
                                                            onClick={() => setResourceDetailWithDate("percent")}>%</span>
                                            <span
                                                className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                                            ${resourceDetailWithDate === "currency" ? "bg-white text-dodglerBlue" : "text-blueBell"}`}
                                                onClick={() => setResourceDetailWithDate("currency")}>€</span>
                                        </div>

                                        <DonutChart
                                            kind={resourceDetailWithDate}
                                            data={Props.data.result.map((item: any) => parseFloat(parseFloat(item.percent).toFixed(2)))}
                                            amount={Props.data.result.map((item: any) => parseFloat(parseFloat(item.amount).toFixed(2)))}
                                            colors={Props.data.result.map((item: any) => "#" + item.color)}
                                            labels={Props.data.result.map((item: any) => item.title)}
                                        />
                                    </>
                            }

                        </>
                }
            </div>
        </div>
    );
}

export default Index;
