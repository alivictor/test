import React from 'react';
import {Link} from "react-router-dom";
import URLs from "../../../../../constants/URLs";
import {useTranslation} from "react-i18next";
import {Swiper, SwiperSlide} from 'swiper/react';

interface  IProps{
  setProfitDataFilter: any,
  profitDataFilter: any,
  articles: any
}
const Filter = ({setProfitDataFilter, profitDataFilter, articles}:IProps) => {
  const {
    t,
  } = useTranslation();

  const ProfitFilterItems = [
    {
      title: t('home.daily_profit'),
      slug: "Daily",
    },
    {
      title: t('home.weekly_profit'),
      slug: "Weekly",
    },
    {
      title: t('home.monthly_profit'),
      slug: "Monthly",
    },
    {
      title: t('home.yearly_profit'),
      slug: "Yearly",
    },
  ]
  return (
    <Swiper
      className="h-full w-full px-2 home--profit-filter-items "
      breakpoints={{
        320: {
          slidesPerView: 1.75,
          spaceBetween: 5
        },
        360: {
          slidesPerView: 1.9,
          spaceBetween: 0
        },
        400: {
          slidesPerView: 2.5,
        },
        500: {
          slidesPerView: 3,
        },
        769: {
          slidesPerView: 5,
        },
        855: {
          slidesPerView: 6,
        },
        900: {
          slidesPerView: 5.5,
        },
        1000: {
          slidesPerView: 4,
        },
        1099: {
          slidesPerView: 4.5,
        },
        1200: {
          slidesPerView: 5.2,
        },
        1300: {
          slidesPerView: 5.25,
        },
        1400: {
          slidesPerView: 6,
        },
        1700: {
          slidesPerView: 8,
        },

      }}
    >
      {articles.map((item: any, i: number) => i === 0 && <SwiperSlide key={i} className="hidden"/>)}

      {
        ProfitFilterItems.map((item, index) =>
          <SwiperSlide key={index}>
            <div onClick={() => setProfitDataFilter({type: item.slug})}
                 className={`w-full h-full text-sm text-text-minsk 
                        font-bold inline-flex items-center justify-center cursor-pointer
                        ${profitDataFilter.type === item.slug ? " active " : ""}
                        `}>
              {item.title}
            </div>
          </SwiperSlide>
        )
      }
      <SwiperSlide key={11}>
        <Link to={URLs.dashboard_live_arbitrage}
              className="live-arbitrage h-full text-sm rounded-xl text-white font-bold bg-darkblue inline-flex items-center justify-center">
          {t('sidebar.live_arbitrage')}
        </Link>
      </SwiperSlide>
    </Swiper>
  );
}

export default Filter;
