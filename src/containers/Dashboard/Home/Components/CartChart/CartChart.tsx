import React, {useEffect} from 'react';
import Chart from "react-apexcharts";

interface IProps {
  title: string,
  value: string,
  chart: any[]
}
const CartChart = (Props: IProps) => {

  // useEffect(()=>{
  //
  // },[])
  Props.chart.length > 30 && Props.chart.splice(0, Props.chart.length-30)
  const options = {
    series: [{
      data: Props.chart
    }],
    chart: {
      type: 'line',
      width: 100,
      height: 35,
      paddingTop: 5,
      sparkline: {
        enabled: true
      },
    },
    labels:['Total Profit'],
    tooltip: {
      fixed: {
        enabled: false
      },
      x: {
        show: false
        // title: {
        //   formatter: function () {
        //     return 'Total Profit'
        //   }
        // }
      },
      y: {
        title: {
          formatter: function () {
            return 'Amount(€):'
          }
        }
      },
      marker: {
        show: false
      }
    },
    colors:  ['#37D159'],
    stroke: {
      show: true,
      curve: 'smooth',
      lineCap: 'butt',
      width: 2,
      dashArray: 0,
    },
  };
  return (
    <div className="w-full h-full rounded-xl bg-white px-6 md:px-3 xl:px-6">
      <div className="h-12 flex items-start pt-4">
        <p className="text-minsk font-bold text-lg md:text-md xl:text-lg">{Props.title}</p>
      </div>

      <div className="flex items-center justify-between flex-nowrap mt-4">
        <div>
          <p className="text-shamrock font-bold mt-1 text-xl md:text-xl xl:text-2xl rtl">{Props.value}</p>
        </div>
        <div>
          <Chart
            options={options}
            series={options.series}
            type="line"
            width="100"
            height="35"

          />
        </div>
      </div>
    </div>
  );
}

export default CartChart;
