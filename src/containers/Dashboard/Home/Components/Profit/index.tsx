import React, {useState} from 'react';
import moment from "moment-timezone";
import {currencyFormatter} from "../../../../../assets/helpers/utilities";
import Loading from "../../../../../components/UiKits/Loading/Loading";
import BarChart from "../../../../../components/Charts/Bar";
import DonutChart from "../../../../../components/Charts/Donut";
import ProfitChart from "../../../../../components/Charts/Line/ProfitChart";
import ProfitBarChart from "../../../../../components/Charts/Line/ProfitBarChart";
import {useTranslation} from "react-i18next";

interface IProps {
    getResourceDetailWithDateData: any,
    getResourceDetailWithDateLoading: boolean,
    profitDataFilter: any,
    getResourceDetailWithDateError: any,
    getProfitError: any,
    getProfitLoading: any,
    getProfitData: any,
    resourceDetailWithDate: any,
}


const {
    t,
} = useTranslation();

const Index = (Props: IProps) => {
    const [resourceDetailWithDate, setResourceDetailWithDate] = useState<string>("percent");

    return (
        <div className="w-full bg-white mt-4 rounded-xl home--profit-second">
            <div className="grid grid-cols-12 px-2 md:px-4 pt-4 home--profit-second-bar-chart">
                <div className="col-span-2 h-full grid grid-rows-3 pt-4 pb-2 div1">
                    <div className="w-full row-span-1 home--profit-filter-date">
                        <div
                            className="items bg-white rounded-xl h-12 mt-2 flex items-center justify-start gap-1 px-3 border-2 border-gray-300">
                            <div><img src="/assets/img/svg/icons/calendar-light-outline.svg" alt=""/></div>
                            <div className="text-sm ">
                                {Props.getResourceDetailWithDateData && !Props.getResourceDetailWithDateLoading
                                    ?
                                    <>
                                        From
                                        &nbsp;
                                        {moment(Props.getResourceDetailWithDateData.startAt).tz("Europe/Berlin").format('LL')}
                                    </>
                                    : "From -"
                                }
                            </div>
                        </div>
                        <div
                            className="items bg-white rounded-xl h-12 mt-2 flex items-center justify-start gap-1 px-3 border-2 border-gray-300">
                            <div><img src="/assets/img/svg/icons/calendar-light-outline.svg" alt=""/></div>
                            <div className="text-sm ">
                                {Props.getResourceDetailWithDateData && !Props.getResourceDetailWithDateLoading
                                    ?
                                    <>
                                        To
                                        &nbsp;
                                        {moment(Props.getResourceDetailWithDateData.endAt).tz("Europe/Berlin").format('LL')}
                                    </>
                                    : "To -"
                                }
                            </div>
                        </div>
                    </div>

                    <div className="max-w-full row-span-2" style={{flex: "0 0 100%"}}>
                        <div
                            className="w-1/2 md:w-full expected mx-2 md:mx-auto inline-flex items-start justify-between md:justify-start mt-0 md:mt-8 pt-2 ">
                            <div className="w-8"><img src="/assets/img/svg/icons/expected-daily-profit.svg"
                                                      alt=""/></div>
                            <div>
                                <p className="">
                                    <p className="font-bold text-minsk text-lg lg:text-lg">
                                        {t('home.expected')}&nbsp;
                                    </p>
                                    <p className="text-minsk text-sm lg:text-md">
                                        {Props.profitDataFilter.type === "Daily" && t('home.daily_profit')}
                                        {Props.profitDataFilter.type === "Weekly" && t('home.weekly_profit')}
                                        {Props.profitDataFilter.type === "Monthly" && t('home.monthly_profit')}
                                        {Props.profitDataFilter.type === "Yearly" && t('home.yearly_profit')}
                                    </p>
                                </p>

                                <p
                                    className="mt-2 text-shamrock text-sm lg:text-md xl:text-lg rtl text-center rounded-xl border border-shamrock badge-shamrock">
                                    {
                                        Props.getResourceDetailWithDateData && !Props.getResourceDetailWithDateLoading
                                            ?
                                            currencyFormatter(Props.getResourceDetailWithDateData.expectedProfit)
                                            : "-"
                                    }
                                </p>
                            </div>
                        </div>

                        <div
                            className="w-1/2 md:w-full current  mx-2 md:mx-auto inline-flex items-start justify-start mt-4 ">
                            <div className="w-8"><img src="/assets/img/svg/icons/current-daily-profit.svg"
                                                      alt=""/></div>
                            <div>
                                <p className="">
                                    <p className="font-bold text-minsk text-lg">
                                        {t('home.current')}&nbsp;
                                    </p>
                                    <p className="text-minsk text-sm ">
                                        {Props.profitDataFilter.type === "Daily" && t('home.daily_profit')}
                                        {Props.profitDataFilter.type === "Weekly" && t('home.weekly_profit')}
                                        {Props.profitDataFilter.type === "Monthly" && t('home.monthly_profit')}
                                        {Props.profitDataFilter.type === "Yearly" && t('home.yearly_profit')}
                                    </p>
                                </p>

                                <p
                                    className="mt-2 text-shamrock text-sm lg:text-md xl:text-lg rtl text-center rounded-xl border border-shamrock badge-shamrock">
                                    {
                                        Props.getResourceDetailWithDateData && !Props.getResourceDetailWithDateLoading
                                            ?
                                            currencyFormatter(Props.getResourceDetailWithDateData.currentProfit)
                                            : "-"
                                    }
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="col-span-7 h-full mx-0 md:mx-4 div2">

                    {
                        Props.getResourceDetailWithDateError
                        && <div
                          className="text-danger text-center h-full flex items-center justify-center">{Props.getResourceDetailWithDateError}</div>
                    }
                    {!Props.getResourceDetailWithDateError &&
                    Props.getResourceDetailWithDateLoading
                        ? <Loading/>
                        : <>

                            Props.getResourceDetailWithDateData.result.length === 0
                            ?
                            <div
                                className="text-danger text-center h-full flex items-center justify-center">
                                {
                                    localStorage.getItem('i18nextLng')?.includes('en')
                                        ?
                                        <img src="/assets/img/svg/bar-chart-nodata-en.svg" alt=""/>
                                        :
                                        <img src="/assets/img/svg/bar-chart-nodata-de.svg" alt=""/>
                                }
                            </div>
                            :   <BarChart
                                data={Props.getResourceDetailWithDateData.result.map((item: any) => parseFloat(item.amount) > 1 ? parseFloat(item.amount).toFixed(2) : parseFloat(item.amount).toFixed(4))}
                                colors={Props.getResourceDetailWithDateData.result.map((item: any) => "#" + item.color)}
                                labels={Props.getResourceDetailWithDateData.result.map((item: any) => item.title)}
                                />
                            : ""

                        </>
                    }


                </div>

                <div className="col-span-3 h-full bg-link-water rounded-xl pie-chart-container div3">

                    <div
                        className="w-full font-bold text-center text-sm mb-4">
                        {t('home.profit_distribution_chart')}
                    </div>
                    {
                        Props.getResourceDetailWithDateError
                        && <div
                          className="text-danger text-center h-full flex items-center justify-center">{Props.getResourceDetailWithDateError}</div>

                    }

                    {
                        !Props.getResourceDetailWithDateError &&
                        Props.getResourceDetailWithDateLoading
                            ? <Loading/>
                            : <>

                                        Props.getResourceDetailWithDateData.result.length === 0
                                            ?
                                            <div
                                                className="text-danger text-center h-full flex items-center justify-center">
                                                {
                                                    localStorage.getItem('i18nextLng')?.includes('en')
                                                        ?
                                                        <img src="/assets/img/svg/pie-chart-nodata-en.svg" alt=""/>
                                                        :
                                                        <img src="/assets/img/svg/pie-chart-nodata-de.svg" alt=""/>
                                                }
                                            </div>
                                            : <>
                                                <div
                                                    className="pie-chart-container--tab mx-auto py-1 px-2 rounded-lg border-1 border-darkBlue bg-link-water flex justify-between items-center font-bold text-center text-sm mb-0 xl:mb-2">
                                                        <span
                                                            className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                                            ${resourceDetailWithDate === "percent" ? "text-white bg-darkblue" : "text-darkBlue"}`}
                                                            onClick={() => setResourceDetailWithDate("percent")}>%</span>
                                                    <span
                                                        className={` w-6/12 rounded-lg h-full inline-flex items-center justify-center text-center cursor-pointer
                                                            ${resourceDetailWithDate === "currency" ? "text-white bg-darkblue" : "text-darkBlue"}`}
                                                        onClick={() => setResourceDetailWithDate("currency")}>€</span>
                                                </div>

                                                <DonutChart
                                                    kind={resourceDetailWithDate}
                                                    data={Props.getResourceDetailWithDateData.result.map((item: any) => parseFloat(parseFloat(item.percent).toFixed(2)))}
                                                    amount={Props.getResourceDetailWithDateData.result.map((item: any) => parseFloat(parseFloat(item.amount).toFixed(2)))}
                                                    colors={Props.getResourceDetailWithDateData.result.map((item: any) => "#" + item.color)}
                                                    labels={Props.getResourceDetailWithDateData.result.map((item: any) => item.title)}
                                                />
                                            </>
                                        : ""
                            </>
                    }


                </div>
            </div>

            {Props.getProfitError
                ? <div className="text-center text-danger w-full">{Props.getProfitError}</div>
                : Props.getProfitLoading && !Props.getProfitData
                    ? <Loading/>
                    : Props.getProfitData &&
                    Props.getProfitData.length !== 0
                        ?
                        <>
                            <ProfitChart
                                data={Props.getProfitData}
                                paddingRight={Props.getProfitData}
                                profitKind={Props.profitDataFilter.type}
                                bullet={true}
                            />


                            <ProfitBarChart
                                data={Props.getProfitData}
                                paddingRight={Props.getProfitData}
                            />

                        </>
                        : <div className="mb-4 mt-4 live-arbitrage--graph">
                            <div className="live-arbitrage--table rounded-lg bg-white">
                                <div className="header px-6 rounded-tl-xl">
                                            <span className="text-darkBlue font-bold">
                                              {Props.profitDataFilter.type === "Daily" && t('home.daily_profit')}
                                                {Props.profitDataFilter.type === "Weekly" && t('home.weekly_profit')}
                                                {Props.profitDataFilter.type === "Monthly" && t('home.monthly_profit')}
                                                {Props.profitDataFilter.type === "Yearly" && t('home.yearly_profit')}
                                                {t('home.chart')}
                                            </span>
                                </div>
                                <div className="flex justify-around items-center w-full h-64 my-20">
                                    {
                                        localStorage.getItem('i18nextLng')?.includes('en')
                                            ? <img src="/assets/img/svg/line-chart-nodata-en.svg" alt=""/>
                                            : <img src="/assets/img/svg/line-chart-nodata-de.svg" alt=""/>
                                    }
                                </div>
                            </div>
                        </div>
            }

        </div>
    );
}

export default Index;
