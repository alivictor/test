import React from 'react';
import './index.scss';
import URLs from "../../../../../constants/URLs";
import {Link} from "react-router-dom";

interface IProps {
	classes?: string;
	styles?: any;
	title: string;
	value: string;
	badge: string,
	badgeTitle: string,
	icon: string,
	link: string
	linkTitle: string

}

export default ({ classes, title, value, badge, badgeTitle,styles, icon, link, linkTitle }: IProps) => {
	return (
		<div className={`card rounded-xl p-4 ${classes} `} style={styles ? styles : {}}>
			<div className="flex justify-start items-center">
				<img src={icon} className="w-10" />
				<div className="text ml-4 ">
					<div className="opacity-50">{title}</div>
					<div className="text-xl font-semibold rtl text-left">{value}</div>
				</div>
			</div>
			<div className="flex justify-between items-center mt-3">
				<div className="flex justify-start items-center p-1 rounded-lg deposit-total">
					<img src="/assets/img/svg/icons/white_chart_icon.svg" className="w-0.5 icon" alt=""/>
					<span className="ml-2 text-thin text-sm text">{badgeTitle}: <span className="rtl">{badge}</span></span>
				</div>
				<Link to={link} className="text flex justify-start items-center ml-3 cursor-pointer">
					{linkTitle}
					<img src="/assets/img/svg/icons/angle_right_blue_200.svg" className="w-0.5 ml-2 icon"  alt=""/>
				</Link>
			</div>
		</div>
	);
};
