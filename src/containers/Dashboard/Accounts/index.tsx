import React, {Suspense, useEffect} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import './accounts.scss';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {FetchLevels} from "../../../actions/levels/LevelsActions";
import UserModel from "../../../models/user.model";
import URLs from "../../../constants/URLs";
import {checkAvatar, handleAvatarError, showLevelStatus} from "../../../assets/helpers/utilities";
import {levelInterface, LevelItemInterface} from "../../../models/levels.model";
import {useTranslation} from "react-i18next";
import Button from "../../../components/UiKits/Button";

const LevelCart = React.lazy(() => import('../../../components/LevelCart/LevelCart'));

interface IProps {
    levels: levelInterface[],
    user: UserModel,
    fetchLevels: any,
    level: LevelItemInterface,
    levelLength: number,
    levelsLoading: boolean,
    levelsError: string,
}

const Index = (Props: IProps) => {
    useEffect(() => {
        Props.levelLength === 0 && Props.fetchLevels()
    }, []);
    const {
        t,
    } = useTranslation();
    return (
        <div className="accounts mt-8 px-4 md:px-8">

            <div className='w-full flex items-center profile lg:flex-row md:flex-col sm:flex-col flex-col'>
                <div className='flex flex-1 items-center h-full p-2'>
                    <img src={checkAvatar(Props.user && Props.user.photo)}
                         onError={handleAvatarError}
                         className='rounded-full'
                         alt="" />
                    <div className='flex flex-col flex-1 name'>
                        <p className='flex-1'>{Props.user && Props.user.fullName}</p>
                        <span className='flex-1'>
                            {
                                Props.level
                                    ? !showLevelStatus(Props.level.group?.title, Props.level.title)
                                    ? <Link to={URLs.dashboard_invest}
                                            className="text-white">{t('sidebar.invest')}</Link>
                                    : showLevelStatus(Props.level.group?.title, Props.level.title)
                                    : <Link to={URLs.dashboard_invest}
                                            className="text-white">{t('sidebar.invest')}</Link>
                            }
                        </span>
                    </div>
                </div>
                <div className='flex items-center h-full p-2'>
                    <Button name='Invest to Level up' to='/' model='accent' />
                </div>
            </div>

            <Suspense fallback={<Loading/>}>
                {
                    Props.levelsError !== '' && <div className="col-span-8 md:col-span-7 grid grid-cols-4 w-100">
                        <div className=" col-span-4 flex items-center justify-center">
                            {
                                Props.levelsError.includes('Server')
                                    ? <img src={"/assets/img/png/500-min.png"} className="mx-auto" alt=""/>
                                    : <p className="text-danger text-center">{Props.levelsError}</p>
                            }
                        </div>
                    </div>
                }
                {
                    Props.levelsLoading && Props.levelsError === ''
                        ? <div className="col-span-8 md:col-span-7 grid grid-cols-4 w-100">
                            <div className="mx-auto col-span-4"><Loading/></div>
                        </div>
                        :
                        <div className="accounts-levels w-full relative mt-10">
                            <div className="grid grid-cols-8 gap-3">
                                <div className="col-span-1 accounts-levels--titles">
                                    <ul className="w-full">
                                        <li/>
                                        <li className="hidden"><p>{t('accounts.investment_amount')}</p></li>
                                        <li><p>{t('invest.market')}</p></li>
                                        <li className="hidden"><p>{t('accounts.estimated_monthly_returns_in_percent')}</p></li>
                                        <li><p>{t('accounts.estimated_annual_returns_in_percent')}</p></li>
                                        <li><p>{t('accounts.euro_bank_transfer_BTC_ETH_SMT_withdrawals')}</p></li>
                                        <li><p>{t('accounts.support')}</p></li>
                                        <li><p>{t('accounts.access_to_our_institutional_accounts')}</p></li>
                                        <li><p>{t('accounts.access_to_the_otc_precious_metals_market')}</p></li>
                                        <li><p>{t('accounts.access_to_the_otc_oil_crypto_market')}</p></li>
                                        <li><p>{t('accounts.access_to_the_otc_forex_market')}</p></li>
                                        <li><p>{t('accounts.additional_otc_markets_with_higher_margins')}</p></li>
                                        <li><p>{t('accounts.additional_SMT_token_bonus_per_trade')}</p></li>
                                        <li><p>{t('accounts.concierge_service_for_manual_higher_margin_trades')}</p></li>
                                    </ul>
                                </div>

                                <LevelCart levels={Props.levels}/>
                            </div>
                        </div>
                }
            </Suspense>

        </div>
    );
};


const mapStateToProps = (state: any) => {
    return {
        levels: state.levels.items,
        levelLength: state.levels.items ? state.levels.items.length : 0,
        user: state.auth.user,
        level: state.auth.level,
        levelsLoading: state.levels.loading,
        levelsError: state.levels.error,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchLevels: () => dispatch(FetchLevels())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
