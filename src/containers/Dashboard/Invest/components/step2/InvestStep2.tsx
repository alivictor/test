import React from 'react';
import {IInvestRequests} from "../../../../../actions/invest";
import {connect} from "react-redux";
import {formatter} from "../../../../../assets/helpers/utilities";
import {useTranslation} from "react-i18next";

interface IProps {
  onSetStepNumber: any,
  activeInvest: IInvestRequests
}

const InvestStep2 = (Props: IProps) => {

  const handleSubmit = (e: any) => {
    e.preventDefault();
  };
  const {
    t,
  } = useTranslation();

  return (
    <>

      <form
          onSubmit={handleSubmit}
          className="mt-10 w-full sm:w-5/6 md:w-full xl:w-11/12 px-0 sm:px-6 md:px-12 mx-auto step-2-container-form">
        <div
            style={{width:"max-content"}}
            className=" text-left bg-green-100 border border-green-400 text-shamrock font-bold mb-2 pl-4 pr-8 py-3 rounded relative rounded-full"
            role="alert">
                        <span className="flex justify-start items-center pr-8">
                            <img src="/assets/img/svg/icons/carbon_email.svg" className="mr-2 w-6" />
                            Please check your email
                        </span>
          <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
                        <div>
                            <svg className="fill-current h-6 w-6 text-shamrock mr-1"
                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path
                                d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg>
                        </div>
                    </span>
        </div>
        <div className="flex flex-wrap justify-center">
          {/*<div className="withdrawal w-full lg:w-1/2 gap-2 my-4">*/}
          {/*  <label className="text-blue-900" htmlFor="account_owner">IBAN</label>*/}
          {/*  <div className="mr-2 mt-2">*/}
          {/*    <input type="text" name="account_owner" id="account_owner"*/}
          {/*           value={"MOLIOR"}*/}
          {/*           readOnly={true}*/}
          {/*           className="p-4 rounded-xl border-1 border-gray-300 w-full"*/}
          {/*    />*/}
          {/*  </div>*/}
          {/*</div>*/}
          {/*<div className="withdrawal w-full lg:w-1/2 gap-2 my-4">*/}
          {/*  <label className="text-blue-900" htmlFor="bic">BIC</label>*/}
          {/*  <div className="mr-2 mt-2">*/}
          {/*    <input type="text" name="bic" id="bic"*/}
          {/*           value={"MOLIOR"}*/}
          {/*           readOnly={true}*/}
          {/*           className="p-4 rounded-xl border-1 border-gray-300 w-full"*/}
          {/*    />*/}
          {/*  </div>*/}
          {/*</div>*/}
          <div className="withdrawal w-full lg:w-1/2 gap-2 my-4">
            <label className="text-blue-900" htmlFor="amount">Amount</label>
            <div className="mr-2 mt-2">
              <input type="text" name="amount" id="amount"
                     value={Props.activeInvest && Props.activeInvest.lastActivity && formatter.format(parseFloat(Props.activeInvest.lastActivity.amount))}
                     readOnly={true}
                     className="p-4 rounded-xl border-1 border-gray-300 w-full"
              />
            </div>
          </div>
          <div className="withdrawal w-full lg:w-1/2 gap-2 my-4">
            <label className="text-blue-900" htmlFor="reference">Reference</label>
            <div className="mr-2 mt-2">
              <input type="text" name="reference" id="reference"
                     value={Props.activeInvest && Props.activeInvest.lastActivity && Props.activeInvest.saTrackingCode}
                     readOnly={true}
                     className="p-4 rounded-xl border-1 border-gray-300 w-full"
              />
            </div>
          </div>
        </div>

        <div className="w-full mt-10 md:mt-16">
          <div className="mx-auto">
            <button onClick={() => Props.onSetStepNumber(3)}
                    className="dodgler-button mx-auto block focus:outline-none investBtn">
              {t('invest.next')}
            </button>
          </div>
        </div>
      </form>

    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    activeInvest: state.invest.activeInvest
  }
};

export default connect(mapStateToProps)(InvestStep2);
