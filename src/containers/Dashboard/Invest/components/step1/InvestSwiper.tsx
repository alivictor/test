import React, {Suspense} from 'react';
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {A11y, Pagination, Scrollbar, Navigation} from "swiper";
// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import 'react-input-range/lib/css/index.css';
import {connect} from "react-redux";
import {levelInterface} from "../../../../../models/levels.model";
import Loading from "../../../../../components/UiKits/Loading/Loading";
import {readableNumber} from "../../../../../assets/helpers/utilities";
import {useTranslation} from "react-i18next";

interface IProps {
    levels: levelInterface[],
    levelsLoading: boolean,
    levelsError: string,
}

const InvestSwiper = (Props: IProps) => {
    SwiperCore.use([Pagination, Scrollbar, A11y, Navigation]);

    const {
        t,
    } = useTranslation();
    return (
        <Suspense fallback={<Loading/>}>

            {
                Props.levelsError !== '' && <div className="col-span-8 md:col-span-7 grid grid-cols-4 w-100">
                    <div className=" col-span-4 flex items-center justify-center">
                        {
                            Props.levelsError.includes('Server')
                                ? <img src={"/assets/img/png/500-min.png"} className="mx-auto" alt=""/>
                                : <p className="text-danger text-center">{Props.levelsError}</p>
                        }
                    </div>
                </div>
            }
            {
                Props.levelsLoading && Props.levelsError === ''
                    ? <div className="col-span-8 md:col-span-7 grid grid-cols-4 w-100">
                        <div className="mx-auto col-span-4"><Loading/></div>
                    </div>
                    : <Swiper
                        className="col-span-8 md:col-span-7 grid grid-cols-1 sm:grid-cols-3 w-100"
                        spaceBetween={20}
                        slidesPerView={4}
                        // navigation
                        // pagination
                        breakpoints={{
                            320: {
                                slidesPerView: 1,
                                spaceBetween: 0,
                            },
                            767: {
                                slidesPerView: 2,
                                spaceBetween: 10,
                            },
                            850: {
                                slidesPerView: 3,
                                spaceBetween: 5
                            },
                        }}
                    >
                        {Props.levels.map((item, index) => {
                            if(index !== Props.levels.length - 1 ){
                                return <SwiperSlide className="col-span-1 step-1-container--cart" key={index}>
                                    <ul className="w-full py-6 px-4">
                                        <li>
                                            <div className="flex justify-start items-center pb-6">
                                                <img src="/assets/img/svg/icons/blue_square.svg" alt=""/>
                                                <div className="ml-3">
                                                    <div className="text-blue-900">€ {item.amount}</div>
                                                    <div className="text-blue-700">{item.title}</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p className="font-thin">
                                                {t('invest.market')}
                                            </p>
                                            <p>
                                                {item.levels[item.levels.length - 1].market}
                                            </p>
                                        </li>
                                        <li>
                                            <p className="font-thin">{t('invest.return')}</p>
                                            <p>
                                                {readableNumber(item.levels[0].profitAverage)} %
                                                - {readableNumber(item.levels[item.levels.length - 1].profitAverage)} %
                                            </p>
                                        </li>
                                    </ul>
                                </SwiperSlide>
                            }else {
                                return (<></>)
                            }
                        })}


                    </Swiper>
            }

        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        levelsLoading: state.levels.loading,
        levelsError: state.levels.error,
    }
};


export default connect(mapStateToProps)(InvestSwiper);

