import React, {useEffect, useState} from 'react';
import InputRange from "react-input-range";
import 'react-input-range/lib/css/index.css';
import {connect} from "react-redux";
import UserModel from "../../../../../models/user.model";
import {levelInterface, LevelItemInterface} from "../../../../../models/levels.model";
import InvestSwiper from "./InvestSwiper";
import {IInvestRequests} from "../../../../../actions/invest";
import {checkInvestStatus} from "../../../../../assets/helpers/utilities";
import Loading from "../../../../../components/UiKits/Loading/Loading";
import {useTranslation} from "react-i18next";

interface IProps {
    setShow: any,
    onChangeAmount: any,
    onSetStepNumber: any,
    hasInvest: boolean,
    investLoading: boolean,
    investStatus: string,
    investError: string,
    levels: levelInterface[],
    user: UserModel,
    level: LevelItemInterface,
    activeInvest: IInvestRequests,
}

const InvestStep1 = (Props: IProps) => {
    const [plan, selectPlan] = useState<string | null>(null);
    const [planAnimation, setPlanAnimation] = useState<string | null>(null);
    const minAmount = 2500;
    const maxAmount = 250000;
    const [amount, setAmount] = useState<number>();

    useEffect(() => {
        if (Props.activeInvest && Props.activeInvest.lastActivity) {
            if (parseFloat(Props.activeInvest.lastActivity.amount) !== 0) {
                setAmount(parseFloat(Props.activeInvest.lastActivity.amount));
                handleChangeAmount(parseFloat(Props.activeInvest.lastActivity.amount));
            }
        }


    }, [])

    useEffect(() => {
        if (plan !== "-") {
            planAnimation === "animation2"
                ? setPlanAnimation("animation1")
                : setPlanAnimation("animation2")
        }
    }, [plan])

    const handleChangeAmount = (value: number) => {
        setAmount(value);
        Props.onChangeAmount(value);

        findLevelWithValue(value + Props.user.point)
    };

    function findLevelWithValue(value: number) {
        // eslint-disable-next-line array-callback-return
        Props.levels.map((item, index) => {
            if (parseFloat(item.levels[0].minScore) <= value && parseFloat(item.levels[item.levels.length - 1].maxScore) >= value) {
                // eslint-disable-next-line array-callback-return
                item.levels.map((level, index) => {
                    if (parseFloat(level.minScore) <= value && parseFloat(level.maxScore) >= value) {
                        selectPlan(item.title);
                    } else {
                        if (value < minAmount) {
                            selectPlan("-");
                        }
                    }
                })
            } else {
                if (value < minAmount) {
                    selectPlan("-");
                }
            }


        })
    }

    const sendInvestRequest = (e: any) => {
        e.preventDefault();
        Props.setShow(true);
    };

    const {
        t,
    } = useTranslation();
    return (
        <>
            <div className="step-1-container">
                <div className="grid grid-cols-8 gap-3">
                    <div className="col-span-1 step-1-container--titles">
                        <ul className="w-full">
                            <li/>
                            {/*<li><p>{t('invest.amount')} (€)</p></li>*/}
                            <li><p>{t('invest.market')}</p></li>
                            <li className="profit"><p>~ {t('invest.profit')}</p></li>
                        </ul>
                    </div>

                    <InvestSwiper levels={Props.levels}/>
                </div>

                <form
                    onSubmit={(e) => sendInvestRequest(e)}
                    className="grid grid-cols-3 gap-1 w-full md:w-3/4 mt-5 md:mt-12 px-0 mx-auto">
                    <div className="col-span-3 md:col-span-1 step-1-container--select ">
                        <div className="relative">
                            <label className="block login-container--label
               text-left text-minsk text-sm font-bold mb-2 ml-1" htmlFor="level">
                                {t('invest.plan')}
                            </label>
                        </div>
                        <div className="relative input-box">
                            <div
                                className={`input w-full py-3 px-4 pr-8 rounded leading-tight flex items-center ${planAnimation}`}>
                                {
                                    plan
                                        ? plan
                                        : t('invest.plan')
                                }
                            </div>
                        </div>
                    </div>

                    <div className="col-span-3 md:col-span-1 step-1-container--select">
                        <div className="relative">
                            <label className="block login-container--label
                                text-left text-minsk text-sm font-bold mb-2 ml-1" htmlFor="value">
                                {t('invest.value')}
                            </label>
                        </div>
                        <div className="relative input-box">
                            <input type="number"
                                   id="value"
                                   readOnly={checkInvestStatus(Props.activeInvest && Props.activeInvest.lastActivity ? Props.activeInvest.lastActivity.status : "Disable")}
                                   value={amount}
                                   min={minAmount}
                                   step={500}
                                   max={maxAmount}
                                   placeholder={t('invest.invest_amount')}
                                   onChange={(e) => handleChangeAmount(parseFloat(e.target.value))}
                                   className="block appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none"
                            />
                        </div>

                        <div className="relative">
                            <div className={`w-full mt-4 range-container price `}>
                                <InputRange
                                    maxValue={maxAmount}
                                    minValue={minAmount}
                                    value={amount || minAmount}
                                    step={500}
                                    disabled={checkInvestStatus(Props.activeInvest && Props.activeInvest.lastActivity ? Props.activeInvest.lastActivity.status : "Disable")}
                                    onChange={(value) => handleChangeAmount(typeof value === "number" ? value : 0)}
                                />
                            </div>
                        </div>

                    </div>

                    <div className="col-span-3 md:col-span-1 step-1-container--select">
                        <div className="mx-auto">
                            {
                                checkInvestStatus(Props.activeInvest && Props.activeInvest.lastActivity ? Props.activeInvest.lastActivity.status : "Disable")
                                    ? <div
                                        onClick={() => Props.onSetStepNumber(2)}
                                        className="dodgler-button mx-auto md:mr-auto block focus:outline-none w-full lg:w-4/5 xl:w-3/5 mt-8 cursor-pointer">
                                        {t('invest.payment_information')}
                                    </div>
                                    : (amount && amount + Props.user.point >= 250000) || (plan === 'Concriege')
                                    ? null
                                    : <button
                                        disabled={Props.investLoading}
                                        className="dodgler-button mx-auto md:mr-auto block focus:outline-none w-full md:w-4/5 mt-8 cursor-pointer">
                                        {t('accounts.invest')}
                                        {Props.investLoading && <Loading
                                          stylesProp2={{
                                              width: '35px',
                                              height: 'auto',
                                              position: 'absolute',
                                              top: "20%",
                                              left: '48%'
                                          }}/>}
                                    </button>
                            }

                        </div>
                    </div>

                    <div className="col-span-3 mt-10 md:mt-16">
                        <div className="mx-auto">
                            {
                                ((amount && amount + Props.user.point >= 250000) || (plan === 'Concriege'))
                                && <div className="w-full fade">
                                  <p className="w-full mt-5 text-minsk  text-lg pr-5fade ">
                                      {t('calculator.support_message')}

                                  </p>
                                  <p className="w-full text-minsk mt-10 xl:mt-16 text-lg">
                                    <img src={"/assets/img/svg/icons/Message.svg"} alt=""
                                         className="inline-block mr-2"/>
                                      {t('calculator.contact')} support@pntcap.com
                                  </p>
                                </div>
                            }

                            {
                                Props.investError !== "" &&
                                <p className="w-full text-danger text-center mt-5">
                                    {Props.investError}
                                </p>
                            }
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
};

const mapStateToProps = (state: any) => {
    return {
        levels: state.levels.items,
        levelsLoading: state.levels.loading,
        levelsError: state.levels.error,
        investStatus: state.invest.request.status,
        investLoading: state.invest.request.loading,
        investError: state.invest.request.error,
        user: state.auth.user,
        level: state.auth.level,
        hasInvest: state.auth.hasInvest,
        activeInvest: state.invest.activeInvest

    }
};

export default connect(mapStateToProps)(InvestStep1);
