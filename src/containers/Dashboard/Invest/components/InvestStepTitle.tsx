import React from 'react';
interface IProps {
    title: string,
    description: string,
    icon?: string
}
const InvestStepTitle = (Props: IProps) => {

    return (
        <>
            <div className="w-full px-0 sm:px-3 md:px-0 xl:px-3 my-0 py-2 mx-auto">
                <div className="invest-title">{Props.title}</div>
                <div className="invest-description text-minsk">{Props.description}</div>
            </div>
        </>
    );
};

export default InvestStepTitle;
