import React from 'react';
import {formatter, stringDate} from "../../../../assets/helpers/utilities";
import InvestModel from "../../../../models/invest.model";
import Lightbox from 'react-image-lightbox';
import URLs from "../../../../constants/URLs";
import {useTranslation} from "react-i18next";


interface IProps {
    data: InvestModel[]
}

const InvestRows = (Props: IProps) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(false);
    const [activePhoto, setActivePhoto] = React.useState<string | undefined>("");

    const handleShowLightBox = (src: string | undefined) => {
        setActivePhoto(src);
        setIsOpen(true);
    };
    const {
        t,
    } = useTranslation();
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                            <tr className="h-20" key={index}>
                                <td>{item.id}</td>
                                <td>{stringDate(item.createdAt)}</td>
                                <td className="rtl text-center"><span className="rtl text-center"> {formatter.format(parseFloat(item.lastActivity.amount))}</span></td>
                                <td className="rtl text-center"><span className="rtl text-center"> {formatter.format(parseFloat(item.lastActivity.amountAccepted))}</span></td>
                                <td>
                                    {item.lastActivity.status}
                                </td>
                                <td>{item.trackingCode}</td>
                                <td>
                                    {
                                        item.attachment.path
                                            ? item.attachment.path.includes('pdf')
                                            ? <a download href={URLs.base_URL + "/" + item.attachment.path} target="_blank" rel="noopener noreferrer">{t('confirmSection.see')}</a>
                                            : <div className="cursor-pointer"
                                                   onClick={() => handleShowLightBox(item.attachment.path)}>{t('confirmSection.see')}</div>
                                            : '---'
                                    }
                                </td>
                            </tr>
                    )
                })
            }


            {isOpen && (
                <Lightbox
                    mainSrc={URLs.base_URL + "/" + activePhoto}
                    onCloseRequest={() => setIsOpen(false)}
                />
            )}

        </>
    )
};
export default InvestRows;
