import React from 'react';
import {FileI} from "../../index";
import {IInvestRequests} from "../../../../../actions/invest";
import URLs from "../../../../../constants/URLs";
import {Link} from 'react-router-dom';
import Loading from "../../../../../components/UiKits/Loading/Loading";
import {useTranslation} from "react-i18next";

interface IProps {
    show: boolean,
    onSetStepNumber: any,
    setShow: any,
    document: FileI | null,
    setDocument: any,
    handleChangeDocument: any,
    uploadError: string,
    uploadLoading: boolean,
    uploadStatus: string,
    activeInvest: IInvestRequests
}

const InvestStep3 = (Props: IProps) => {

    const sendInvestSerial = (e: any) => {
        e.preventDefault();
        Props.setShow(true);
    };
    const {
        t,
    } = useTranslation();

    return (
        <>
            {
                Props.activeInvest && Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status === "Pending"
                && <>
                    <div className="w-full sm:w-5/6 md:w-3/4 px-0 sm:px-6 md:px-12 my-1 md:my1 py-4 mx-auto">
                        <div className="w-full sm:w-5/6 mx-auto my-2 md:my-1  text-center">
                            <img src={"/assets/img/svg/icons/pending-icon.svg"} className="mx-auto my-6" alt=""/>
                            <h2 className="font-bold mt-3 mb-5 text-minsk text-2xl">
                                {t('invest.your_invest_is_pending')}
                            </h2>
                            <p className="text-minsk">
                                {t('invest.step_3_description')}
                            </p>
                        </div>
                    </div>

                    <div className="w-full mt-10 md:mt-16">
                        <div className="mx-auto">

                            <Link to={URLs.dashboard_home}
                                  className="dodgler-button mx-auto w-40 block focus:outline-none investBtn">
                                OK
                            </Link>
                        </div>
                    </div>
                </>
            }

            <form className="mt-10 w-full sm:w-5/6 md:w-3/4 mx-auto px-0 sm:px-6 md:px-12" onSubmit={sendInvestSerial}>

                <div className="inline-flex flex-wrap w-full mt-6">
                    {
                        Props.activeInvest && Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status !== "Pending"
                        && <div className="flex-auto w-full mx-auto">
                            <div className="uploadBox mx-auto flex flex-wrap justify-center items-center">
                                <div
                                    className={`flex flex-wrap justify-center items-center w-full h-full ${!Props.document && ''}`}>
                                    {
                                        Props.document
                                            ?
                                            <div
                                                className="uploadedDocument flex flex-wrap justify-center items-center">
                                                {
                                                    Props.document?.format.includes('image')
                                                    &&
                                                    <img src={Props.document?.src} className="img" alt=""/>
                                                }
                                                {
                                                    Props.document?.format.includes('pdf')
                                                    &&
                                                    <label>
                                                        <img src={"/assets/img/svg/icons/file-icon-gray.svg"} className="mx-auto mb-8" alt=""/>


                                                    </label>
                                                }

                                                <div
                                                    className="removeButton flex flex-wrap justify-center items-center"
                                                    onClick={() => Props.setDocument(null)}>
                                                    <img src={"/assets/img/svg/icons/times.svg"} className="img" alt=""/>
                                                </div>
                                            </div>
                                            :
                                            <>
                                                <label htmlFor="uploadFile">
                                                    <img src={"/assets/img/svg/icons/upload-icon.svg"} className="mx-auto mb-8" alt=""/>

                                                </label>
                                                <input type="file" id="uploadFile" name="uploadFile"
                                                       onChange={(e) => Props.handleChangeDocument(e)}
                                                       accept="image/*,.pdf"
                                                       style={{display: "none"}}/>
                                            </>
                                    }


                                </div>
                            </div>
                        </div>
                    }


                    <div className="withdrawal flex-auto w-full md:w-1/2 pt-10 pb-10">
                        <div className="w-full mt-2 flex flex-wrap justify-center items-center ">
                            <div className="m-3">
                                {
                                    Props.activeInvest && Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status === "Pending"
                                        ? null
                                        : <button disabled={Props.uploadLoading}
                                                  className="greenButton mx-auto block focus:outline-none investBtn bg-dodglerBlue">
                                            {t('invest.upload')}
                                            {Props.uploadLoading && <Loading stylesProp2={{width:'35px',height:'auto', position: 'absolute',top:"20%", left:'48%'}}/>}
                                        </button>
                                }
                            </div>
                            <div className="m-3">
                                {
                                    Props.activeInvest && Props.activeInvest.lastActivity && Props.activeInvest.lastActivity.status === "Pending"
                                        ? null
                                        : <Link to={URLs.dashboard_home}
                                                style={{background:"white"}}
                                                  className="withdrawal-button border mx-auto block focus:outline-none investBtn border-dodglerBlue text-dodglerBlue">
                                            {t('invest.upload_later')}
                                            {Props.uploadLoading && <Loading stylesProp2={{width:'45px',height:'auto', position: 'absolute',top:"20%", left:'48%'}}/>}
                                        </Link>
                                }
                            </div>
                        </div>
                        {
                            Props.uploadStatus === 'Success'
                                ?
                                <div role="alert" className="mt-4">
                                    <div
                                        className="border border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                                        <p>{t('invest.step3_thanks')}</p>
                                    </div>
                                </div>
                                : Props.uploadStatus !== '' && <div role="alert" className="mt-4">
                                <div
                                    className="border border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                                    <p>{Props.uploadStatus}</p>
                                </div>
                            </div>
                        }
                        {
                            Props.uploadError !== ''
                            &&
                            <div role="alert" className="mt-4">
                                <div
                                    className="border border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    <p>{Props.uploadError}</p>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </form>

        </>
    );
};


export default InvestStep3;
