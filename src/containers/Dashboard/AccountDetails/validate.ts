import {Values, errorsType} from "./interfaces";
import {strongRegex} from "../../Auth/SignUp/validate";

export const validate = (values : Values) => {
  const errors : errorsType = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if(!values.currentPassword){
    errors.currentPassword = "Required"
  }else if(values.currentPassword.length < 8) {
    errors.currentPassword = "Minimum length is 8"
  }
  if(!values.newPassword){
    errors.newPassword = "Required"
  }else if(values.newPassword.length < 8) {
    errors.newPassword = "Minimum length is 8"
  }
  if(!values.firstname){
    errors.firstname = "Required"
  }
  if(!values.lastname){
    errors.lastname = "Required"
  }
  if(!values.phone){
    errors.phone = "Required"
  }

  if(!values.confirmNewPassword){
    errors.confirmNewPassword = "Required"
  }else if(values.newPassword !== values.confirmNewPassword) {
    errors.confirmNewPassword = "Passwords Don't Match"
  }
  return errors;
};

export const passwordValidate = (values : Values) => {
  const errors : errorsType = {};

  if(!values.currentPassword){
    errors.currentPassword = "Required"
  }else if(values.currentPassword.length < 8) {
    errors.currentPassword = "Minimum length is 8"
  }
  if(!values.newPassword){
    errors.newPassword = "Required"
  }else if(strongRegex(values.newPassword) < 2) {
    errors.newPassword = "Enter stronger password"
  }else if(values.newPassword.length < 8) {
    errors.newPassword = "Minimum length is 8"
  }else if(values.newPassword === values.currentPassword) {
    errors.newPassword = "Passwords Must Not Match"
  }

  if(!values.confirmNewPassword){
    errors.confirmNewPassword = "Required"
  }else if(values.newPassword !== values.confirmNewPassword) {
    errors.confirmNewPassword = "Passwords Don't Match"
  }
  return errors;
};