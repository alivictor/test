import React, {Suspense, useState} from 'react';
import './account-details.scss';
import Loading from "../../../components/UiKits/Loading/Loading";
import {connect} from 'react-redux';
import UserModel from "../../../models/user.model";
import {UpdateProfile, UploadAvatar} from "../../../actions/auth";
import {useTranslation} from "react-i18next";
import PasswordForm from "./components/PasswordForm";
import PhoneNumberForm from "./components/PhoneNumberForm";
import GeneralForm from "./components/GeneralForm";
import WalletAndBankAccountForm from "./components/WalletAndBankAccountForm";

// import googleIcon from '../../../assets/img/svg/icons/google-auth-icon.svg';

interface IProps {
  user: UserModel,
  UpdateProfile: any,
  UploadAvatar: any,
}

const Index = (Props: IProps) => {
  const [smsAuth, setSmsAuth] = useState<boolean>(false);
  // const [googleAuth, setGoogleAuth] = useState(true);


  const {
    t,
  } = useTranslation();


  return (
      <div className="account-details w-full mt-8 px-8">

        <div
          className="wrapper bg-white rounded-xl px-16 pt-16 pb-0 w-full m-0 m-auto my-6">
          {
            Props.user
            &&

            <GeneralForm UploadAvatar={(formData: any, setUploadAvatarStatus: any) => Props.UploadAvatar(formData, setUploadAvatarStatus)}
                         UpdateProfile={(data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) =>
                           Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, resetFormFlag)}
                         user={Props.user}
            />
          }


          {
            Props.user
            &&
            <PhoneNumberForm user={Props.user}
                             UpdateProfile={(data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) =>
                               Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, resetFormFlag)}
            />
          }


          {
            Props.user
            &&
            <WalletAndBankAccountForm user={Props.user}
                             UpdateProfile={(data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) =>
                               Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, resetFormFlag)}
            />
          }


          <PasswordForm
            UpdateProfile={(data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) =>
              Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, resetFormFlag)}
          />
        </div>
      </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    user: state.auth.user
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    UpdateProfile: (data: any, setStatus: any, setSubmitting: any, resetForm: any, resetFormFlag: boolean) => dispatch(UpdateProfile(data, setStatus, setSubmitting, resetForm, resetFormFlag)),
    UploadAvatar: (data: any, setStatus: any) => dispatch(UploadAvatar(data, setStatus))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
