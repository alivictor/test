import React from 'react';
// import walletGray from "../../../../assets/img/svg/icons/wallet-gray-icon.svg";
import {Field, Form, Formik, FormikHelpers} from "formik";
import {countryDefaultState} from "../../../../models/country.model";
import {bankFormValues} from "../interfaces";
import StatusMessage from "../../../../components/UiKits/StatusMessage/StatusMessage";
import Loading from "../../../../components/UiKits/Loading/Loading";
import {useTranslation} from "react-i18next";
import UserModel from "../../../../models/user.model";
import Countries from "../../../../components/Countries/Countries";

interface IProps {
  user: UserModel,
  UpdateProfile: any,
}

const WalletAndBankAccountForm = (Props: IProps) => {

  const {
    t,
  } = useTranslation();

  return (
    <>
      <div className="w-full mb-16">
        <div className="inline-block mr-2"><img src={"/assets/img/svg/icons/wallet-gray-icon.svg"} alt=""/></div>
        <div className="inline-block mx-2 text-minsk font-bold title">
          {t('accounts_details.wallet_and_bank_account')}
        </div>
      </div>
      {/*TODO:: validation for values*/}
      <Formik
        initialValues={{
          bitcoin: Props.user.wallet?.bitcoin || "",
          ethereum: Props.user.wallet?.ethereum || "",
          eurNameOfTheClient: Props.user.eurNameOfTheClient || "",
          eurAddressOfTheClient: Props.user.eurAddressOfTheClient || "",
          eurBankName: Props.user.eurBankName || "",
          eurBankAddress: Props.user.eurBankAddress || "",
          eurIBAN: Props.user.eurIBAN || "",
          eurSwiftOrBIC: Props.user.eurSwiftOrBIC || "",
          eurCountryClient: Props.user.eurCountryClient || countryDefaultState,
          eurCountryBank: Props.user.eurCountryBank || countryDefaultState,
          eurComments: Props.user.eurComments || "",
        }}
        onSubmit={(
          values: bankFormValues,
          {setSubmitting, setStatus, resetForm}: FormikHelpers<bankFormValues>
        ) => {
          let data = {
            wallet: {
              bitcoin: values.bitcoin,
              ethereum: values.ethereum
            },
            eurNameOfTheClient: values.eurNameOfTheClient,
            eurAddressOfTheClient: values.eurAddressOfTheClient,
            eurBankName: values.eurBankName,
            eurIBAN: values.eurIBAN,
            eurSwiftOrBIC: values.eurSwiftOrBIC,
            eurBankAddress: values.eurBankAddress,
            eurCountryClient: values.eurCountryClient.id,
            eurCountryBank: values.eurCountryBank.id,
            eurComments: values.eurComments,
          };

          Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, false);
        }}
      >
        {({values, isSubmitting, status}) => (
          <Form className="edit-profile edit-profile--third-container pb-12">
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">

                <label htmlFor="bitcoin">BTC Wallet</label>
                <Field type="text" name="bitcoin"
                       id="bitcoin"
                       value={values.bitcoin}
                       placeholder={"BTC Wallet"}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="ethereum">ETH Wallet</label>
                <Field type="text" name="ethereum"
                       id="ethereum"
                       value={values.ethereum}
                       placeholder={"ETH Wallet"}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-8 flex flex-wrap gap-16 justify-start">
              <div
                className="title inline-block text-minsk font-normal">
                {t('accounts_details.eur_account')}
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurNameOfTheClient">EUR: {t('accounts_details.name_of_the_client')}</label>
                <Field type="text" name="eurNameOfTheClient"
                       id="eurNameOfTheClient"
                       required={true}
                       value={values.eurNameOfTheClient}
                       placeholder={`EUR: ${t('accounts_details.name_of_the_client')}`}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurAddressOfTheClient">{t('accounts_details.address_of_the_client')}</label>
                <Field type="text" name="eurAddressOfTheClient"
                       id="eurAddressOfTheClient"
                       required={true}
                       value={values.eurAddressOfTheClient}
                       placeholder={`EUR: ${t('accounts_details.address_of_the_client')}`}
                />
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurBankName">{t('accounts_details.your_bank_name')}</label>
                <Field type="text" name="eurBankName"
                       id="eurBankName"
                       required={true}
                       value={values.eurBankName}
                       placeholder={t('accounts_details.your_bank_name')}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurIBAN">IBAN</label>
                <Field type="text" name="eurIBAN"
                       id="eurIBAN"
                       required={true}
                       value={values.eurIBAN}
                       placeholder={"IBAN"}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurSwiftOrBIC">Swift/BIC</label>
                <Field type="text" name="eurSwiftOrBIC"
                       id="eurSwiftOrBIC"
                       required={true}
                       value={values.eurSwiftOrBIC}
                       placeholder={"Swift/BIC"}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurBankAddress">{t('accounts_details.bank_address')}</label>
                <Field type="text" name="eurBankAddress"
                       id="eurBankAddress"
                       required={true}
                       value={values.eurBankAddress}
                       placeholder={t('accounts_details.bank_address')}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurCountryClient">{t('accounts_details.country_of_the_client')}</label>
                <div className="relative">
                  <Field as="select"
                         required={true}
                         className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="eurCountryClient" name="eurCountryClient">
                    <Countries kind="name"
                               default={values.eurCountryClient ? values.eurCountryClient.id : undefined}/>
                  </Field>
                  <div className="pointer-events-none
                                                    absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>

                </div>

              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurCountryBank">{t('accounts_details.country_of_the_bank')}</label>
                <div className="relative">
                  <Field as="select"
                         required={true}
                         className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="eurCountryBank" name="eurCountryBank">
                    <Countries kind="name"
                               default={values.eurCountryBank ? values.eurCountryBank.id : undefined}/>
                  </Field>
                  <div className="pointer-events-none
                                                 absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>

                </div>

              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="eurComments">{t('accounts_details.comments')}</label>
                <Field as="textarea" name="eurComments" id="eurComments"
                       value={values.eurComments}/>
              </div>
            </div>

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-8 justify-end">

              <StatusMessage status={status}/>
              <button type="submit"
                      disabled={isSubmitting}
                      className="dodgler-button w-48 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                Save
                {isSubmitting && <Loading stylesProp2={{
                  width: '35px',
                  height: 'auto',
                  position: 'absolute',
                  top: "20%",
                  left: '48%'
                }}/>}
              </button>
            </div>
          </Form>
        )}
      </Formik>

    </>
  );
}

export default WalletAndBankAccountForm;
