import React from 'react';
import {ErrorMessage, Form, Formik, FormikHelpers} from "formik";
import {errorsType, initialValues, Values} from "../interfaces";
import InputPassword from "../../../../components/UiKits/InputPassword/InputPassword";
import StatusMessage from "../../../../components/UiKits/StatusMessage/StatusMessage";
import Loading from "../../../../components/UiKits/Loading/Loading";
import {strongRegex} from "../../../Auth/SignUp/validate";
import {useTranslation} from "react-i18next";

interface IProps {
  UpdateProfile: any
}

const PasswordForm = (Props: IProps) => {

  const passwordValidate = (values: Values) => {
    const errors: errorsType = {};

    if (!values.currentPassword) {
      errors.currentPassword = t('confirmSection.required');
    } else if (values.currentPassword.length < 8) {
      errors.currentPassword = t('errors.password_min_length_8');
    }
    if (!values.newPassword) {
      errors.newPassword = t('confirmSection.required');
    } else if (strongRegex(values.newPassword) < 2) {
      errors.newPassword = t('errors.weak_password');
    } else if (values.newPassword.length < 8) {
      errors.newPassword = t('errors.password_min_length_8');
    } else if (values.newPassword === values.currentPassword) {
      errors.newPassword = t('errors.passwords_must_not_match')
    }

    if (!values.confirmNewPassword) {
      errors.confirmNewPassword = t('confirmSection.required');
    } else if (values.newPassword !== values.confirmNewPassword) {
      errors.confirmNewPassword = t('errors.passwords_dont_match');
    }
    return errors;
  };

  const {
    t,
  } = useTranslation();
  return (
    <>
      <div className="w-full mb-16">
        <div className="inline-block mr-2"><img src={"/assets/img/svg/icons/password-gray-icon.svg"} alt=""/></div>
        <div className="inline-block mx-2 text-minsk font-bold title">{t('kyc.password')}</div>
      </div>
      <Formik
        initialValues={initialValues}
        validate={passwordValidate}
        onSubmit={(
          values: Values,
          {setSubmitting, setStatus, resetForm}: FormikHelpers<Values>
        ) => {
          let data = {
            current_password: values.currentPassword,
            new_password: values.newPassword
          };
          Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, true);
        }}
      >
        {({isSubmitting, status}) => (
          <Form className="edit-profile edit-profile--third-container pb-12">

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <div className="relative password">
                  <InputPassword label={t('accounts_details.current_password')}
                                 name={"currentPassword"} placeholder={t('accounts_details.current_password')}/>
                </div>
                <span className="text-danger"><ErrorMessage name={"currentPassword"}/></span>
              </div>
            </div>

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <div className="relative password">
                  <InputPassword label={t('accounts_details.new_password')}
                                 name={"newPassword"} placeholder={t('accounts_details.new_password')}/>
                </div>
                <span className="text-danger"><ErrorMessage name={"newPassword"}/></span>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <div className="relative password">
                  <InputPassword label={t('accounts_details.confirm_new_password')}
                                 name={"confirmNewPassword"}
                                 placeholder={t('accounts_details.confirm_new_password')}/>
                </div>
                <span className="text-danger"><ErrorMessage name={"confirmNewPassword"}/></span>
              </div>
            </div>

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-8 justify-end">
              <StatusMessage status={status}/>
              <button type="submit"
                      disabled={isSubmitting}
                      className="dodgler-button w-48 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                {t('accounts_details.save')}
                {isSubmitting && <Loading stylesProp2={{
                  width: '35px',
                  height: 'auto',
                  position: 'absolute',
                  top: "20%",
                  left: '48%'
                }}/>}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
}

export default PasswordForm;
