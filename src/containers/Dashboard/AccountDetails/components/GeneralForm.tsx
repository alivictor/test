import React, {useState} from 'react';
import {Field, Form, Formik, FormikHelpers} from "formik";
import {firstFormValues} from "../interfaces";
import {checkAvatar, handleAvatarError} from "../../../../assets/helpers/utilities";
import Loading, {LoadingStyleSmall} from "../../../../components/UiKits/Loading/Loading";
import StatusMessage from "../../../../components/UiKits/StatusMessage/StatusMessage";
import imageCompression from "browser-image-compression";
import UserModel from "../../../../models/user.model";
import {useTranslation} from "react-i18next";

interface IProps {
  UploadAvatar: any,
  UpdateProfile: any,
  user: UserModel
}

const GeneralForm = (Props: IProps) => {
  const [uploadAvatarStatus, setUploadAvatarStatus] = useState<string>("");

  async function uploadAvatar(event: any) {

    const imageFile = event.target.files[0];

    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 1920,
      useWebWorker: true
    };
    try {
      const compressedFile = await imageCompression(imageFile, options);

      let formData = new FormData();

      //TODO:: check blob
      formData.append("file", imageFile);

      if ((compressedFile.size / 1024 / 1024) > 1) {
        await setUploadAvatarStatus('Maximum Size is 1Mb !')
      } else {
        await Props.UploadAvatar(formData, setUploadAvatarStatus); // write your own logic
      }
    } catch (error) {
      setUploadAvatarStatus(error + ' !')
    }
  }

  const {
    t,
  } = useTranslation();


  return (
    <>
      <div className="w-full mb-4">
        <div className="inline-block mr-2"><img src={"/assets/img/svg/icons/edit-icon.svg"} alt=""/></div>
        <div
          className="inline-block mx-2 text-minsk font-bold title">{t('accounts_details.edit_profile')}</div>
      </div>
      {/*TODO:: validation for values */}
      <Formik
        initialValues={{
          additional_phone_number: Props.user.phone?.additionalNumber || "",
          avatar: "",
          gender: Props.user.gender || ""
        }}
        onSubmit={(
          values: firstFormValues,
          {setSubmitting, setStatus, resetForm}: FormikHelpers<firstFormValues>,
        ) => {
          let data = {
            phone: {
              additionalNumber: values.additional_phone_number,
              gender: values.gender
            }
          };
          Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, false);

        }}
      >
        {({values, status}) => (
          <Form className="edit-profile edit-profile--first-container pb-12">

            <div className="w-full my-6 profile-image">
              <div className="w-40 h-40 rounded-full relative">
                <img src={checkAvatar(Props.user && Props.user.photo)}
                     onError={handleAvatarError}
                     className="w-full rounded-full h-full " alt=""/>
                {
                  uploadAvatarStatus === 'Uploading...'
                  &&
                  <div
                    className="w-full rounded-full h-full bg-white opacity-50 absolute top-0">
                    <Loading stylesProp={LoadingStyleSmall}/>
                  </div>
                }

                <label htmlFor="avatar"
                       className="absolute upload-avatar p-4 rounded-full bg-salmon cursor-pointer">
                  <img src={"/assets/img/svg/icons/edit-white-icon.svg"} className="text-white w-full h-auto" alt=""/>

                </label>
                <Field type="file" id="avatar"
                       onChange={uploadAvatar}
                       name="avatar" style={{display: "none"}}/>


                <div className={`mt-1 text-center 
                                            ${uploadAvatarStatus.includes('!') && 'text-danger'}
                                            ${uploadAvatarStatus.includes('success') && 'text-success'}
                                            `}><small>{uploadAvatarStatus}</small>
                </div>
              </div>
            </div>
            <div className="w-full mt-16 mb-10 pl-10 small-px-6">
              <p className="text-minsk">
                Ref ID <span>{Props.user.promotional?.referralCode}</span>
              </p>
            </div>

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto flex flex-wrap gap-16 justify-start fullname">
              <div className="flex-1">
                <label htmlFor="first_name">{t('accounts_details.firstName')}</label>
                <Field disabled={true} type="text" value={Props.user.firstName}
                       name="first_name" id="first_name" placeholder={t('signUp.enter_your_name')}/>
              </div>
              <div className="flex-1">
                <label htmlFor="last_name">{t('accounts_details.lastName')}</label>
                <Field type="text" name="last_name" value={Props.user.lastName} disabled={true}
                       id="last_name" placeholder={t('signUp.enter_your_last_name')}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="flex-1">
                <label htmlFor="country">{t('accounts_details.choose_your_country')}</label>
                <div className="relative">
                  <Field as="select"
                         required={true}
                         name="country"
                         disabled={true}
                         className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="country">
                    <option defaultValue={""}
                            value={Props.user.country?.id || ""}>{Props.user.country?.name}</option>
                  </Field>
                  <div className="pointer-events-none
                                                    absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>

                </div>
              </div>
              <div className="flex-1">
                <label htmlFor="gender">{t('signUp.gender')}</label>
                <div className="relative">
                  <Field as="select"
                         required={true}
                         name="gender"
                         className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="gender">
                    {
                      !Props.user.gender
                      &&
                      <option value={Props.user.gender || ""}>{Props.user.gender_label || "None"}</option>
                    }
                    <option value="Male" selected={Props.user.gender === "Male"}>{t('signUp.male')}</option>
                    <option value="Female" selected={Props.user.gender === "Female"}>{t('signUp.female')}</option>
                  </Field>
                  <div className="pointer-events-none
                                                    absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>

                </div>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">

                <label
                  htmlFor="additional_phone_number">{t('accounts_details.additional_number')}</label>
                <Field type="tel" name="additional_phone_number"
                       id="additional_phone_number"
                       value={values.additional_phone_number}
                       readOnly={true}
                       placeholder={t('accounts_details.enter_your_additional_phone_number')}/>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-8 justify-end">

              <StatusMessage status={status}/>

              <button type="submit"
                      disabled={true}
                // disabled={isSubmitting}
                      className="dodgler-button w-48 bg-dodglerBlue text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                {t('accounts_details.save')}
              </button>


            </div>
          </Form>
        )}
      </Formik>
    </>
  );
}

export default GeneralForm;
