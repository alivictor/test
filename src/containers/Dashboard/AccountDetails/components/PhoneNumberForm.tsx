import React from 'react';
import {Field, Form, Formik, FormikHelpers} from "formik";
import {secondFormValues} from "../interfaces";
import {readableCountryCode} from "../../../../assets/helpers/utilities";
import StatusMessage from "../../../../components/UiKits/StatusMessage/StatusMessage";
import {useTranslation} from "react-i18next";
import UserModel from "../../../../models/user.model";

interface IProps {
  user: UserModel,
  UpdateProfile: any
}
const PhoneNumberForm = (Props: IProps) => {

  const {
    t,
  } = useTranslation();

  return (
    <>
      <div className="w-full mb-16">
        <div className="inline-block mr-2"><img src={"/assets/img/svg/icons/phone-icon.svg"} alt=""/></div>
        <div
          className="inline-block mx-2 text-minsk font-bold title">{t('accounts_details.phone')}</div>
      </div>
      {/*TODO:: validation for values*/}
      <Formik
        initialValues={{
          country_code: "+" + Props.user.phone?.countryCode || "",
          phone_number: Props.user.phone?.number || "",
        }}
        onSubmit={(
          values: secondFormValues,
          {setSubmitting, setStatus, resetForm}: FormikHelpers<secondFormValues>
        ) => {
          let data = {
            phone: {
              countryCode: readableCountryCode(values.country_code),
              number: values.phone_number.length === 11
                ? values.phone_number.slice(1, 11).trim()
                : values.phone_number?.trim(),
              additional_phone_number: Props.user.phone?.additionalNumber || "",
            }
          };
          Props.UpdateProfile(data, setStatus, setSubmitting, resetForm, false);
        }}
      >
        {({values, status}) => (
          <Form className="edit-profile edit-profile--second-container pb-12">

            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="country_code">{t('accounts_details.code')}</label>
                <div className="relative">
                  <Field as="select"
                         required={true}
                         readOnly={true}
                         name="country_code"
                         className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="country_code">
                    <option value={values.country_code}
                            selected={true}>{values.country_code}</option>
                    {/*<CountryCode/>*/}
                  </Field>
                  <div className="pointer-events-none
                  absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>

                </div>
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-16 justify-start">
              <div className="w-full">
                <label htmlFor="phone_number">{t('accounts_details.number')}</label>
                <Field type="tel" name="phone_number"
                       required={true}
                       id="phone_number"
                       readOnly={true}
                       value={values.phone_number}
                       placeholder={t('signUp.enter_phone_number')}
                />
              </div>
            </div>
            <div
              className="w-11/12 sm:max-w-full px-3 m-0 m-auto mt-4 flex flex-wrap gap-8 justify-end">
              <StatusMessage status={status}/>

              <button type="submit"
                // disabled={isSubmitting}
                      disabled={true}
                      className="dodgler-button w-48 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                {t('accounts_details.save')}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
}

export default PhoneNumberForm;
