import React from 'react';
import UserModel from "../../../../models/user.model";
import {stringDate} from "../../../../assets/helpers/utilities";

interface IProps {
    data: ReferralHistoryItem[]
}

export interface ReferralHistoryItem {
    id: number,
    user: UserModel,
    referral: UserModel,
    point: number,
    subscribedAt: string
}


const ReferralRows = (Props: IProps) => {
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                        <tr className="h-20" key={index}>
                            <td>
                                {item.user ? item.user.firstName : "User Deleted"}
                                <br/>
                                {item.user && item.user.lastName}
                            </td>
                            <td>{stringDate(item.subscribedAt || "---")}</td>
                            <td>{item.point}</td>
                        </tr>
                    )
                })
            }
        </>
    )
};
export default ReferralRows;
