import React from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import {useTranslation} from "react-i18next";

interface IProps {
  isCopy: boolean,
  onCopy: any,
  text: string,
  link: string,
  index: number,
}

const ReferralItem = (Props: IProps) => {

  const {
    t,
  } = useTranslation();
  return (
    <CopyToClipboard text={Props.link}
      onCopy={() => Props.onCopy(Props.index)}>
      <div className="referral-links--item my-12">
        <div className="title text-minsk text-left w-full ">
          {t('referral_program.referral_link')} - <span>
          {Props.text === 'en' && "English"}
          {Props.text === 'de' && "Germany"}
        </span>
        </div>
        <div className=" link py-4 border-b border-b-gray-400 flex flex-no-wrap justify-between items-center">
          <div>
            <p className={`${Props.isCopy ? "selected" : ""}  `}>{Props.link}</p>
          </div>

          <div className="icon pr-2 flex justify-center flex-wrap">
            {Props.isCopy && <span className="text-success mx-2">{t('referral_program.copied')}</span>}
            <img src={"/assets/img/svg/icons/copy-icon.svg"} alt="" className="inline-block m-0 m-auto"/>
          </div>
        </div>
      </div>
    </CopyToClipboard>
  );
};

export default ReferralItem;
