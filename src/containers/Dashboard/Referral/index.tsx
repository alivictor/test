import React, {useState, useEffect} from 'react';
import './referral.scss'
import ReactPaginate from "react-paginate";
import {NextButton, PrevButton} from "../../../components/UiKits/Pagination/Pagination";
import {filterI} from "../WithDrawal";
import {connect} from "react-redux";
import {GET_REFERRAL_HISTORY} from "../../../actions/auth/AuthActionTypes";
import {GetReferralHistory, GetReferralInfo} from "../../../actions/auth";
import ReferralRows, {ReferralHistoryItem} from "./components/ReferralRows";
import UserModel from "../../../models/user.model";
import {referralLinks} from "../../../reducers/auth";
import {thead} from "../../../components/UiKits/ClassicTable/ClassicTable";
import {useTranslation} from "react-i18next";
import ReferralProgramCard from './components/referralProgramCard';

const ReferralItem = React.lazy(() => import("./components/ReferralItem"));

const ClassicTable = React.lazy(() => import("../../../components/UiKits/ClassicTable/ClassicTable"));

interface IProps {
    fetchData: any,
    fetchHistoryData: any,
    clearData: any,
    userData: UserModel,
    tableLoading: boolean,
    tableError: string,
    loading: boolean,
    error: string,
    allData: ReferralHistoryItem[],
    total: number,
    userCount: number,
    totalPoint: number,
    links: referralLinks[],
}

const Index = (Props: IProps) => {
    const [copy, setCopy] = useState<number>(999);
    const [filter, setFilter] = useState<filterI>({take: 5, skip: 0});

    useEffect(() => {
        Props.fetchData(filter);
        Props.total === 0 && Props.fetchHistoryData(filter);

      return function cleanup() {
        Props.clearData();
      };
    }, [filter]);

    const [activePage, setActivePage] = useState<number>(0);
    const onPageChange = (number: number) => {
        setActivePage(number);
        setFilter({...filter, skip: number * filter.take})
    };

    const {
        t,
    } = useTranslation();

    const headers: thead[] = [
        {title: t('referral_program.full_name')},
        {title: t('referral_program.register_date')},
        {title: t('referral_program.point_earned')},
    ];
    return (
        <div className="referral mt-8">
            <ReferralProgramCard reward={Props.totalPoint} referrals={Props.userCount}/>

            {
                Props.links.length !==0
                &&
                <div className="bg-white my-10 sm:p-6 md:p-10 lg:p-16 rounded-xl referral-links">

                    {
                        Props.links.map((item, index: number) => <ReferralItem key={index} link={item.link}
                                                                               text={item.ln} index={index}
                                                                               isCopy={index === copy}
                                                                               onCopy={(data: number) => setCopy(data)}/>)
                    }

                    {
                        Props.error !== "" && <div className="h-32 text-center text-danger">{Props.error}</div>
                    }

                </div>
            }


            <div className="my-20 w-full">
                <div className="my-3 flex flex-wrap justify-between items-center">
                    <div className="pl-1 home-title text-normal text-minsk my-3">{t('referral_program.referral_history')}</div>
                </div>
                <ClassicTable error={Props.tableError} headers={headers}
                              loading={Props.tableLoading} total={Props.total}
                              children={<ReferralRows data={Props.allData}/>}/>

                {
                    Props.total > 5
                    &&
                    <div className="pagination sm:flex-1 sm:flex sm:items-center
                                        sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                      <ReactPaginate
                        previousLabel={PrevButton}
                        nextLabel={NextButton}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={Props.total / filter.take}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={(number) => onPageChange(number.selected)}
                        forcePage={activePage}
                        containerClassName={'pagination'}
                          // subContainerClassName={'pages pagination'}
                        activeClassName={'active'}
                      />
                    </div>
                }
            </div>
        </div>
    );
};
const mapStateToProps = (state: any) => {

    return {
        allData: state.auth.referral.history.items,
        links: state.auth.referral.links,
        userCount: state.auth.referral.userCount,
        totalPoint: state.auth.referral.totalPoint,
        total: state.auth.referral.history.total,
        tableLoading: state.auth.referral.history.loading,
        tableError: state.auth.referral.history.error,
        error: state.auth.referral.error,
        loading: state.auth.referral.loading,
        userData: state.auth.user
    }
};
const mapDispatchToProps = (dispatch: any) => {
    return {
        fetchData: () => dispatch(GetReferralInfo()),
        fetchHistoryData: (data: filterI) => dispatch(GetReferralHistory(data)),
        clearData: () => dispatch({
            type: GET_REFERRAL_HISTORY,
            payload: {
                items: [],
                loading: false,
                total: 0,
                error: '',
            }
        }),

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Index);
