import React from 'react';
import '../FAQ/faq.scss'

const DocItems = React.lazy(() => import('./components/DocItems'));

const Index = () => {

  return (
      <div className="faq mt-16 w-full">
        <div className="pl-2 my-4 home-title text-minsk text-normal">Documents</div>

        <div className="faq-container pt-10 pb-20 w-full bg-white border-xl">
          <div className="faq-container--list">
            <DocItems/>
            <DocItems/>
            <DocItems/>
            <DocItems/>
            <DocItems/>
          </div>
        </div>
      </div>
  );
};

export default Index;
