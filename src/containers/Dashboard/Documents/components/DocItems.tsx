import React from 'react';
import '../../FAQ/faq.scss';
import {Link} from 'react-router-dom';

const DocItems = () => {

  return (
    <div className={`faq-container--list-item`}>
      <Link to={""}>
        <div
          className="faq-header w-full flex flex-no-wrap justify-between h-20 items-center">
          <div className="title flex-auto">
            <img src={"/assets/img/svg/icons/file-icon.svg"} alt="" className="inline-block mx-3"/>
            <p className="pb-0 inline-block mx-3 home-title text-normal">Privacy Policy</p>
          </div>
          <div className="download-icon flex-auto">
            <div className="ml-auto float-right">
              <img src={"/assets/img/svg/icons/download-salmon-icon.svg"} alt=""/>
            </div>
          </div>
        </div>
      </Link>

    </div>
  );
};

export default DocItems;
