import React from 'react';
import {Field, Form, Formik} from "formik";
import StatusMessage from "../../../../components/UiKits/StatusMessage/StatusMessage";
import {useTranslation} from "react-i18next";
import UserModel from "../../../../models/user.model";

interface initialValueInterface {
  mailLanguage: string
}

interface IProps {
  user: UserModel,
  onSubmit: any,
  loading: boolean,
  error: string,
  status: string

}

export default (Props: IProps) => {

  const {
    t,
  } = useTranslation();

  return (
    <div>
      <Formik
        initialValues={{
          mailLanguage: Props.user.setting.emailLanguage || "de"
        }}
        onSubmit={(
          values: initialValueInterface,
        ) => {
          Props.onSubmit(values);
        }}
      >
        {({values, status, resetForm, submitForm}) => (
          <Form className=" pb-12">

            <div className="w-full mt-10 mb-12 m-auto grid grid-cols-2 justify-start">
              <div className="col-span-2">
                <label htmlFor="mailLanguage">{t('settings.email_language')}</label>

                <div className="relative w-64 border border-blue-100">
                  <Field as="select"
                         required={true}
                         name="mailLanguage"
                         className="block appearance-none mt-2 w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                         id="mailLanguage">
                    <option value={"de"} selected={(values.mailLanguage || Props.user.setting.emailLanguage) === 'de'}>Germany</option>
                    <option value={"en"} selected={(values.mailLanguage || Props.user.setting.emailLanguage) === 'en'}>English</option>
                  </Field>
                  <div className="pointer-events-none
                                                    absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                    <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                      <path
                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>
                </div>
                <p className="mt-2 description">{t('settings.select_language')}</p>
              </div>
            </div>


            <div className="w-full  m-0 m-auto mt-8 flex flex-wrap  justify-start">
              <div onClick={() => resetForm()}
                   className="withdrawal-button cursor-pointer w-40 text-minsk py-3 px-4 rounded-xl focus:outline-none "
                   style={{backgroundColor: "#F3F6FE"}}>
                {t('confirmSection.cancel')}
              </div>

              <button type="submit"
                      disabled={Props.loading}
                      className="dodgler-button mx-3 w-40 text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                {t('accounts_details.save')}
              </button>


              <div className="my-2 sm:my-0">
                {
                  Props.status !== ""
                  &&
                  <StatusMessage status={Props.status} message={t('settings.success_message')}/>
                }
                {
                  Props.error !== ""
                  &&
                  <StatusMessage status={Props.error}/>
                }
              </div>

            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}

