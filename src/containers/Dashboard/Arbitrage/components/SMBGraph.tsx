import React from 'react';
import Loading from "../../../../components/UiKits/Loading/Loading";

const CustomSMBChart = React.lazy(() => import("../../../../components/Charts/Line/SMBChart"));

interface IProps {
  smbError: string,
  smbLoading: boolean,
  smb: any[],
}

const SMBGraph = (Props: IProps) => {

  return (
    <div className="my-24 pt-16 live-arbitrage--graph" id="liveSMBGraph">
      <div className="live-arbitrage--table mt-6 rounded-lg bg-white">
        <div className="header py-6 px-6 rounded-tl-xl">
            <span className="text-minsk font-bold">
                 Live SMB Graph
            </span>
        </div>

        <form>
          <div
            className="coin-filters my-3 w-full flex flex-wrap justify-between items-center py-6 px-6">
            <div className="fiats my-3 md:my-0 flex-auto flex justify-start items-center">
              <div className="fiats-item mx-2 ">
                <input
                  type="checkbox" name="currency"
                  id="EUR2" checked={true}/>

                <label htmlFor="EUR2" className=" px-6 py-2">
                  EUR
                </label>
              </div>
            </div>
          </div>
        </form>

        <div className="w-full pr-10">
          {
            Props.smbError !== ""
              ? <p className="text-danger">{Props.smbError}</p>
              : Props.smbLoading
              ? <Loading/>
              : <CustomSMBChart
                paddingRight={Props.smb}
                data={Props.smb}
              />
          }
        </div>
      </div>
    </div>
  );
}

export default SMBGraph;
