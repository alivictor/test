import React, {Suspense} from 'react';
import Loading, {LoadingStyleNormal} from "../../../../components/UiKits/Loading/Loading";
import EuroSymbol from "../../../../components/UiKits/EuroSymbol/EuroSymbol";
import {useTranslation} from "react-i18next";
import {ICurrencies, IExchangers} from "../../../../actions/arbitrage";

interface IProps {
    lastUpdate: string,
    currencies: ICurrencies[],
    exchangers: IExchangers[],
    error: string,
}

const ArbitrageTable = (Props: IProps) => {
    const {
        t,
    } = useTranslation();
    return (
        <Suspense fallback={<Loading/>}>
            <div className="live-arbitrage--table mt-6 rounded-lg">
                <div className="header bg-white py-4 px-6 rounded-tl-lg rounded-tr-lg">
                    <div className="flex flex-wrap justify-start items-center mb-6">
                        <div className="relative border-2 rounded-lg border-blue-100">
                            <select name="price_type" id="price_type"
                                    className="text-black font-medium px-4 py-3 pr-8
                                block appearance-none bg-white rounded
                                leading-tight focus:outline-none">
                                <option value="EUR">EUR</option>
                            </select>
                            <div
                                className="pointer-events-none font-bold absolute inset-y-0 right-0 flex items-center px-2 text-dodglerBlue">
                                <svg className="fill-current h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <span className="text-minsk font-bold">{t('live_arbitrage.live_arbitrage_table')}</span>

                    <div className="mt-2"><small className="text-gray-700">
                        {t('live_arbitrage.last_updated')} {Props.lastUpdate}</small></div>
                </div>
                {
                    Props.exchangers.length > 0
                        ? <div className="w-full new-table">
                            <ul className="exchangers">
                                <li>
                                    <div className="opacity-0 exchangerLogo">
                                        <img src={"/assets/img/svg/icons/btcIcon.svg"} alt="" className="mx-3"/>
                                        <span>Sample</span>
                                    </div>
                                </li>
                                {
                                    Props.exchangers.map((item, index: number) =>
                                        <li className="" key={index}>
                                            <div className="w-full h-full flex justify-start items-center pl-3">
                                                <div className="inline-block mx-1 w-4 h-4 exchangerLogo">
                                                    <img
                                                        src={item.logo !== "" ? item.logo : "/assets/img/svg/icons/exchange-icon.svg"}
                                                        alt={item.title}/>
                                                </div>
                                                <div className="inline-block mx-1">
                                                    <span className="text-black">{item.title}</span>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                }

                                <li className="arbitrage">
                                    <div className="w-full h-full flex justify-start items-center pl-3">
                                        <div className="inline-block mx-1">
                                            <span className="text-black font-bold ">Arbitrage</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <div className="currencies flex flex-no-wrap">
                                {
                                    Props.currencies.map((item, index: number) =>
                                        <ul key={index}>
                                            <li>
                                                <div
                                                    className="flex h-full w-full flex-no-wrap justify-start items-center pl-2">
                                                    <div className="mx-1 inline-flex items-center w-4 h-4 coinLogo">
                                                        <img src={item.logo} alt="Bitcoin"
                                                             style={{width: '100%', height: 'auto'}}/>

                                                    </div>
                                                    <div className="mx-1 inline-block">
                                                        <span className="font-bold ">{item.title}</span>
                                                    </div>
                                                </div>
                                            </li>

                                            {
                                                item.markets.map((market, marketIndex) => {
                                                    // let pct = parseFloat(market.change24PctHour);
                                                    let pct = parseFloat(market.pct);
                                                    return (
                                                        <li className={`${pct > 0 ? 'success' : ""}`}
                                                            key={marketIndex}>
                                                            <div
                                                                className="flex h-full w-full justify-between items-center m-0 m-auto">
                                                                <div
                                                                    className="inline-block flex-auto pl-2 text-left">
                                                                      <span
                                                                          className={`font-bold text-left 
                                                                             ${pct === 1 ? ' text-green-700' : ""}
                                                                             ${pct === -1 ? ' text-red-700' : ""}`}>
                                                                        {
                                                                            market.id === 0
                                                                                ? ""
                                                                                : <EuroSymbol/>
                                                                        }
                                                                          {
                                                                              market.id === 0
                                                                                  ? "-"
                                                                                  : market.price
                                                                          }

                                                                      </span>
                                                                </div>

                                                                <div className={`pointer-events-none flex-auto font-bold inline-block
                                                                          ${pct === 1 ? 'text-green-700 rotate180' : ""}
                                                                          ${pct === -1 ? 'text-red-700' : ""}
                                                                          ${pct === 0 ? 'opacity-0' : ""}
                                                                           inset-y-0 flex items-center`}>
                                                                    {/*{*/}
                                                                    {/*    pct !== 0*/}
                                                                    {/*    &&*/}
                                                                        <svg className="fill-current h-5 w-5"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 20 20">
                                                                          <path
                                                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                                                        </svg>
                                                                    {/*}*/}
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )

                                                })
                                            }

                                            <li className="relative">
                                                <div
                                                    className="flex h-full w-full flex-no-wrap justify-center items-center pl-2">
                                                        <span
                                                            className="text-black font-bold text-center flex justify-center">
                                                           <EuroSymbol/> {item.arbitrage}
                                                        </span>
                                                </div>

                                            </li>
                                        </ul>)
                                }
                            </div>

                        </div>
                        : Props.error !== '' ? <span
                            className="w-full text-center text-2xl text-danger pt-10 m-0 m-auto block">{Props.error}</span> :
                        <Loading stylesProp={LoadingStyleNormal}/>
                }
            </div>
        </Suspense>
    );
}

export default ArbitrageTable;
