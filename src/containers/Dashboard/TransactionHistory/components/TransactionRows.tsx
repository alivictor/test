import React from 'react';
import {currencyFormatter, formatter, stringDate} from "../../../../assets/helpers/utilities";
import {TransactionDto} from "../../../../models/responses/transaction.model";

interface IProps {
    data: TransactionDto[]
}

const TransactionRows = (Props: IProps) => {
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                        <tr className="h-20" key={index}>
                            <td
                                className={`text-center rtl
                                  ${((item.type.includes('Deposit') || item.type.includes('Profit')) && item.amount!=0) && 'text-green-700'}   
                                  ${(item.type  === "Withdrawal" && item.amount!=0) && "text-red-700"}   
                                  ${(item.type  === "WithdrawalBlock" || item.amount==0) && "text-gray-800"}   
                                   font-bold 
                                `}
                            >

                                {currencyFormatter(item.amount)}
                                {((item.type.includes('Deposit') || item.type.includes('Profit')) && item.amount!=0) && ' + '}
                                {(item.type  === "Withdrawal" && item.amount!=0) && " - "}

                            </td>
                            <td>{item.serial === '' ? "System" : item.serial}</td>
                            <td>{item.description}</td>
                            <td>{item.status}</td>
                            <td>{item.type}</td>
                            <td>{stringDate(item.createdAt)}</td>
                        </tr>
                    )
                })
            }
        </>
    )
};
export default TransactionRows;
