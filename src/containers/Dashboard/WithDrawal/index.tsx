import React, {useEffect, useState} from 'react';
import Loading from "../../../components/UiKits/Loading/Loading";
import ClassicTable, {thead} from "../../../components/UiKits/ClassicTable/ClassicTable";
import './withdrawal.scss';

import {connect} from 'react-redux';
import {FetchWithdrawals, sendWithdrawalRequest, withDrawRequest} from "../../../actions/withdrawal";
import WithdrawalRows from "./components/WithdrawalRows";
import {currencyFormatter, stringDateToDMY} from "../../../assets/helpers/utilities";
import ReactPaginate from "react-paginate";
import {NextButton, PrevButton} from "../../../components/UiKits/Pagination/Pagination";
import WithdrawalModel from "../../../models/withdrawal.model";
import ConfirmModal from "../../../components/UiKits/Modal/ConfirmModal";
import {useTranslation} from "react-i18next";
import {getLocalStorage} from "../../../assets/helpers/storage";
import MessageModal from "../../../components/UiKits/Modal/MessageModal";

interface IProps {
  balance: string,
  fetchData: any,
  tableError: string,
  tableLoading: boolean,
  allData: WithdrawalModel[],
  sendWithdrawRequest: any,
  total: number,
  withdrawError: string,
  withdrawStatus: string,
  withdrawLoading: boolean,
}

export interface filterI {
  take: number,
  skip: number,
}

const Index = (Props: IProps) => {
  const [withdraw, setWithdraw] = useState<null | string>(null);
  const [filter, setFilter] = useState<filterI>({take: 5, skip: 0});
  const [show, setShow] = useState<boolean>(false);
  const [showPopupCantWithdraw, setShowPopupCantWithdraw] = useState<boolean>(false);
  useEffect(() => {
    Props.fetchData(filter)
  }, [filter]);

  useEffect(() => {
    if (Props.withdrawStatus.includes('Success') || Props.withdrawError !== '') {
      setShow(false);
    }
  }, [Props.withdrawStatus || Props.withdrawError]);

  const sendWithdrawRequest = (e: any) => {
    e.preventDefault();
    if (getLocalStorage('canWithdrawal') && getLocalStorage('canWithdrawalAt')) {
      setShow(true);
    } else {
      setShowPopupCantWithdraw(true);
    }
  };

  const sendWithdrawRequestConfirm = () => {
    let data: { amount: number } = {
      amount: parseFloat(withdraw || "0")
    };
    Props.sendWithdrawRequest(data)
  };

  const {
    t,
  } = useTranslation();

  const headers: thead[] = [
    {title: t('transaction_history.created_at')},
    {title: t('invest.amount')},
    {title: t('invest.status')},
    {title: t('invest.tracking_code')},
    {title: t('invest.paid_at')},
  ];

  const [activePage, setActivePage] = useState<number>(0);
  const onPageChange = (number: number) => {
    setActivePage(number);
    setFilter({...filter, skip: number * filter.take})
  };

  let canWithdrawalAt = getLocalStorage('canWithdrawalAt') ? stringDateToDMY(getLocalStorage('canWithdrawalAt')) : "";

  return (
      <div className="faq mt-10 md:mt-16 w-full withdrawal">
        {
          show
          &&
          <ConfirmModal callBackConfirm={() => sendWithdrawRequestConfirm()}
                        callBackCancel={() => setShow(false)}
                        callBackClose={() => setShow(false)} title={"Withdrawal"}
                        loading={Props.withdrawLoading}
                        kind={"green"}
                        content={`Are you sure you want to perform this operation?`}
                        icon={""}/>
        }

        {
          showPopupCantWithdraw
          &&
          <MessageModal
              callBackClose={() => setShowPopupCantWithdraw(false)}
              title={t('withdraw.cant_withdraw_message', {date: canWithdrawalAt})}
              kind={"red"}
              content={""}
              icon={""}/>
        }


        <div className="faq-container pt-2 md:pt-10 pb-16 md:pb-20 px-4 md:px-16 w-full bg-white border-xl">
          <div className="px-0 md:px-8 mt-4 mb-2 home-title text-minsk text-normal">{t('sidebar.withdrawal')}</div>
          <div className="w-full text-minsk px-0 md:px-8 py-6">
            <p>
              {t('withdraw.description')}
            </p>
          </div>

          <form className="mt-5 md:mt-10 w-full grid grid-cols-1 md:grid-cols-12 px-0 md:px-6" onSubmit={sendWithdrawRequest}>

            <div className="col-span-4 inline-flex flex-wrap md:flex-no-wrap w-full gap-2 my-2">
              <div>
                <div className="mb-3 flex items-center justify-start">
                  <label htmlFor="balance">{t('withdraw.balance')}</label>
                </div>
                <input type="text" name="balance" id="balance"
                       className="text-left rtl"
                       value={currencyFormatter(parseFloat(Props.balance))}
                       readOnly={true}/>
              </div>
            </div>

            <div className="col-span-4 inline-flex flex-wrap md:flex-no-wrap w-full gap-2 my-2">
              <div>
                <div className="mb-3 flex items-center justify-start">
                  <label htmlFor="withdraw">{t('sidebar.withdrawal')}</label>
                </div>
                <input type="number" name="withdraw" id="withdraw"
                       onChange={(e: any) => setWithdraw(e.target.value)}
                       className="w-full" min={0} max={parseFloat(Props.balance)}
                       value={withdraw || ""}/>
              </div>

            </div>

            <div className="col-span-4 inline-flex flex-wrap md:lex-no-wrap w-full mt-6 md:mt-12">
              <div className="flex-auto w-full">
                <button type="submit"
                        disabled={Props.withdrawLoading}
                        className="dodgler-button relative h-12 md:h-16 w-full text-white font-bold py-3 px-4 rounded-xl focus:outline-none">
                  Withdraw
                  {Props.withdrawLoading && <Loading stylesProp2={{
                    width: '35px',
                    height: 'auto',
                    position: 'absolute',
                    top: "20%",
                    left: '48%'
                  }}/>}

                </button>
                {
                  Props.withdrawStatus === 'Success'
                  &&
                  <div role="alert" className="mt-4">
                    <div
                        className="border border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                      <p>{t('withdraw.success_message')}</p>
                    </div>
                  </div>
                }
                {
                  Props.withdrawError !== ''
                  &&
                  <div role="alert" className="mt-4">
                    <div
                        className="border border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                      <p>{Props.withdrawError}</p>
                    </div>
                  </div>
                }
              </div>
            </div>
          </form>
        </div>

        <div className="transaction-history live-arbitrage">
          <div className="my-12 w-full bg-white rounded-xl">
            <div className="px-8 pt-6 mb-3 flex flex-wrap ml-12  justify-between items-center">
              <div className="pl-1 home-title text-normal text-minsk my-3">{t('withdraw.withdrawal_history')}</div>
            </div>
            <ClassicTable error={Props.tableError} headers={headers}
                          loading={Props.tableLoading} total={Props.total}
                          children={<WithdrawalRows data={Props.allData}/>}/>
            {
              Props.total > 5
              &&
              <div className="pagination sm:flex-1 sm:flex sm:items-center
                                        sm:justify-center bg-white h-20 rounded-bl-lg rounded-br-lg">
                <ReactPaginate
                    previousLabel={PrevButton}
                    nextLabel={NextButton}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={Props.total / filter.take}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={(number) => onPageChange(number.selected)}
                    forcePage={activePage}
                    containerClassName={'pagination'}
                    // subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
              </div>
            }
          </div>
        </div>
      </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    balance: state.auth.user.balance,
    allData: state.withdrawal.items,
    total: state.withdrawal.total,
    tableLoading: state.withdrawal.loading,
    tableError: state.withdrawal.error,
    withdrawError: state.withdrawal.request.error,
    withdrawLoading: state.withdrawal.request.loading,
    withdrawStatus: state.withdrawal.request.status,
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchData: (data: filterI) => dispatch(FetchWithdrawals(data)),
    sendWithdrawRequest: (data: withDrawRequest) => dispatch(sendWithdrawalRequest(data, false))
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(Index);
