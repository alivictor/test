import React from 'react';
import WithdrawalModel from "../../../../models/withdrawal.model";
import {currencyFormatter, formatter, stringDate} from "../../../../assets/helpers/utilities";

interface IProps {
    data: WithdrawalModel[]
}

const WithdrawalRows = (Props: IProps) => {
    return (
        <>
            {
                Props.data.map((item, index) => {
                    return (
                        <tr className="h-20" key={index}>
                            <td>{stringDate(item.createdAt)}</td>
                            <td className="text-center rtl">{currencyFormatter(item.amount)}</td>
                            <td>{item.status}</td>
                            <td>{item.trackingCode ? item.trackingCode : "---"}</td>
                            <td>{item.paidAt ? stringDate(item.paidAt) : "---"}</td>
                        </tr>
                    )
                })
            }
        </>
    )
};

export default WithdrawalRows;
