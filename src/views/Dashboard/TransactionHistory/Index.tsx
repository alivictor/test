import React, {Suspense} from 'react';
import TransactionHistory from '../../../containers/Dashboard/TransactionHistory'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <TransactionHistory/>
        </Suspense>
    );
}

export default Index;
