import React, {Suspense} from 'react';
import Accounts from '../../../containers/Dashboard/Accounts'
import Loading from "../../../components/UiKits/Loading/Loading";
const Index = () => {
  return (
      <Suspense fallback={<Loading/>}>
    <Accounts/>
      </Suspense>
  );
}

export default Index;
