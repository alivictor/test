import React, {Suspense} from 'react';
import KYC from '../../../containers/Dashboard/KYC'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <KYC/>
        </Suspense>
    );
}

export default Index;
