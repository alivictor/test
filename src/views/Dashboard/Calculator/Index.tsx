import React, {Suspense} from 'react';
import Calculator from "../../../components/Calculator/Calculator";
import Loading from "../../../components/UiKits/Loading/Loading";
const Index = () => {
  return (
      <Suspense fallback={<Loading/>}>
    <Calculator/>
      </Suspense>
  );
}

export default Index;
