import React, {Suspense} from 'react';
import Documents from '../../../containers/Dashboard/Documents'
import Loading from "../../../components/UiKits/Loading/Loading";
const Index = () => {
  return (
      <Suspense fallback={<Loading/>}>
    <Documents/>
      </Suspense>
  );
}

export default Index;
