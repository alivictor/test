import React, {Suspense} from 'react';
import Invest from '../../../containers/Dashboard/Invest'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <Invest/>
        </Suspense>
    );
}

export default Index;
