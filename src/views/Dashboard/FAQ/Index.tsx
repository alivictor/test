import React, {Suspense} from 'react';
import FAQ from '../../../containers/Dashboard/FAQ'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <FAQ/>
        </Suspense>
    );
}

export default Index;
