import React, {Suspense} from 'react';
import AccountDetails from '../../../containers/Dashboard/AccountDetails'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <AccountDetails/>
        </Suspense>
    );
}

export default Index;
