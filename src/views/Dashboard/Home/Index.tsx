import React, {Suspense} from 'react';
import Home from '../../../containers/Dashboard/Home'
import Loading from "../../../components/UiKits/Loading/Loading";
const Index = () => {
  return (
      <Suspense fallback={<Loading/>}>
    <Home/>
      </Suspense>
  );
}

export default Index;
