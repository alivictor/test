import React, {Suspense} from 'react';
import LiveArbitrage from '../../../containers/Dashboard/Arbitrage'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <LiveArbitrage/>
        </Suspense>
    );
}

export default Index;
