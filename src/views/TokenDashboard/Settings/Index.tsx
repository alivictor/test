import React, {Suspense} from 'react';
import Settings from '../../../containers/Dashboard/Settings'
import Loading from "../../../components/UiKits/Loading/Loading";

const Index = () => {
    return (
        <Suspense fallback={<Loading/>}>
            <Settings/>
        </Suspense>
    );
}

export default Index;
