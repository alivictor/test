import React, {Suspense, useEffect, useState} from 'react';
import {renderRoutes, RouteConfigComponentProps} from "react-router-config";
import './authWraper.scss';
import ReactPlayer from 'react-player';
// import playIcon from '../../assets/img/svg/icons/play-icon.svg';
import LanguageDropDown from "../../components/DropDowns/LanguageDropDown/LanguageDropDown";
import {useTranslation} from "react-i18next";

import Loading from "../../components/UiKits/Loading/Loading";
import {Redirect} from "react-router-dom";
import URLs from "../../constants/URLs";
import MessageModal from "../../components/UiKits/Modal/MessageModal";

const AuthWrapper = ({route}: RouteConfigComponentProps) => {
  const [show, setShow] = useState(false)
  const [modalMessage, setModalMessage] = useState<any>(null)
  useEffect(() => {
    let message = JSON.parse(localStorage.getItem('modalMessage') || "null");
    if (message) {
      setModalMessage(message)
      setShow(true)
    }
  }, []);

  const onChangeModal = () => {
    setShow(false);
    setModalMessage(null);
    localStorage.removeItem('modalMessage')
  }

  const {
    t
  } = useTranslation();

  if (localStorage.getItem('userAuth')) {

    return (
      <>
        <Redirect to={{
          pathname: URLs.dashboard_home
        }}/>
      </>
    )

  } else {

    return (
      <Suspense fallback={<Loading/>}>
        <div className="flex flex-wrap h-screen">
          {
            show
            &&
            <MessageModal
              callBackClose={() => onChangeModal()}
              title={modalMessage.title}
              kind={"red"}
              content={""}
              icon={""}/>
          }

          <div className="sm-w-full md:w-2/4 bg-default p-4 text-center">

            <div className="flex flex-wrap w-full h-full justify-center relative align-middle login-container">
              <div className="auth-wrapper--header flex justify-end pr-6 absolute right-0 pt-4">
                <LanguageDropDown/>
              </div>
              <div className="auth-wrapper--header flex justify-end pr-6 absolute left-0">
                <div className="w-full xl:ml-auto sm:m-auto">
                  <img src="/assets/img/svg/logo-typography.svg" alt="typography" className="w-48 h-auto"/>
                </div>
              </div>

              <div className="xl:w-4/6 md:w-5/6 m-auto h-auto pt-10">
                <div className="w-full max-w-xl m-auto">
                  {route && renderRoutes(route.routes, null, {})}
                </div>
              </div>
            </div>
          </div>
          <div className="md:w-2/4 xl:w-2/4 lg:w-2/4 sm-w-full bg-default p-0 pb-8
                text-center text-gray-200 auth-wrapper--bg ">
            <div className="flex flex-wrap w-full h-full justify-center align-middle">
              <div className="xl:w-4/6 md:w-5/6 ml-auto mr-auto mt-48 mt-sm-4 h-auto"/>
            </div>
          </div>
        </div>
      </Suspense>
    );
  }
};

export default AuthWrapper;
