import UserModel from "../../../../models/user.model";
import {useTranslation} from "react-i18next";
import URLs from "../../../../constants/URLs";
import {checkAvatar, handleAvatarError} from "../../../../assets/helpers/utilities";
import DropDown from "../../../../components/DropDowns/DropDown";
import React from "react";

export function ProfileDropDown(ownProps: { isOnProfile: any, setIsOnProfile: any, user:UserModel }) {
    const {t} = useTranslation();

    const ProfileLinks = [
        {
            title: `Hi ${ownProps.user && ownProps.user.firstName}`,
            link: URLs.dashboard_account_details
        },
        {
            title: `${ownProps.user && ownProps.user.mail}`,
            link: URLs.dashboard_account_details
        },
        {
            title: t('sidebar.account_details'),
            link: URLs.dashboard_account_details
        },
        {
            title: t('sidebar.logout'),
            link: URLs.logout
        },
    ];
    return (
        <div>
            <div className="notifications-dropdown mx-3 relative">
                <div
                    onClick={() => ownProps.setIsOnProfile(!ownProps.isOnProfile)}
                    className="pt-4 md:pt-0 md:flex items-center ">
                    <div className="order-2 notifications-dropdown--button cursor-pointer rounded-full ">
                        <img
                            src={checkAvatar(ownProps.user && ownProps.user.photo)}
                            onError={handleAvatarError}
                            alt="" className="rounded-full w-12 h-12 avatar"/>

                    </div>
                    <p className="mt-2 md:mt-0 md:mr-2 font-bold text-ellipsis w-28"
                       aria-label={ownProps.user && ownProps.user.fullName}>
                        {ownProps.user && ownProps.user.fullName}
                    </p>
                </div>

            </div>
            {
                ownProps.isOnProfile &&
                <DropDown key={"profile"} classes="notifications-dropdown--menu" items={ProfileLinks}
                          onChangeLang={(data: string) => console.log("")}
                          kind="profile"
                          onChangeDropDown={(data: boolean) => ownProps.setIsOnProfile(data)}/>
            }
        </div>
    )
}
