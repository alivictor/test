import React, {Suspense, useEffect, useState} from 'react';
import {connect} from 'react-redux';
import './sidebar.scss';
import Loading from "../../../components/UiKits/Loading/Loading";
import UserModel from "../../../models/user.model";
import DropDown from "../../../components/DropDowns/DropDown";
import {checkAvatar, currencyFormatter, handleAvatarError} from "../../../assets/helpers/utilities";
import URLs from "../../../constants/URLs";
import {useTranslation} from "react-i18next";
import {Link} from 'react-router-dom'
import InfoCart from "../../../layouts/dashboard/Sidebar/Cart";
import {getLocalStorage} from "../../../assets/helpers/storage";
import moment from "moment-timezone";
import {ProfileDropDown} from "../Header/ProfileDropDown";

export interface INavItem {
    title: string,
    link: string,
    id: number,
    badge?: string | number | null
}

interface IProps {
    onClose: any,
    onOpen: any,

    isOnProfile: boolean,
    setIsOnProfile: any,

    onChangeLang: any,
    setIsOnLang: any,

    isOnLang: boolean,
    show: boolean,
    closeMobile: boolean,
    pathname: string,
    user: UserModel,
    NavItems: INavItem[]
}

export const flags = [
    {
        name: "EN",
        link: 'en-US'
    },
    {
        name: "GE",
        link: 'de-GE'
    },
];

const Sidebar = (ownProps: IProps) => {
    const [userTokenFullDetails, setUserTokenFullDetails] = useState<{
        user: UserModel,
        financial: {
            balance: number
            deposits: number
            marketCap: number
            roi: number | null
            smbPrice: string
            tokens: number
            avgSmbPrice: number
        }
    } | null>(null);

    useEffect(() => {
        getLocalStorage('userTokenFullDetails') && setUserTokenFullDetails(getLocalStorage('userTokenFullDetails'))
    }, [])

    const language = localStorage.getItem('i18nextLng') || 'en-US';
    const {t} = useTranslation();

    const ProfileLinks = [
        {
            title: `Hi ${ownProps.user && ownProps.user.firstName}`,
            link: URLs.dashboard_account_details
        },
        {
            title: `${ownProps.user && ownProps.user.mail}`,
            link: URLs.dashboard_account_details
        },
        {
            title: t('sidebar.account_details'),
            link: URLs.dashboard_account_details
        },
        {
            title: t('sidebar.logout'),
            link: URLs.logout
        },
    ];


    return (
        <Suspense fallback={<Loading/>}>
            <aside id="sidebar"
                   className={
                       `fixed block overflow-x-hidden without-scroll-bar 
                         overflow-y-auto m-0 p-0 clearfix sidebar 
                         ${ownProps.show ? 'open' : 'short'}
                         ${ownProps.closeMobile ? 'closeMobile' : null}
                         `
                   }>

                <div className="sidebar-container">
                    <div className="closeIcon ">
                        <img src="/assets/img/svg/icons/close-icon.svg" alt="" onClick={() => ownProps.onClose()}/>
                    </div>
                    <div className="user-details">
                        <div className="flex justify-end">
                            <div className="notifications-dropdown mx-3 relative ">
                                <div className="notifications-dropdown--button cursor-pointer"
                                     onClick={() => null}>
                                    {
                                        0 > 0
                                        && <div
                                            className="notifications-dropdown--badge flex items-center justify-center">{0}</div>
                                    }
                                    <img src={"/assets/img/svg/icons/notification-icon.svg"}
                                         alt={`${0} notifications`}/>
                                </div>

                            </div>
                            <div className="languages-dropdown ml-3 relative ">
                                <div className="languages-dropdown--button rounded-full cursor-pointer h-6 w-6"
                                     onClick={() => ownProps.setIsOnLang(!ownProps.isOnLang)}>

                                    {language === "en-US" &&
                                    <span className="w-full h-full flex">
                                        EN
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>

                                    }
                                    {language === "de-GE" && <span className="w-full h-full flex">
                                        GE
                                        <img src="/assets/img/svg/icons/Polygon 3.svg" className="ml-1" alt=""/>
                                    </span>}
                                </div>
                                {
                                    ownProps.isOnLang && <DropDown classes="languages-dropdown--menu" items={flags}
                                                                   kind="languages"
                                                                   onChangeLang={(data: string) => ownProps.onChangeLang(data)}
                                                                   onChangeDropDown={(data: boolean) => ownProps.setIsOnLang(false)}/>
                                }
                            </div>
                        </div>
                        <ProfileDropDown isOnProfile={ownProps.isOnProfile} setIsOnProfile={(value:boolean)=>ownProps.setIsOnProfile(value)} user={ownProps.user}/>

                    </div>

                    <div className="pt-4 px-4 block ">
                        <Link to={URLs.dashboard_calculator}
                              className="flex flex-nowrap items-center mx-2 justify-start pl-2 h-12  rounded-full">
                            <span><img src="/assets/img/svg/icons/calculator-blue.svg" alt=""/></span>
                            <span className="ml-3 text-sm">
                                Start Date: {userTokenFullDetails ? moment(userTokenFullDetails.user.startedAt).format('LL') : "-"}
                            </span>
                        </Link>
                    </div>

                    <div className="mt-3">
                        <InfoCart
                            value={userTokenFullDetails ? currencyFormatter(userTokenFullDetails.financial.smbPrice) : "-"}
                            title={t('home.token_price')}
                            link={URLs.dashboard_accounts}
                            icon={"/assets/img/svg/icons/smb-token-price.svg"}/>
                    </div>
                    <div className="mt-3">
                        <InfoCart
                            value={userTokenFullDetails ? currencyFormatter(userTokenFullDetails.financial.marketCap) : "-"}
                            title={t('home.market_cap')}
                            link={URLs.dashboard_accounts}
                            icon={"/assets/img/svg/icons/market-cap-icon.svg"}/>
                    </div>
                    <div className="mt-3">
                        <InfoCart
                            value={userTokenFullDetails ? currencyFormatter(userTokenFullDetails.financial.marketCap) : "-"}
                            title={t('home.total_balance')}
                            link={URLs.dashboard_accounts}
                            icon={"/assets/img/svg/icons/total_balance_icon_black.svg"}/>
                    </div>

                </div>
            </aside>
        </Suspense>
    );
};

const mapStateToProps = (state: any) => {
    return {
        user: state.auth.user
    }
};

export default connect(mapStateToProps)(Sidebar);
