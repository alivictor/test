import React from 'react';
import {Link} from 'react-router-dom';
import {INavItem} from "./Sidebar";

interface IProps {
  Item: INavItem,
  active: boolean,
  show: boolean,
  onCloseMenu: any,
}

const NavLink = (Props: IProps) => {

  return (
    <Link to={Props.Item.link}
          onClick={()=>Props.onCloseMenu()}
          className={
      `relative block py-4 sidebar-nav--item
       flex flex-no-wrap align-middle overflow-x-hidden
        ${Props.show ? 'px-8' : 'px-4 justify-center'}
        ${Props.active && 'active'}`
    }
         key={Props.Item.id}>
      <div className="flex flex-no-wrap ">


        <span className={`text-white sidebar-nav--item-title ${Props.show && 'show'}`}>{Props.Item.title}</span>
        {
          Props.Item.badge &&
          <span className={`ml-10 show sidebar-badge flex items-center justify-center rounded-full text-white ${Props.show && 'badgeIcon'}`}>
            {Props.Item.badge}
          </span>
        }
      </div>
    </Link>

  );
};

export default NavLink;
