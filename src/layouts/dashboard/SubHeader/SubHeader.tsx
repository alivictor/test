import React from 'react';
import './subheader.scss';
import {useTranslation} from "react-i18next";
import UserModel from "../../../models/user.model";
import {getLocalStorage} from "../../../assets/helpers/storage";
import {Link, useRouteMatch} from "react-router-dom";

const SubHeader = () => {
    const router = useRouteMatch();
    const memberships: UserModel[] = getLocalStorage('memberships') || []
    const userData: UserModel | null = getLocalStorage('userData') || null;

    const {t} = useTranslation();
    return (
        <div className="w-full subheader pr-8 flex flex-no-wrap justify-between items-center">
            <div>
                {
                    memberships.length === 2
                    &&
                    <div className="dashboard-buttons">
                        {
                            userData && userData.registerMembership === "arbitrage-investor"
                                ?
                                <>
                                    <a href={"/dashboard/home"}
                                          className={` py-2 px-4 ${router.path.includes('/dashboard') && "active"}`}>
                                        Arbitrage Dashboard
                                    </a>
                                    <a href={"/token-dashboard/home"}
                                          className={`ml-3  py-2 px-4 ${router.path.includes('/token-dashboard') && "active"}`}>
                                        Token Dashboard
                                    </a>
                                </>
                                :
                                <>
                                    <a href={"/token-dashboard/home"}
                                          className={`py-2 px-4 ${router.path.includes('/token-dashboard') && "active"}`}>
                                        Token Dashboard
                                    </a>
                                    <a href={"/dashboard/home"}
                                          className={`ml-3 py-2 px-4 ${router.path.includes('/dashboard') && "active"}`}>
                                        Arbitrage Dashboard
                                    </a>
                                </>
                        }
                    </div>
                }
            </div>
        </div>
    );
};

export default SubHeader;
