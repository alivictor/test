import * as React from 'react';
import {useEffect, useState, Suspense} from "react";
import {renderRoutes} from "react-router-config";
import {INavItem} from "./Sidebar/Sidebar";
import Sidebar from "./Sidebar/TokenSidebar";
import './main.scss';
import {Redirect} from 'react-router-dom';
import UserModel from "../../models/user.model";
import {
    fetchNotifications,
    GetUserInfoAction,
    LoginAction,
    unknownLevel, unknownUser,
} from "../../actions/auth";
import {connect} from "react-redux";
import {
    useTranslation,
} from 'react-i18next';
import Loading from "../../components/UiKits/Loading/Loading";
import {indexApi} from "../../apis";
import {LevelItemInterface} from "../../models/levels.model";
import {IInvestRequests} from "../../actions/invest";
import {SET_ACTIVE_INVEST} from "../../constants/actionTypes";
import Header from "./Header/Header";
import URLs from "../../constants/URLs";

import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import 'swiper/swiper-bundle.min.css';
import SwiperCore, {Navigation, EffectFade, Autoplay} from 'swiper';

SwiperCore.use([Navigation, EffectFade, Autoplay]);

const Tabs = React.lazy(() => import("./Tab"));

const TokenDashboardWrapper =
    ({
         route,
         history,
         location,
         match,
         staticContext,
         LoginAction,
         AuthSuccess,
         AuthError,
         fetchAllNotifications
     }: any) => {
        const {t, i18n} = useTranslation();

        const changeLanguage = (lng: string) => {
            i18n.changeLanguage(lng);
        };
        const [open, setOpen] = useState<boolean>(true);
        const [closeMobile, setCloseMobile] = useState<boolean>(true);
        const [lang, setLang] = useState<boolean>(false);
        const [isOnProfile, setIsOnProfile] = useState<boolean>(false);
        const [notification, setNotification] = useState<boolean>(false);

        const toggleSidebar = (data: boolean) => {
            setOpen(data);
            setCloseMobile(prevState => !prevState);
        };

        const toggleSidebarMobile = () => {
            if (window.screen.width < 993) {
                setOpen(prevState => !prevState);
                setCloseMobile(prevState => !prevState)
            }
        };

        const hideDropDowns = (data: string | undefined) => {
            setLang(false);
            setNotification(false);

            if (data !== 'Sidebar') {
                toggleSidebarMobile()
            }

            if (data === 'main') {
                if (window.screen.width < 993) {
                    setOpen(true);
                    setCloseMobile(true)
                }
            }
        };

        useEffect(() => {
            if (localStorage.getItem('userAuth')) {
                let retrievedObject = localStorage.getItem('userData');

                let hasInvest = localStorage.getItem('HI') === 'undefined' || localStorage.getItem('HI') === null
                    ? 'false'
                    : localStorage.getItem('HI');

                let canInvest = localStorage.getItem('CI') === 'undefined' || localStorage.getItem('CI') === null
                    ? 'false'
                    : localStorage.getItem('CI');

                let isPremium = localStorage.getItem('IP') === 'undefined' || localStorage.getItem('IP') === null
                    ? 'false'
                    : localStorage.getItem('IP');

                let userLevel = localStorage.getItem('userLevel') === 'undefined' || localStorage.getItem('userLevel') === null
                    ? JSON.stringify(unknownLevel)
                    : localStorage.getItem('userLevel');

                if (retrievedObject != null) {
                    LoginAction({
                        user: JSON.parse(retrievedObject),
                        hasInvest: JSON.parse(hasInvest || 'false'),
                        isPremium: JSON.parse(isPremium || 'false'),
                        canInvest: JSON.parse(canInvest || 'false'),
                        level: userLevel ? JSON.parse(userLevel) : unknownLevel
                    })
                }
                let interval: any;
                indexApi.defaults.headers.Authorization = `bearer ${localStorage.getItem('userAuth')}`;
                AuthSuccess();
                interval = setInterval(function () {
                    indexApi.defaults.headers.Authorization = `bearer ${localStorage.getItem('userAuth')}`;
                    AuthError !== 'Unauthorized' && AuthSuccess();
                }, 1000 * 60);

                setTimeout(() => {
                    fetchAllNotifications({take: 5, skip: 0}, true);
                    clearTimeout()
                }, 1000)

                return function cleanup() {
                    clearInterval(interval);
                };
            }

            if (!localStorage.getItem('i18nextLng')) {
                localStorage.setItem('i18nextLng', 'en-US')
            }

        }, []);

        if (localStorage.getItem('userAuth') === null || AuthError === 'Unauthorized') {
            return (
                <>
                    <Redirect to={{
                        pathname: '/logout'
                    }}/>
                </>
            )
        } else {
            const userData: UserModel = JSON.parse(localStorage.getItem('userData') || "") || unknownUser;

            const NavItems: INavItem[] = [
                {
                    title: t('sidebar.dashboard'),
                    link: URLs.token_dashboard_home,
                    id: 1,
                },
                {
                    title: t('sidebar.transaction_history'),
                    link: URLs.token_dashboard_transaction_history,
                    id: 7,
                },
                {
                    title: t('sidebar.kyc'),
                    link: URLs.token_dashboard_kyc,
                    id: 9,
                    badge: userData && userData.kyc.verify ? null : 1
                },
                {
                    title: t('sidebar.account_details'),
                    link: URLs.token_dashboard_account_details,
                    id: 10,
                },
                {
                    title: t('sidebar.settings'),
                    link: URLs.token_dashboard_settings,
                    id: 12,
                },
            ];

            return (
                <Suspense fallback={<Loading/>}>
                    <div>
                        <Sidebar {...location}
                                 onOpen={(data: boolean) => toggleSidebar(data)}
                                 onClose={toggleSidebarMobile}
                                 show={open}
                                 onChangeLang={(data: string) => changeLanguage(data)}
                                 setIsOnLang={(data: boolean) => setLang(data)} isOnLang={lang}
                                 closeMobile={closeMobile}
                                 NavItems={NavItems}
                                 isOnProfile={isOnProfile}
                                 setIsOnProfile={(data: boolean) => setIsOnProfile(data)}
                        />
                        <main
                            className={`transition-0-2 main ${open ? 'opened' : 'closed'}`}>
                            <div className={`main-container main-bg`}>
                                <Header pathname={location.pathname}
                                        isOpen={open && (!closeMobile)}
                                        isClose={closeMobile}
                                        onOpen={toggleSidebarMobile}
                                        onClose={toggleSidebarMobile}
                                        onChangeLang={(data: string) => changeLanguage(data)}
                                        setIsOnLang={(data: boolean) => setLang(data)} isOnLang={lang}
                                        setIsOnNotification={(data: boolean) => setNotification(data)}
                                        isOnNotification={notification}
                                        isOnProfile={isOnProfile}
                                        setIsOnProfile={(data: boolean) => setIsOnProfile(data)}
                                />
                                <div className="w-full mt-4">
                                    <Tabs
                                        items={NavItems}
                                        route={location.pathname}
                                    />
                                </div>

                                <div className="sm:px-6 md:px-8 xl:px-10 small-px-3 h-full content"
                                     onClick={() => hideDropDowns('main')}>
                                    {/*<SubHeader/>*/}
                                    {
                                        route && renderRoutes(route.routes, {
                                            history,
                                            location,
                                            match,
                                            staticContext
                                        }, {})
                                    }
                                </div>
                            </div>
                        </main>
                    </div>
                </Suspense>
            );
        }
    };

const mapStateToProps = (state: any) => {
    return {
        AuthError: state.auth.error,
        userData: state.auth.user,
        isPremium: state.auth.isPremium,
        hasInvest: state.auth.hasInvest,
        canInvest: state.auth.canInvest,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        LoginAction: (data: { user: UserModel, level: LevelItemInterface, hasInvest: boolean, canInvest: boolean, isPremium: boolean }) => dispatch(LoginAction(data)),
        AuthSuccess: () => dispatch(GetUserInfoAction()),
        setActiveInvest: (invest: IInvestRequests) => dispatch({
            type: SET_ACTIVE_INVEST,
            payload: {
                data: invest
            }
        }),
        fetchAllNotifications: (data: { take: number, skip: number }, reset: boolean) => dispatch(fetchNotifications(data, reset)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TokenDashboardWrapper);
