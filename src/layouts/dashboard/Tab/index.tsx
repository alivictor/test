import React, {Suspense} from 'react';
import './style.scss'
import {INavItem} from "../Sidebar/Sidebar";
import {Link} from "react-router-dom";
import {Swiper, SwiperSlide} from 'swiper/react';
import Loading from "../../../components/UiKits/Loading/Loading";


interface IProps {
    items: INavItem[],
    route: any
}

const Index = (Props: IProps) => (
    <Suspense fallback={<Loading/>}>
        <div className="TabWrapper">
            <Swiper
                className="h-full w-full px-2"
                spaceBetween={30}
                slidesPerView={7}
                pagination
                navigation={{
                    nextEl: '.next',
                }}
                breakpoints={{
                    1099: {
                        slidesPerView: 4.5,
                    },
                    1500: {
                        slidesPerView: 7,
                    },
                }}
            >
                {
                    Props.items.map((item, index) =>
                        <SwiperSlide key={index}>
                            <Link to={item.link}
                                  className={`w-full h-full text-sm text-darkBlue 
                        font-bold inline-flex items-center justify-start cursor-pointer
                        `}>
                                <p className={`${Props.route === item.link && "active"}`}>{item.title}</p>
                            </Link>
                        </SwiperSlide>
                    )
                }

            </Swiper>

            <div className="next">
                <img src="/assets/img/svg/icons/next-icon.svg" alt=""/>
            </div>
        </div>
    </Suspense>
);

export default Index;