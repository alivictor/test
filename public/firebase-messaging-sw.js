importScripts('https://www.gstatic.com/firebasejs/7.13.2/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.13.2/firebase-messaging.js')

const firebaseConfig = {
  apiKey: "AIzaSyC2_5kGQvmYa_LX3UoXk9aCFSZztTVRqGQ",
  authDomain:"smartbitrage-9622b.firebaseapp.com",
  databaseURL: "https://smartbitrage-9622b.firebaseio.com",
  projectId: "smartbitrage-9622b",
  storageBucket: "smartbitrage-9622b.appspot.com",
  messagingSenderId: "1041911299151",
  appId:"1:1041911299151:web:077d124496bd3c05b2be3a",
};

firebase.initializeApp(firebaseConfig)

const initMessaging = firebase.messaging();

// initMessaging.setBackgroundMessageHandler(function(payload) {
//   console.log('[firebase-messaging-sw.js] Received background message ', payload);
//   // Customize notification here
//   const notificationTitle = payload.title;
//   const notificationOptions = {
//     body: payload.body,
//     icon: "./logo.png"
//   };
//
//   return self.registration.showNotification(notificationTitle,
//     notificationOptions);
// });